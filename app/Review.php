<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    //
    protected $casts=[
        'created_at' => 'datetime:M d, Y'
    ];

//    protected $with = ['user'];
 
    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }
    public function vendor(){
        return $this->belongsTo('App\vendor','vendor_id','id');
    }
    
}
