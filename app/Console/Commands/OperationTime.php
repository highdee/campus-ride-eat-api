<?php

namespace App\Console\Commands;

use App\Services\UserService;
use Illuminate\Console\Command;

class OperationTime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:vendor-operation-time';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command will send notifications to customers about the opening and closing time of their favorite vendors';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        UserService::getVendorOPTime();
    }
}
