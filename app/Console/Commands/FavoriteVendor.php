<?php

namespace App\Console\Commands;

use App\Services\UserService;
use App\User;
use Illuminate\Console\Command;

class FavoriteVendor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:favorite-vendor';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (User::all() as $user){
            try {
                UserService::saveFavouriteVendor($user->id);
            }catch (\Exception $exception){}
        }
    }
}
