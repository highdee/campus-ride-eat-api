<?php

namespace App\Console\Commands;

use App\item;
use App\Mail\customerNotification;
use App\Mail\sendCustomerOrderNotification;
use App\Mail\sendOrderNotification;
use App\Notification;
use App\order;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class sendNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:notify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

    }
}
