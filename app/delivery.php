<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class delivery extends Model
{
    //

    protected $appends=['source','destination'];

    protected $casts=[
        'delivered' => 'datetime:d-m-Y H:i A'
    ];

    public function getSourceAttribute(){
        $order=$this->order()->first();
        $source = $order->vendor()->first() != null ? $order->vendor()->first()->address : '';
        return $source;
    }

    public function getDestinationAttribute(){
        $destination=$this->order()->first();

        return $destination ? $destination->address:'';

    }
    public function user(){
        return $this->hasOne('App\User', 'id','user_id');
    }
    public function order(){
        return $this->belongsTo('App\order','order_id','id');
    }
    public function rider(){
        return $this->belongsTo('App\rider','rider_id','id');
    }
}
