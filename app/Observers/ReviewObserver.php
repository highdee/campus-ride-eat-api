<?php

namespace App\Observers;

use App\Review;
use App\vendor;

class ReviewObserver
{
    /**
     * Handle the review "created" event.
     *
     * @param  \App\Review  $review
     * @return void
     */
    public function created(Review $review)
    {
        try{
            $vendor=vendor::find($review->vendor_id);
            $reviews = $vendor->reviews()->get();
            $ratings=0;

            foreach ($reviews as $review){
                $ratings+=$review->rate;
            }

            $ratings= $ratings < 1 ? $ratings:$ratings/count($reviews);

            $vendor->rating =  round($ratings, 1);;
            $vendor->save();
        }catch (\Exception $msg){}
    }

    /**
     * Handle the review "updated" event.
     *
     * @param  \App\Review  $review
     * @return void
     */
    public function updated(Review $review)
    {
        //
    }

    /**
     * Handle the review "deleted" event.
     *
     * @param  \App\Review  $review
     * @return void
     */
    public function deleted(Review $review)
    {
        //
    }

    /**
     * Handle the review "restored" event.
     *
     * @param  \App\Review  $review
     * @return void
     */
    public function restored(Review $review)
    {
        //
    }

    /**
     * Handle the review "force deleted" event.
     *
     * @param  \App\Review  $review
     * @return void
     */
    public function forceDeleted(Review $review)
    {
        //
    }
}
