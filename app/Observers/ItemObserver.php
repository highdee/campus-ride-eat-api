<?php

namespace App\Observers;

use App\Http\Controllers\NotificationController;
use App\item;
use App\Services\UserService;
use App\User;
use App\vendor;
use Illuminate\Support\Facades\DB;

class ItemObserver
{
    /**
     * Handle the item "created" event.
     *
     * @param  \App\item  $item
     * @return void
     */
    public function created(item $item)
    {
        $favorites = UserService::getFavourite($item);

        $vendor = vendor::find($item->vendor_id);


        $devices = User::whereIn('id', $favorites)->pluck('device_id');


        NotificationController::PushNotification(
            $vendor->name,
            $item->name." has been added to our menu.",
            $devices,
            ['type'=>'food','restaurant_id'=>$vendor->id, 'item_id'=>$item->id]
        );


    }

    /**
     * Handle the item "updated" event.
     *
     * @param  \App\item  $item
     * @return void
     */
    public function updated(item $item)
    {
        //
    }

    /**
     * Handle the item "deleted" event.
     *
     * @param  \App\item  $item
     * @return void
     */
    public function deleted(item $item)
    {
        //
    }

    /**
     * Handle the item "restored" event.
     *
     * @param  \App\item  $item
     * @return void
     */
    public function restored(item $item)
    {
        //
    }

    /**
     * Handle the item "force deleted" event.
     *
     * @param  \App\item  $item
     * @return void
     */
    public function forceDeleted(item $item)
    {
        //
    }
}
