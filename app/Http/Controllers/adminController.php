<?php

namespace App\Http\Controllers;

use App\Withdrawal;
use Illuminate\Http\Request;
use App\User;
use App\vendor;
use App\rider;
use App\streets;
use App\order;
use App\item;
use App\itemType;
use App\delivery;
use App\DeliverySystem;
use App\transaction;
use App\balance;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Notification;
use App\Payforme;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;

class adminController extends Controller
{
                    // ORDERS
        // 1=order made, 2-vendor acccept order,3-rider picked up,4-delivered
    public function __construct(){
        $this->middleware('auth:admin');
    }

    public function delayTime(){
        $time = DB::table('config')->where('name', 'delay_time')->first();
        return view('admin.delay_time')->with('delay', $time);
    }

    public function updateDelayTime(Request $request){
        $this->validate($request, [
            'delay_time'=>'required|integer'
        ]);
        DB::table('config')->where('name', 'delay_time')->update([
            'value'=>$request->delay_time,
            'updated_at'=>now()
        ]);
        session()->flash('status', 'success');
        session()->flash('msg', 'Delay time updated successfully');
        return redirect()->back();
    }

    public function announcement(){
        $time = DB::table('config')->where('name', 'announcement')->first();
        // dd($time);
        return view('admin.announcement')->with('announcement',$time);
    }

    public function updateAnnouncement(Request $request){
        $this->validate($request, [
//            'announcement'=>'required|string'
        ]);
        DB::table('config')->where('name', 'announcement')->update([
            'value'=>$request->announcement,
            'updated_at'=>now()
        ]);
        session()->flash('status', 'success');
        session()->flash('msg', 'Notification updated successfully');
        return redirect()->back();
    }

    public function payformes(){
        $pays = Payforme::all();
        return view('admin.payformes')->with('pays',$pays);
    }

    public function payforme($id){
        $pay = Payforme::find($id);
        // dd($pay->payments[0]->pivot);
        if(!$pay){
            session()->flash('status', 'error');
            session()->flash('msg', 'Pay4me details not found');
        }
        $friends = DB::table('customer_payment_friend')->where('payforme_id', $pay->id)->get();
        $data = json_decode($pay->data);
        $foods = [];
        foreach($data->foods as $food){
            $item = item::find($food->item_id);
            $sd = [
                'name'=>$item->name,
                'quantity'=>$food->quantity
            ];
            array_push($foods, $sd);
        }
        // dd($foods);
        return view('admin.payforme')->with(['pay'=>$pay,'friends'=>$friends, 'foods'=>$foods]);
    }

    public function printUserExcel(){
        return Excel::download(new UsersExport(), 'users.xlsx');
    }

    public function showFeaturedFoods(){
        $foods = item::where('featured', 1)->orderBy('sort_id', 'asc')->get();
        if($foods->max('sort_id') == 0){
            $id = 1;
            foreach ($foods as $index =>$val){
                $val->sort_id = $id;
                $id += 1;
                $val->save();
            }
        }
        return view('admin.food_sort')->with('foods', $foods);
    }

    public function showFeaturedVendors(){
        $vendors = vendor::where('featured', 1)
                            ->where('deleted_at', null)
                            ->orderBy('sort_id', 'asc')->get();
        if($vendors->max('sort_id') == 0){
            $id = 1;
            foreach ($vendors as $val){
                $val->sort_id = $id;
                $id += 1;
                $val->save();
            }
        }
        $vendors = DB::table('vendors')->where('featured', 1)
                            ->where('deleted_at', null)
                            ->orderBy('sort_id', 'asc')->get();
        return view('admin.vendor_sort')->with('vendors', $vendors);
    }

    public function withdrawalRequest(){
        $with=Withdrawal::where('accepted_at', null)
                        ->where('rejected_at', null)
                        ->orderBy('created_at', 'desc')->get();
        return view('admin.withdrawal_request')->with('withdrawals', $with);
    }

    public function withdrawals(){
        $with=Withdrawal::where('accepted_at', null)
                        ->orWhere('rejected_at', null)
                        ->orderBy('created_at', 'desc')->get();
        return view('admin.withdrawals')->with('withdrawals', $with);
    }

    public function rejectWithdrawal(Request $request){
        $this->validate($request, [
            'withdrawal_id' => 'required|integer',
        ]);

        $with = Withdrawal::find($request->withdrawal_id);
        if (!$with) {
            session()->flash('status', 'error');
            session()->flash('msg', 'Withdrawal not found');
            return redirect('/campus-admin/withdrawal-request');
        }
        $with->rejected_at=now();
        $with->save();
        session()->flash('status','success');
        session()->flash('msg','Withdrawal request rejected successfully');
        return redirect()->back();
    }
    public function acceptWithdrawal(Request $request){
        $this->validate($request, [
            'withdrawal_id'=>'required|integer',
            'amount'=>'required',
        ]);

        $with=Withdrawal::find($request->withdrawal_id);
        if(!$with){
            session()->flash('status','error');
            session()->flash('msg','Withdrawal not found');
            return redirect('/campus-admin/withdrawal-request');
        }
        if($with->rider_id){
            $bal=balance::where('rider_id', $with->rider_id)->first();
            $bal->amount= $bal->amount - $request->amount;
            $bal->save();
        }else{
            $bal=balance::where('vendor_id', $with->vendor_id)->first();
            $bal->amount= $bal->amount - $request->amount;
            $bal->save();
        }
        $with->accepted_at=now();

        if(!$with->save()){
            session()->flash('status','error');
            session()->flash('msg','Error accepting withdrawal');
            return redirect()->back();
        }
        session()->flash('status','success');
        session()->flash('msg','Withdrawal request accepted successfully');
        return redirect()->back();
    }
    public function withdrawal($id){
        $with=Withdrawal::find($id);
        if(!$with){
            session()->flash('status','error');
            session()->flash('msg','Withdrawal not found');
            return redirect('/campus-admin/withdrawal-request');
        }
        return view('admin.withdrawal')->with('with', $with);
    }
    public function FoodFeaturedStatus(Request $request){
        $this->validate($request, [
            'food_id'=>'required|integer'
        ]);
        $item=item::find($request->food_id);
        if(!$item){
            session()->flash('status','error');
            session()->flash('msg','Item not found');
            return redirect()->back();
        }
        if($item->featured == 1){
            $item->featured = 0;
            $item->sort_id = 0;
            $msg= $item->name.' removed from featured successfully';
        }else{
            $lg = item::where('featured', 1)->max('sort_id');
            $item->featured = 1;
            $item->sort_id = $lg + 1;
            session()->flash('status','success');
            $msg= $item->name.' item featured successfully';
        }
        if(!$item->save()){
            session()->flash('status','error');
            session()->flash('msg','Error updating food status');
            return redirect()->back();
        }

        session()->flash('status','success');
        session()->flash('msg',$msg);
        return redirect()->back();
    }

    public function deleteFood(Request $request){
        $this->validate($request, [
            'food_id'=>'required|integer'
        ]);
        $food=item::find($request->food_id);
        if(!$food){
            session()->flash('status','error');
            session()->flash('msg','Food not found');
            return redirect('campus-admin/foods');
        }
        if(!$food->delete()){
            session()->flash('status','error');
            session()->flash('msg','Error deleting food');
            return redirect()->back();
        }
        session()->flash('status','success');
        session()->flash('msg','Food deleted successfully');
        return redirect('campus-admin/foods');
    }

    public function VendorFeaturedStatus(Request $request){
        $this->validate($request, [
            'vendor_id'=>'required|integer'
        ]);
        $item=vendor::find($request->vendor_id);
        if(!$item){
            session()->flash('status','error');
            session()->flash('msg','Vendor not found');
            return redirect()->back();
        }
        if($item->featured == 1){
            $item->featured = 0;
            $msg= $item->name.' removed from featured successfully';
        }else{
            $item->featured = 1;

            $lg = vendor::where('featured', 1)
                        ->where('deleted_at', null)
                        ->max('sort_id');
            $item->sort_id = $lg + 1;

            session()->flash('status','success');
            $msg= $item->name.' featured successfully';
        }
        if(!$item->save()){
            session()->flash('status','error');
            session()->flash('msg','Error updating vendor status');
            return redirect()->back();
        }

        session()->flash('status','success');
        session()->flash('msg',$msg);
        return redirect()->back();
    }

    public function unverified_vendor(){
        $vendor=vendor::where('status',0)->get();
        return view('admin.unverified_vendors')->with('vendors',$vendor);
    }
    public function unverified($id){
        $vendor=vendor::find($id);
        $streets=streets::all();
        if(!$vendor){
            session()->flash('status','error');
            session()->flash('msg','Vendor not found');
            return redirect()->back();
        }
        return view('admin.verify_vendor')->with(['vendor'=>$vendor,'streets'=>$streets]);
    }
    public function verify_vendor(Request $req){
        $this->validate($req, [
            'vendor_id'=>'required|integer',
        ]);

        $ven=vendor::find($req->vendor_id);
        if($req->type){
            $ven->type=$req->type;
        }
        if($req->name){
            $ven->name=$req->name;
        }
        if($req->phone){
            $ven->phone=$req->phone;
        }
        if($req->street){
            $ven->street=$req->street;
        }
        if($req->address){
            $ven->address=$req->address;
        }
        $ven->status=1;
        if(!$ven->save()){
            session()->flash('status','error');
            session()->flash('msg','Error verifying vendor');
            return redirect()->back();
        }
        $user = User::find($ven->user_id);
        NotificationController::PushNotification("Rideeat accepted your Application","Your application to become a vendor on rideeats has been accepted.",[$user->device_id],['type'=>'vendor_accepted','id'=>$ven->id]);

        session()->flash('status','success');
        session()->flash('msg','Vendor verified successfully');
        return redirect('/campus-admin/vendor/'.$ven->id);
    }

    public function cancelVendorRequest(Request $req){
        $this->validate($req, [
            'vendor_id'=>'required|integer'
        ]);
        $ven=vendor::find($req->vendor_id);
        $ven->status = -1;
        if(!$ven->save()){
            session()->flash('status','error');
            session()->flash('msg','Error cancelling vendor request');
            return redirect()->back();
        }
        session()->flash('status','success');
        session()->flash('msg','Vendor request cancelled successfully');
        return redirect('/campus-admin/vendors-request');
    }
    public function cancelRiderRequest(Request $req){
        $this->validate($req,[
            'rider_id'=>'required|integer'
        ]);
        $rider=rider::find($req->rider_id);
        $rider->status = -1;
        if(!$rider->save()){
            session()->flash('status','error');
            session()->flash('msg','Error cancelling rider request');
            return redirect()->back();
        }
        session()->flash('status','success');
        session()->flash('msg','Rider request cancelled successfully');
        return redirect('/campus-admin/riders-request');
    }
    public function unverified_riders(){
        $rider=rider::where('status',0)->get();
        return view('admin.unverified_riders')->with('riders',$rider);
    }
    public function unverified_rider($id){
        $rider=rider::find($id);
        if(!$rider){
            session()->flash('status','error');
            session()->flash('msg','Rider not found');
            return redirect()->back();
        }
        return view('admin.unverified_rider')->with('rider',$rider);
    }
    public function verify_rider(Request $req){
        $this->validate($req, [
            'rider_id'=>'required|integer',
        ]);

        $rider=rider::find($req->rider_id);
        $rider->status=1;
        if($req->vehicle_type){
            $rider->vehicle_type=$req->type;
        }
        if($req->name){
            $rider->name=$req->name;
        }
        if($req->address){
            $rider->address=$req->address;
        }
        if($req->reg_no){
            $rider->vehicle_reg_number=$req->reg_no;
        }
        if($req->facebook){
            $rider->facebook=$req->facebook;
        }
        if($req->kin_name){
            $rider->kin_name=$req->kin_name;
        }
        if($req->kin_phone){
            $rider->kin_phone=$req->kin_phone;
        }
        if($req->kin_address){
            $rider->kin_address=$req->kin_address;
        }
        if(!$rider->save()){
            session()->flash('status','error');
            session()->flash('msg','Error verifying rider');
            return redirect()->back();
        }
        session()->flash('status','success');
        session()->flash('msg','Rider verified successfully');
        return redirect()->back();
    }

    public function updateType(Request $req){
        $this->validate($req, [
            'type'=>'required|string',
            'type_id'=>'required',
        ]);
        $type=itemType::find($req->type_id);
        if(!$type){
            session()->flash('status','error');
            session()->flash('msg','Food type not found');
            return redirect('/campus-admin/types');
        }
        $type->title=$req->type;

        if($req->hasFile('image')){
            $file=$req->file('image');
            $fileformats=['jpg','JPG','jpeg','JPEG','png','PNG'];

            if(!in_array($file->getClientOriginalExtension(), $fileformats)){
                session()->flash('status', 'error');
                session()->flash('msg', 'Invalid image format');
                return redirect()->back();
            }

            $imagename=date('dmyhis').$file->getClientOriginalName();
            $imagename=str_replace('(','',$imagename);
            $imagename=str_replace(')','',$imagename);

            $file->move(storage_path()."/foods/types/",$imagename);
            $type->filename="http://".$req->getHttpHost()."/foods/types/".$imagename;
        }
        $items=item::where('item_type_id',$req->type_id)->get();
        foreach($items as $item){
            $item->type = $req->type;
            $item->save();
        }
        if(!$type->save()){
            session()->flash('status', 'error');
            session()->flash('msg','Error updating food type, pls try again');
            return redirect()->back();
        }

        session()->flash('status', 'success');
        session()->flash('msg','Food type updated successfully');
        return redirect()->back();

    }

    public function mailUser(Request $request){
        $this->validate($request, [
            'user_id'=>'required|integer',
            'title'=>'required|string',
            'message'=>'required|string'
        ]);

        $notification=new Notification();
        $notification->model_id=$request->user_id;
        $notification->type='message';
        $notification->title=$request->title;
        $notification->text=$request->message;
        if(!$notification->save()){
            session()->flash('status','error');
            session()->flash('msg','Error sending mail to user');
            return redirect()->back();
        }
        session()->flash('status','success');
        session()->flash('msg','Mail sent successfully');
        return redirect()->back();
    }

    public function notifyAllUser(Request $request){
        $this->validate($request, [
            'title'=>'required|string',
            'message'=>'required|string',
            'send_to'=>'required|string'
        ]);

        $notification=new Notification();
        DB::table('notifications')->insert([
            'model_id'=>0,
            'type'=>'message',
            'title'=>$request->title,
            'text'=>$request->message,
            'created_at'=>now(),
            'updated_at'=>now()
        ]);
        if($request->send_to == 'users'){
            $users = User::all()->pluck('device_id');
            // dd($users);

        }elseif($request->send_to == 'riders'){
            $riders = rider::all();
            $device = [];
            foreach($riders as $vendor){
                if($vendor->user){
                    array_push($device, $vendor->user->device_id);
                }
            }
            $users = $device;
        }elseif($request->send_to == 'vendors'){
            $vendors = vendor::all();
            $device = [];
            foreach($vendors as $vendor){
                if($vendor->user){
                    array_push($device, $vendor->user->device_id);
                }
            }
            $users = $device;
        }
        NotificationController::PushNotification($request->title,$request->message, $users,['type'=>'message','id'=>0], 1);
        session()->flash('status','success');
        session()->flash('msg','Mail sent successfully');
        return redirect()->back();
    }

    public function creditUser(Request $request){
        $this->validate($request, [
            'user_id'=>'required|integer',
            'amount'=>'required|regex:/^\d*(\.\d{2})?$/'
        ]);
        $user = User::find($request->user_id);
        if(!$user){
            session()->flash('status','error');
            session()->flash('msg','User not found');
            return redirect('campus-admin/userList');
        }
        $token=mt_rand(1000000,9999999);

        //check this user already as an unused token
        DB::table('credit_wallet')->where('user_id', $request->user_id)
                                        ->where('used_at', null)
                                        ->delete();

        //send token to admin mail
        $notification=new Notification();
        $notification->type='admin';
        $notification->email=env('MAIL_USERNAME');
        $notification->title='Credit user account confirmation';
        $notification->text='To credit '. $user->fullname.' with <b>₦'.$request->amount. '</b>. Your token is <h3>'. $token.'</h3>';
        $notification->save();

        DB::table('credit_wallet')->insert([
            'user_id'=>$request->user_id,
            'amount'=>$request->amount,
            'token'=>$token,
            'created_at'=>now()
        ]);
        session()->flash('status','success');
        session()->flash('msg','To continue this transaction,enter the token sent to the admin mail');
        return redirect('campus-admin/confirm-user-credit/'.$user->id.'/'.$request->amount);
    }

    public function confirmUserCredit($user_id, $amount){
        if(empty($user_id) || empty($amount)){
            session()->flash('status','error');
            session()->flash('msg','Invalid transaction, try again');
            return redirect('campus-admin/userList');
        }

        $user = User::find($user_id);
        if(!$user){
            session()->flash('status','error');
            session()->flash('msg','User not found');
            return redirect('campus-admin/userList');
        }

        $cred=DB::table('credit_wallet')->where('user_id', $user_id)
                                ->where('amount', $amount)
                                ->where('used_at', null)
                                ->first();



        if(!$cred){
            session()->flash('status','error');
            session()->flash('msg','Transaction not found for this user');
            return redirect('campus-admin/user/'.$user_id);
        }
        return view('admin.confirm_credit')->with(['amount'=>$amount, 'user'=>$user]);

    }

    public function verifyUserCredit(Request $request){
        $this->validate($request, [
            'user_id'=>'required|integer',
            'token'=>'required'
        ]);

        $cred = DB::table('credit_wallet')->where('user_id', $request->user_id)
                                        ->where('used_at', null)
                                        ->where('token', $request->token)
                                        ->first();
        if(!$cred){
            session()->flash('status','error');
            session()->flash('msg','Invalid token, check and try again');
            return redirect()->back();
        }

        DB::table('credit_wallet')->where('user_id', $request->user_id)
                                ->where('used_at', null)
                                ->where('token', $request->token)
                                ->update([
                                    'used_at'=>now()
                                ]);
        PaymentController::creditUser($cred->amount, $request->user_id, "Rideeat credited your wallet.", $request->narration ?? $request->narration );

        session()->flash('status','success');
        session()->flash('msg','User account credited successfully');
        return redirect('campus-admin/user/'.$request->user_id);
    }

    public function creditVendor(Request $request){
        $this->validate($request, [
            'vendor_id'=>'required|integer',
            'amount'=>'required|regex:/^\d*(\.\d{2})?$/'
        ]);
        $vendor = vendor::find($request->vendor_id);
        if(!$vendor){
            session()->flash('status','error');
            session()->flash('msg','Vendor not found');
            return redirect('campus-admin/vendorList');
        }
        $token=mt_rand(1000000,9999999);

        //check this user already as an unused token
        DB::table('credit_wallet')->where('vendor_id', $request->vendor_id)
                                        ->where('used_at', null)
                                        ->delete();

        //send token to admin mail
        $notification=new Notification();
        $notification->type='admin';
        $notification->email=env('MAIL_USERNAME');
        $notification->title='Credit vendor account confirmation';
        $notification->text='To credit '. $vendor->name.' with <b>₦'.$request->amount. '</b>. Your token is <h3>'. $token.'</h3>';
        $notification->save();

        DB::table('credit_wallet')->insert([
            'vendor_id'=>$request->vendor_id,
            'amount'=>$request->amount,
            'token'=>$token,
            'created_at'=>now()
        ]);
        session()->flash('status','success');
        session()->flash('msg','To continue this transaction,enter the token sent to the admin mail');
        return redirect('campus-admin/confirm-vendor-credit/'.$vendor->id.'/'.$request->amount);
    }
    public function confirmVendorCredit($vendor_id, $amount){
        if(empty($vendor_id) || empty($amount)){
            session()->flash('status','error');
            session()->flash('msg','Invalid transaction, try again');
            return redirect('campus-admin/vendorList');
        }

        $vendor = vendor::find($vendor_id);
        if(!$vendor){
            session()->flash('status','error');
            session()->flash('msg','Vendor not found');
            return redirect('campus-admin/vendorList');
        }

        $cred=DB::table('credit_wallet')->where('vendor_id', $vendor_id)
                                ->where('amount', $amount)
                                ->where('used_at', null)
                                ->first();

        if(!$cred){
            session()->flash('status','error');
            session()->flash('msg','Transaction not found for this vendor');
            return redirect('campus-admin/vendor/'.$vendor_id);
        }
        return view('admin.confirm_vendor_credit')->with(['amount'=>$amount, 'vendor'=>$vendor]);

    }
    public function verifyVendorCredit(Request $request){
        $this->validate($request, [
            'vendor_id'=>'required|integer',
            'token'=>'required',
            'user_id'=>'required|integer'
        ]);

        $cred = DB::table('credit_wallet')->where('vendor_id', $request->vendor_id)
                                                ->where('used_at', null)
                                                ->where('token', $request->token)
                                                ->first();
        if(!$cred){
            session()->flash('status','error');
            session()->flash('msg','Invalid token, check and try again');
            return redirect()->back();
        }

        DB::table('credit_wallet')->where('vendor_id', $request->vendor_id)
                                                ->where('used_at', null)
                                                ->where('token', $request->token)
                                                ->update([
                                                    'used_at'=>now()
                                                ]);
        $balance = balance::where('vendor_id', $request->vendor_id)->first();
        // dd($balance);
        if(!$balance){
            $balance= new balance();
            $balance->vendor_id = $request->vendor_id;
            $amount = $cred->amount;
        }else{
            $amount = $balance->amount + $cred->amount;
        }
        $balance->amount = $amount;
        $balance->save();

        //add to transaction table
        $tran = new transaction();
        $tran->user_id = $request->user_id;
        $tran->vendor_id = $request->vendor_id;
        $tran->amount = $cred->amount;
        $tran->p_reference='Admin credit vendor transaction';
        $tran->reference='Credit vendor transaction';
        $tran->save();

        session()->flash('status','success');
        session()->flash('msg','Vendor account credited successfully');
        return redirect('campus-admin/vendor/'.$request->user_id);
    }

    public function creditRider(Request $request){
        $this->validate($request, [
            'rider_id'=>'required|integer',
            'amount'=>'required|regex:/^\d*(\.\d{2})?$/'
        ]);
        $rider = rider::find($request->rider_id);
        if(!$rider){
            session()->flash('status','error');
            session()->flash('msg','Rider not found');
            return redirect('campus-admin/vendorList');
        }
        $token=mt_rand(1000000,9999999);

        //check this user already as an unused token
        DB::table('credit_wallet')->where('rider_id', $request->rider_id)
                                        ->where('used_at', null)
                                        ->delete();

        //send token to admin mail
        $notification=new Notification();
        $notification->type='admin';
        $notification->email=env('MAIL_USERNAME');
        $notification->title='Credit rider account confirmation';
        $notification->text='To credit '. $rider->name.' with <b>₦'.$request->amount. '</b>. Your token is <h3>'. $token.'</h3>';
        $notification->save();

        DB::table('credit_wallet')->insert([
            'rider_id'=>$request->rider_id,
            'amount'=>$request->amount,
            'token'=>$token,
            'created_at'=>now()
        ]);
        session()->flash('status','success');
        session()->flash('msg','To continue this transaction,enter the token sent to the admin mail');
        return redirect('campus-admin/confirm-rider-credit/'.$rider->id.'/'.$request->amount);
    }
    public function confirmRiderCredit($rider_id, $amount){
        if(empty($rider_id) || empty($amount)){
            session()->flash('status','error');
            session()->flash('msg','Invalid transaction, try again');
            return redirect('campus-admin/riderList');
        }

        $rider = rider::find($rider_id);
        if(!$rider){
            session()->flash('status','error');
            session()->flash('msg','Rider not found');
            return redirect('campus-admin/riderList');
        }

        $cred=DB::table('credit_wallet')->where('rider_id', $rider_id)
                                ->where('amount', $amount)
                                ->where('used_at', null)
                                ->first();

        if(!$cred){
            session()->flash('status','error');
            session()->flash('msg','Transaction not found for this rider');
            return redirect('campus-admin/rider/'.$rider_id);
        }
        return view('admin.confirm_rider_credit')->with(['amount'=>$amount, 'rider'=>$rider]);

    }
    public function verifyRiderCredit(Request $request){
        $this->validate($request, [
            'rider_id'=>'required|integer',
            'token'=>'required',
            'user_id'=>'required|integer'
        ]);

        $cred = DB::table('credit_wallet')->where('rider_id', $request->rider_id)
                                                ->where('used_at', null)
                                                ->where('token', $request->token)
                                                ->first();
        if(!$cred){
            session()->flash('status','error');
            session()->flash('msg','Invalid token, check and try again');
            return redirect()->back();
        }

        DB::table('credit_wallet')->where('rider_id', $request->rider_id)
                                                ->where('used_at', null)
                                                ->where('token', $request->token)
                                                ->update([
                                                    'used_at'=>now()
                                                ]);
        $balance = balance::where('rider_id', $request->rider_id)->first();
        // dd($balance);
        if(!$balance){
            $balance= new balance();
            $balance->rider_id = $request->rider_id;
            $amount = $cred->amount;
        }else{
            $amount = $balance->amount + $cred->amount;
        }
        $balance->amount = $amount;
        $balance->save();

        //add to transaction table
        $tran = new transaction();
        $tran->user_id = $request->user_id;
        $tran->rider_id = $request->rider_id;
        $tran->amount = $cred->amount;
        $tran->p_reference='Admin credit rider transaction';
        $tran->reference='Credit rider transaction';
        $tran->save();

        session()->flash('status','success');
        session()->flash('msg','Rider account credited successfully');
        return redirect('campus-admin/rider/'.$request->user_id);
    }

    public function addItemType(Request $req){
        $this->validate($req, [
            'type'=>'required|string',
        ]);
        if($req->hasFile("image")){
            $file=$req->file('image');
            $fileformats=['jpg','JPG','jpeg','JPEG','png','PNG'];

            if(!in_array($file->getClientOriginalExtension(), $fileformats)){
                session()->flash('status', 'error');
                session()->flash('msg', 'Invalid image format');
                return redirect()->back();
            }

            $imagename=date('dmyhis').$file->getClientOriginalName();
            $imagename=str_replace('(','',$imagename);
            $imagename=str_replace(')','',$imagename);

            $file->move(storage_path()."/foods/types/",$imagename);
            $imagename="http://".$req->getHttpHost()."/foods/types/".$imagename;

        }else{
            $imagename = null;
        }
        $type=new itemType;
        $type->title=$req->type;
        $type->filename=$imagename;
        if($type->save()){
            session()->flash('status', 'success');
            session()->flash('msg', 'Food type saved successfully');
            return redirect()->back();
        }else{
            session()->flash('status', 'error');
            session()->flash('msg', 'Error saving food type, pls try again');
            return redirect()->back();
        }
        return redirect()->back();
    }
    public function deleteType(Request $req){
        $this->validate($req, [
            'type_id'=>'required|integer'
        ]);
        $type=itemType::find($req->type_id);
        if(!$type){
            session()->flash('status', 'error');
            session()->flash('msg','Food type not found');
            return redirect('/campus-admin/types');
        }
        if(!$type->delete()){
            session()->flash('status', 'error');
            session()->flash('msg','Error deleting food type, pls try again');
            return redirect()->back();
        }
        session()->flash('status', 'success');
        session()->flash('msg','Food type deleted successfully');
        return redirect('/campus-admin/types');
    }
    public function types(){
        $types=itemType::all();
        return view('admin.types')->with('types',$types);
    }
    public function type($id){
        $ty=itemType::find($id);
        if(!$ty){
            session()->flash('status','error');
            session()->flash('msg','Food type not found');
            return redirect('/campus-admin/types');
        }
        return view('admin.type')->with('type',$ty);
    }
    public function foods(){
        $items=item::all();
        return view('admin.foods')->with('foods',$items);
    }
    public function food($id){
        $food=item::find($id);
        if(!$food){
            session()->flash('status','error');
            session()->flash('msg','Food not found');
            return redirect('/campus-admin/foods');
        }
        return view('admin.food')->with('food',$food);
    }
    public function orders(){
        $orders=order::orderBy('created_at', 'desc')->with('owner')->with('vendor')->with('rider')->get();
        return view('admin.orders')->with('orders',$orders);
    }

    public function order($id){
        $order=order::find($id);
        return view('admin.order')->with('order',$order);
    }

    public function adminCancelOrder(Request $request){

        $this->validate($request, [
            'order_id'=>'required|integer',
        ]);

        $order = order::where('id', $request->order_id)->with('vendor','vendor.user')->first();
        if(!$order){
            session()->flash('status','error');
            session()->flash('msg','Order not found');
            return redirect('/campus-admin/orders');
        }

        if($order->status != 1 && $order->status != 2 && $order->status != 3){
            session()->flash('status','error');
            session()->flash('msg','Order can not be canceled');
            return redirect('/campus-admin/orders');
        }


        if($request->pay_customer){
            $total = $order->total + $order->delivery_fare;

            //  Reverse payment back to user
            PaymentController::creditUser($total, $order->user_id, "Payment reversal");
        }

        if($request->debit_vendor){
            $total = $order->total;

            if($order->status >= 2){
                //REMOVE CHARGES
                $total = (env('SERVICE_CHARGE') / 100) * $total ;
                vendorController::debitVendor($order->vendor_id,$total,$request->notify_vendor ? true:false);
            }
        }

        if($request->notify_customer){
            $notification=new Notification();
            $notification->type='message';
            $notification->phone=$order->owner->phone;
            $notification->model_id=$order->owner->id;
            $notification->email=$order->owner->email;
            $notification->device=$order->owner->device_id;
            $notification->title='Order '.$order->reference.' canceled by Admin';
            $notification->text=strlen($request->comment) == 0 ? "We are sorry, this vendor is unable to process your order currently. Your payment will be reversed into your wallet." : $request->comment;
            $notification->save();
        }


        if($request->notify_vendor){
            $notification=new Notification();
            $notification->type='message';
            $notification->phone=$order->vendor->user->phone;
            $notification->model_id=$order->vendor->user->id;
            $notification->email=$order->vendor->user->email;
            $notification->device=$order->vendor->user->device_id;
            $notification->title='Order '.$order->reference.' canceled by Admin';
            $notification->text="Your order has been canceled by the system administrator.";
            $notification->save();
        }


        $order->status = -2;

        if($request->comment){
            $order->cancel_comment = $request->comment;
        }

        if(!$order->save()){
            session()->flash('status','error');
            session()->flash('msg','Error cancelling order');
            return redirect()->back();
        }
        session()->flash('status','success');
        session()->flash('msg','Order cancelled successfully');
        return redirect()->back();
    }

    public function adminCompleteOrder(Request $request){
        $this->validate($request, [
            'order_id'=>'required|integer'
        ]);

        $order = order::where('id', $request->order_id)->with('rider')->first();
        if(!$order){
            session()->flash('status','error');
            session()->flash('msg','Order not found');
            return redirect('/campus-admin/orders');
        }
        if($request->notify_user_c){
            // code to notify user
            $notification=new Notification();
            $notification->type='message';
            $notification->model_id=$order->user_id;
            $notification->title='Order completion notification';
            $notification->text='Order with the reference '.$order->reference.' has been completed.';
            $notification->save();
        }
        if($request->notify_rider_c && $order->rider != null){
            // code to notify rider
            $notification=new Notification();
            $notification->type='message';
            $notification->model_id=$order->rider->user_id;
            $notification->title='Order completion notification';
            $notification->text='Order with the reference '.$order->reference.' has been completed.';
            $notification->save();
        }


        $order->status = 5;
        if(!$order->save()){
            session()->flash('status','error');
            session()->flash('msg','Error completing order');
            return redirect()->back();
        }
        session()->flash('status','success');
        session()->flash('msg','Order completed successfully');
        return redirect()->back();
    }

    public function deliveries(){
        $deliveries=delivery::orderBy('created_at', 'desc')->with('user')->with('rider')->get();

        return view('admin.deliveries')->with('deliveries',$deliveries);
    }
    public function transactions(){
        $transactions=transaction::orderBy('created_at', 'desc')->get();
        return view('admin.transactions')->with('transactions',$transactions);
    }
    public function addSds(Request $req){
        $this->validate($req, [
            'from'=>'required|string',
            'to'=>'required|string',
            'delivery_system'=>'required|string',
            'cost'=>'required|integer'
        ]);
        $sds = DB::insert('insert into source_delivery_sytem_destination (source_id,destination_id,delivery_id,cost) values (?,?,?,?)', [$req->from,$req->to,$req->delivery_system,$req->cost]);
        if(!$sds){
            session()->flash('status','error');
            session()->flash('msg','Error setting source delivery destination system');
            return redirect('campus-admin/sds');
        }
        session()->flash('status','success');
        session()->flash('msg','Source delivery destination system created successfully');
        return redirect('campus-admin/sds');
    }
    public function updateSds(Request $req){
        $this->validate($req, [
            'sds_id'=>'required',
            'from'=>'required|string',
            'to'=>'required|string',
            'delivery_system'=>'required|string',
            'cost'=>'required|integer'
        ]);
        $sds =  DB::table('source_delivery_sytem_destination')
                ->where('id',$req->sds_id)
                ->limit(1)
                ->update([
                    'source_id'=>$req->from,
                    'destination_id'=>$req->to,
                    'delivery_id'=>$req->delivery_system,
                    'cost'=>$req->cost
                ]);
                // sds_id
        if(!$sds){
            session()->flash('status','error');
            session()->flash('msg','Error updating source delivery destination system');
            return redirect('campus-admin/sds');
        }
        session()->flash('status','success');
        session()->flash('msg','Source delivery destination system updated successfully');
        return redirect('campus-admin/sds');
    }
    public function deleteSds(Request $req){
        $this->validate($req, [
            'sds_id'=>'required',
        ]);
        $sds = DB::table('source_delivery_sytem_destination')
                ->where('id',$req->sds_id)
                ->delete();

        if(!$sds){
            session()->flash('status','error');
            session()->flash('msg','Error deleting source delivery destination system');
            return redirect('campus-admin/sds');
        }
        session()->flash('status','success');
        session()->flash('msg','Source delivery destination system deleted successfully');
        return redirect('campus-admin/sds');

    }
    public function sds(){
        $sds=DB::table('source_delivery_sytem_destination')->get();
        $streets=streets::all();
        $delivery_system=DeliverySystem::all();
        foreach($sds as $key=>$value){
            $sds[$key]->from = streets::find($sds[$key]->source_id)['name'];
            $sds[$key]->to = streets::find($sds[$key]->destination_id)['name'];
            $sds[$key]->delivery_system = DeliverySystem::find($sds[$key]->delivery_id)['name'];
        }
        return view('admin.sds')->with(['sds'=>$sds,'streets'=>$streets,'delivery_system'=>$delivery_system]);
    }
    public function delivery_system(){
        $ds=DeliverySystem::all();
        return view('admin.delivery_system')->with('delivery_system',$ds);
    }
    public function addDeliverySystem(Request $request){
        $this->validate($request, [
            'ds_name'=>'required|string'
        ]);
        $ds = new DeliverySystem();
        $ds->name = $request->ds_name;
        $ds->available = 1;
        if(!$ds->save()){
            session()->flash('status','error');
            session()->flash('msg','Error adding delivery system, pls try again');
            return redirect('campus-admin/delivery_system');
        }
        session()->flash('status','success');
        session()->flash('msg','Delivery system added successfully');
        return redirect('campus-admin/delivery_system');
    }
    public function deleteDelivery_system(Request $request){
        $this->validate($request, [
            'ds_id'=>'required|integer'
        ]);
        if(!DeliverySystem::destroy($request->ds_id)){
            session()->flash('status','error');
            session()->flash('msg','Error deleting delivery system, pls try again');
            return redirect('campus-admin/delivery_system');
        }
        session()->flash('status','success');
        session()->flash('msg','Delivery system deleted successfully');
        return redirect('campus-admin/delivery_system');
    }
    public function index(){
        $bal=balance::where('amount','>','0')->sum('amount');
        $debit=transaction::where('status',-1)->sum('amount');
        $credit=transaction::where('status',1)->sum('amount');
        $rider=rider::where('deleted_at', null)->count();
        $deliveries=delivery::all()->count();
        $vendor=vendor::all()->count();
        $users= User::all()->count();

        return view('admin.dashboard')->with([
            'user'=>$users,
            'riders'=>$rider,
            'delivery'=>$deliveries,'vendors'=>$vendor,'balance'=>$bal,'debit'=>$debit,
            'credit'=>$credit,
        ]);
    }
    public function userList(){
        $users=User::orderBy('created_at', 'desc')->get();
        return view('admin.users')->with('users', $users);
    }
    public function user($id){
        $user=User::find($id);
        return view('admin.user')->with('user',$user);
    }
    public function banUser(Request $req){
        $this->validate($req, [
            'user_id'=>'required|integer'
        ]);
        $user=User::find($req->user_id);
        if(!$user){
            session()->flash('status','error');
            session()->flash('msg','User not found, pls try again');
            return redirect('campus-admin/userList');
        }

        if($user->rider){
            $newStatus;
            $user->rider->status != 1 ? $newStatus = 1 : $newStatus = -1;
            $user->rider()->update(['status'=>$newStatus]);
        }
        if($user->vendor){
            $newStatus;
            $user->vendor->status != 1 ? $newStatus = 1 : $newStatus = -1;
            $user->vendor()->update(['status'=>$newStatus]);
        }

        $user->status != 1 ? $user->status = 1 : $user->status = -1;
        if(!$user->save()){
            session()->flash('status','error');
            session()->flash('msg','Error baning user, pls try again');
            return redirect()->back();
        }

        session()->flash('status','success');
        session()->flash('msg','Successful');
        return redirect()->back();
    }
    public function banVendor(Request $req){
        $this->validate($req, [
            'user_id'=>'required|integer'
        ]);
        $vendor=vendor::where('user_id', $req->user_id)->first();
        if(!$vendor){
            session()->flash('status','error');
            session()->flash('msg','Vendor not found, pls try again');
            return redirect('campus-admin/vendorList');
        }

        $vendor->status == 1 ? $vendor->status = -1 : $vendor->status = 1;
        if(!$vendor->save()){
            session()->flash('status','error');
            session()->flash('msg','Error banning vendor, pls try again');
            return redirect()->back();
        }

        session()->flash('status','success');
        session()->flash('msg','Successful');
        return redirect()->back();
    }
    public function banRider(Request $req){
        $this->validate($req, [
            'user_id'=>'required|integer'
        ]);
        $rider=rider::where('user_id', $req->user_id)->first();
        if(!$rider){
            session()->flash('status','error');
            session()->flash('msg','Rider not found, pls try again');
            return redirect('campus-admin/riderList');
        }

        $rider->status != 1 ? $rider->status = 1 : $rider->status = -1;
        if(!$rider->save()){
            session()->flash('status','error');
            session()->flash('msg','Error banning rider, pls try again');
            return redirect()->back();
        }

        session()->flash('status','success');
        session()->flash('msg','Successful');
        return redirect()->back();
    }
    public function deleteUser(Request $req){
        $this->validate($req, [
            'user_id'=>'required|integer'
        ]);
        $user=User::find($req->user_id);
        if(!$user){
            session()->flash('status','error');
            session()->flash('msg','User not found, pls try again');
            return redirect('campus-admin/userList');
        }
        if($user->rider){
            $user->rider()->delete();
        }
        if($user->vendor){
            $user->vendor()->delete();
        }
        if(!$user->delete()){
            session()->flash('status','error');
            session()->flash('msg','Error deleting user, pls try again');
            return redirect()->back();
        }
        session()->flash('status','success');
        session()->flash('msg','User deleted succussfully');
        return redirect('campus-admin/userList');
    }
    public function deleteVendor(Request $req){
        $this->validate($req, [
            'user_id'=>'required|integer'
        ]);
        $vendor=vendor::where('user_id',$req->user_id)->first();
        $user=User::find($req->user_id);
        if(!$vendor){
            session()->flash('status','error');
            session()->flash('msg','Vendor not found, pls try again');
            return redirect('campus-admin/vendorList');
        }
        $vendor->sort_id = 0;
        $vendor->featured = 0;
        $vendor->save();
        if(!$vendor->delete() && !$user->delete()){
            session()->flash('status','error');
            session()->flash('msg','Error deleting vendor');
            return redirect()->back();
        }
        session()->flash('status','success');
        session()->flash('msg','Vendor deleted succussfully');
        return redirect('campus-admin/vendorList');
    }
    public function deleteRider(Request $req){
        $this->validate($req, [
            'user_id'=>'required|integer'
        ]);
        $rider=rider::where('user_id',$req->user_id)->first();
        $user=User::find($req->user_id);
        if(!$rider){
            session()->flash('status','error');
            session()->flash('msg','Rider not found, pls try again');
            return redirect('campus-admin/riderList');
        }
        if(!$rider->delete() || !$user->delete()){
            session()->flash('status','error');
            session()->flash('msg','Error deleting rider, pls try again');
            return redirect()->back();
        }
        session()->flash('status','success');
        session()->flash('msg','Rider deleted succussfully');
        return redirect('campus-admin/riderList');
    }
    public function riderList(){
        $riders=rider::all();
        return view('admin.riders')->with('riders', $riders);
    }
    public function rider($user_id){
        $rider=rider::where('user_id',$user_id)->first();
        if(!$rider){
            session()->flash('status','error');
            session()->flash('msg','Rider not found');
            return redirect()->back();
        }
        return view('admin.rider')->with('rider',$rider);
    }
    public function vendorList(){
        $vendors=vendor::where('deleted_at',NULL)->get();
        return view('admin.vendors')->with('vendors', $vendors);
    }
    public function vendor($user_id){
        $vendor=vendor::where('user_id',$user_id)->first();
        if(!$vendor){
            session()->flash('status','error');
            session()->flash('msg','Vendor not found');
            return redirect('campus-admin/vendorList');
        }
        // dd($vendor->user->bankDetails);
        return view('admin.vendor')->with('vendor',$vendor);
    }
    public function streetList(){
        $streets=streets::all();
        return view('admin.streets')->with('streets', $streets);
    }
    public function addStreet(Request $request){
        $this->validate($request, [
            'street_name'=>'required|string'
        ]);

        $save = new streets();
        $save->name=$request->street_name;
        if(!$save->save()){
            session()->flash('status','error');
            session()->flash('msg','Error creating street, pls try again');
            return redirect('campus-admin/streetList');
        }
        session()->flash('status','success');
        session()->flash('msg','Street created successfully');
        return redirect('campus-admin/streetList');
    }
    public function deletestreet(Request $request){
        $this->validate($request, [
            'street_id'=>'required|integer'
        ]);
        if(!streets::destroy($request->street_id)){
            session()->flash('status','error');
            session()->flash('msg','Error deleting street, pls try again');
            return redirect('campus-admin/streetList');
        }
        session()->flash('status','success');
        session()->flash('msg','Street deleted successfully');
        return redirect('campus-admin/streetList');

    }
    public function logout() {
        Auth::logout();
        return redirect('/campus-admin/login');
    }
}
