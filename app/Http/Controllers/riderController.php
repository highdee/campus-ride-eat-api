<?php

namespace App\Http\Controllers;

use App\balance;
use App\delivery;
use App\openingtime;
use App\order;
use App\transaction;
use App\User;
use App\rider;
use App\vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class  riderController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function getTransactions($id){
        $transactions=transaction::where(['rider_id'=>$id])->paginate(15);

        return $transactions;
    }

    public function create(Request $request){
        $this->validate($request,[
            'name' => 'required|max:40',
            'phone' => 'required',
            'vehicle_type' => 'required',
            'address' => 'required|max:100',
        ]);

        $data=$request->input();
        $user=Auth::user();

        //VALIDATING THE RIDER'S NAME IF IT DOESN'T EXIST BEFORE
        $rider=rider::where(['name'=>$data['name']])->first();
        if($rider){
            if($rider->user_id != $user->id && ($rider->status  == 1 || $rider->status  == 0)) {
                return response([
                    'status' => false,
                    'message' => 'A rider already exist with this rider name',
                ]);
            }
            if($rider->user_id != $user->id && $rider->status == -1){
                $rider=new rider();
                $rider->user_id=$user->id;
            }
        }
        //-----------------------------------------------------

        if(!$rider){
            $rider=new rider();
            $rider->user_id=$user->id;
        }

        $rider->vehicle_type=$data['vehicle_type'];
        $rider->name=$data['name'];
        $rider->address=$data['address'];
        $rider->status=0;
        $rider->save();

        $user->phone=$data['phone'];
        $user->save();

        $user=User::find($user->id);

        return response([
            'status' => true,
            'message' => 'Your request has been submitted',
            'data'  => $user
        ]);
    }

    public function updateRider(Request $request){
        $this->validate($request,[
            'name' => 'required|max:40',
            'phone' => 'required',
            'vehicle_type' => 'required',
            'address' => 'required|max:100',
        ]);

        $data=$request->input();
        $user=Auth::user();

        if(!$user->rider){
            return response([],404);
        }

        $rider=rider::find($user->rider->id);

        $rider->vehicle_type=$data['vehicle_type'];
        $rider->name=$data['name'];
        $rider->address=$data['address'];
        $rider->save();

        $user->phone=$data['phone'];
        $user->save();

        $user=User::find($user->id);

        return response([
            'status' => true,
            'message' => 'Your request has been submitted',
            'rider'  => $rider,
            'user'=>$user
        ]);
    }

    public function getRider(){
        $user=auth::user();

        if(!$user->rider){
            return response([],404);
        }

        $rider=rider::find($user->rider->id);

        $rider['recent_transactions']=$rider->recent_transactions();

        return response([
            'status'=>200,
            'data'=>$rider
        ]);
    }

    public function update_opening_closing(Request $request){
        $this->validate($request,[
            'day'=>'required',
        ]);
        $user=auth::user();

        if(!$user->rider){
            return response([],404);
        }

        $data=$request->input();
        $opcl=openingtime::where(['rider_id'=>$user->rider->id,'day'=>$data['day']])->first();

        if(!$opcl){
            $opcl=new openingtime();
            $opcl->day=$data['day'];
            $opcl->rider_id=$user->rider->id;
        }

        if(isset($data['opening'])){
            $opcl->opening=$data['opening'];
        }
        if(isset($data['closing'])){
            $opcl->closing=$data['closing'];
        }

        $opcl->save();

        $rider=rider::find($user->rider->id);

        return response([
            'status'=>true,
            'message'=>'Time update',
            'data'=>$rider
        ]);

    }

    public function pickOrder($order){
        $user=auth::user();

        if(!$user->rider){
            return response('you are not a rider',404);
        }

        if($user->rider->occupied != 0){
            return response(['status' => false,'message'=>"You are already occupied. Deliver your current order to make another pickup",'data'=>$user->rider],200);
        }

        $order=order::where(['id'=>$order,'rider_id'=>null])->with('vendor')->first();

        if(!$order){
            return response('Order not found',404);
        }

        $order->rider_id=$user->rider->id;
        $order->status=3;
        $order->save();

        $delivery=delivery::find($order->delivery->id);
        $delivery->rider_id=$user->rider->id;
        $delivery->status=1;
        $delivery->picked=date('Y-m-d h:i:s');
        $delivery->save();

        $rider=rider::find($user->rider->id);
        $rider->occupied=1;
        $rider->save();

        NotificationController::PushNotification($user->rider->name,"Order from ".$order->vendor->name." is on its way!",[$order->user['device_id']],['type'=>'order','id'=>$order->id]);

        $order=order::where('id',$order->id)->with('street')->with('vendor')->with('rider','rider.user')->with('review','review.user')->first();


        return response([
            'status'=>true,
            'data'=>$order,
            'message'=>'Order was picked up successfully'
        ]);
    }

    public function getDeliveries(){
        $delivery=delivery::where('status',0)->with('order')->orderBy('id','desc')->paginate(15);
        return $delivery;
    }
    public function getMyDeliveries(){
        $user=auth::user();

        if(!$user->rider){
            return response([],200);
        }

        $delivery=delivery::where('status','>',0)->where('rider_id',$user->rider->id)->with('order')->orderBy('id','desc')->paginate(15);
        return $delivery;
    }

    public function deliverOrder($order,$code){
        $user=auth::user();

        if(!$user->rider){
            return response('you are not a rider',404);
        }

        $order=order::where(['id'=>$order,'rider_id'=>$user->rider->id])->with('street')->with('rider','rider.user')->first();

        if(!$order){
            return response('Order not found',404);
        }

        if($order->reference != $code){
            return response([
                'status'=>false,
                'data'=>[],
                'message'=>'Order reference code is not correct.'
            ],404);
        }

        $order->status=4;
        $order->save();

        $delivery=delivery::find($order->delivery->id);
        $delivery->status=2;
        $delivery->delivered=date('Y-m-d h:i:s');
        $delivery->save();

        //increasing rider balance on the platform
        $balance=balance::where('rider_id',$order->rider->id)->first();
        if(!$balance){
            $balance=new balance();
            $balance->rider_id=$order->rider->id;
            $balance->amount=0;
        }
        $balance->amount+=$order->delivery_fare;
        $balance->save();

        //saving transactions
        $Transaction=new transaction();
        $Transaction->user_id=$order->user['id'];
        $Transaction->rider_id=$user->rider->id;
        $Transaction->amount=$order->delivery_fare;
        $Transaction->p_reference='';
        $Transaction->reference=$order->reference;
        $Transaction->status=1;
        $Transaction->save();


        //rider is ready for another order
        $rider=rider::find($user->rider->id);
        $rider->occupied=0;
        $rider->save();


        $order=order::where(['id'=>$order->id,'rider_id'=>$user->rider->id])->with('vendor')->with('street')->with('rider','rider.user')->with('review','review.user')->first();
        NotificationController::PushNotification("Your order has been delivered","Your order has been delivered, Kindly give a comment about service quality.",[$order->user['device_id']],['type'=>'order','id'=>$order->id]);

        return response([
            'status'=>true,
            'data'=>$order,
            'message'=>'Order was marked delivered successfully'
        ]);
    }

}
