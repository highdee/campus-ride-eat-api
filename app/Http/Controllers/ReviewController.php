<?php

namespace App\Http\Controllers;

use App\order;
use App\Review;
use App\User;
use App\vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
    public function  __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function review(Request $request,$order){
        $details=$request->input();
        $user=Auth::user();



        $order=order::find($order);
        if(!$order){
            return response(['message'=>"Order not found"],404);
        }

        if($order->user_id != $user->id){
            return response(['message'=>"This order doesn't belong to you"],404);
        }


        $review=Review::where('order_id',$order->id)->first();
        if(!$review){
            $review=new Review();
        }
        $review->user_id=$user->id;
        $review->order_id=$order->id;
        $review->vendor_id=$order->vendor_id;

        $vendor=vendor::find($order->vendor_id);
        $vendor_user=User::find($vendor->user_id);

        if(isset($details['content'])){
            $review->content=$details['content'];
            NotificationController::PushNotification("Service review from ".$user->fullname, $details['content'], [$vendor_user->device_id],['type'=>'order','id'=>$order->id]);
        }

        if(isset($details['rate'])){
            $review->rate=$details['rate'];
            NotificationController::PushNotification("Service rating from ".$user->fullname, 'You received a rating of '.$details['rate'], [$vendor_user->device_id],['type'=>'order','id'=>$order->id]);
        }

        $review->save();


        $order=order::where('id',$order->id)->with('rider','rider.user')->with('street')->with('vendor')->first();

        return response([
            'status'=>true,
            'message'=>"Review was successfull",
            'data'=> $order
        ]);
    }
}
