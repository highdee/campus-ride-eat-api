<?php

namespace App\Http\Controllers;

use App\balance;
use App\card;
use App\item;
use App\Notification;
use App\Payforme;
use App\RunTimeLog;
use App\transaction;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use JWTAuth;

class PaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth')->except(['receivePaymentHook']);
    }

    public function getCard(){
        $user=Auth::user();
        $cards=card::where('user_id',$user->id)->get();
        return $cards;
    }

    public function creditWalletWithRef(Request $request){
        $user=Auth::user();
        $details=$request->input();
        $details=collect(json_decode($details['data']));

        $txn = PaymentController::verifyToken($details['reference'], $details['gateway']);
        if(!$txn->status){
            return response(['status'=>false,'message'=>"unable to verify your payment"],200);
        }

        $amount = $txn->amount;
        //Credit user
        PaymentController::creditUser($amount, $user->id, "Wallet Credited by card", $txn->reference);

        if($request->input('save_card') == '1'){
            PaymentController::SaveCard($details['reference']) ;
        }

        return response(['status'=>true,'message'=>"Wallet credited successfully", 'misc'=>appController::getMiscAction($user->id), 'user' => User::find($user->id)],200);
    }

    public function creditWalletWithExistingCard(Request $request,$id,$amount){
        $user=Auth::user();
        $card=card::where(['user_id'=>$user->id,'id'=>$id])->first();
        $details=$request->input();
        $details=collect(json_decode($details['data']));

        if(!$card){
            return response(['status'=>false,'message'=>'Card not found. Please try another one or pay using a new card'],200);
        }


        $result=PaymentController::chargeCard($card,($amount *100),$user->email);

        if(!$result){
            return response([
                'status'=>false,
                'message'=>"An unknown error occured while making payment.Please try again",
            ],200);
        }

        if(!$result->status){
            return response([
                'status'=>false,
                'message'=>"We are unable to process this payment.",
            ],200);
        }
        $details['reference']=$result->data->reference;

        //Credit wallet
        PaymentController::creditUser($amount, $user->id, "Wallet Credited by card");

        return response([
            'status'=>true,
            'message'=>"Your payment was successfull",
            'misc'=>appController::getMiscAction($user->id),
            'user' => User::find($user->id)
        ],200);
    }

    public static function ValidateCheckoutAmount($details, $amount){
        $items = [];
        foreach ($details['foods'] as $food){
            $item=item::find($food->item_id);
            $items[]=['item_id'=> $item->id,'price'=>$item->price,'quantity'=>$food->quantity];
        }

        return OrderController::getSum($items) > $amount ? false : true;
    }

    public function payWithWallet(Request $request, $amount){
        $user=Auth::user();
        $wallet = balance::where('user_id', $user->id)->first();
        $details=$request->input();
        $details=collect(json_decode($details['data']));

        if(!$wallet){
            return response(['status'=>false,'message'=>"Sorry, you don't have sufficient money in your wallet to perform this transaction."],200);
        }

        //      MAKING SURE USERS ARE NOT PAYING LESSER THAN THE AMOUNT TO BE PAID
        if(!PaymentController::ValidateCheckoutAmount($details, $amount)){
            return response([
                'status'=>false,
                'message'=>"Checkout total doesn't correspond to the amount to be paid",
            ],200);
        }
//      =================---------------------==============

        if(($wallet->amount * 100) < $amount){
            return response(['status'=>false,'message'=>"Sorry, you don't have sufficient money in your wallet to perform this transaction."],200);
        }

//        CHARGE USER
        $details['reference']=mt_rand(100000,999999999999).'RT';
        $result = PaymentController::debitUser(($amount/100), $user->id, "(".$details['reference'].") Order placed");
        if(!$result){
            return response(['status'=>false,'message'=>"Sorry, you don't have sufficient money in your wallet to perform this transaction."],200);
        }

//        SAVE ORDER
        $order=OrderController::saveOrder($details,$user,$request->input('save_order'));


        return response([
            'status'=>true,
            'message'=>"Your payment was successfull",
            'order'=>$order,
            'user'=> User::find($user->id),
            'misc'=>appController::getMiscAction($user->id)
        ],200);

    }

    public function getAccessCode(Request $request){

        $details=$request->input();
        $user=Auth::user();

        $headers = array(
            'Authorization: Bearer '.env('SECRET_KEY'),
            'Content-type: Application/json'
        );
        $fields=[
            'amount'=>$details['amount'] ,
            'email'=>$user->email,
        ];
        $url='https://api.paystack.co/transaction/initialize';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST,true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result=curl_exec($ch);
        curl_close($ch);
        $data=json_decode($result);


        if(!$data){
            return response([
                'status'=>false,
                'message'=>"An error occured while verifying payment.Please report to our team",
            ],200);
        }

        if($data->status){
            return response(collect($data),200);
        }else{
            return response(collect($data),200);
        }

    }
    public function payWithCard(Request $request,$id,$amount){
        $user=Auth::user();
        $card=card::where(['user_id'=>$user->id,'id'=>$id])->first();
        $details=$request->input();
        $details=collect(json_decode($details['data']));

        if(!$card){
            return response(['status'=>false,'message'=>'Card not found. Please try another one or pay using a new card'],200);
        }

//      MAKING SURE USERS ARE NOT PAYING LESSER THAN THE AMOUNT TO BE PAID
        if(!PaymentController::ValidateCheckoutAmount($details, $amount)){
            return response([
                'status'=>false,
                'message'=>"Checkout total doesn't correspond to the amount to be paid",
            ],200);
        }
//      ===============================

        $result=PaymentController::chargeCard($card,$amount,$user->email);

        if(!$result){
            return response([
                'status'=>false,
                'message'=>"An unknown error occured while making payment.Please try again",
            ],200);
        }

        if(!$result->status){
            return response([
                'status'=>false,
                'message'=>"We are unable to process this payment.",
            ],200);
        }

        $details['reference']=$result->data->reference;
        $order=OrderController::saveOrder($details,$user,$request->input('save_order'));

        $Transaction=new transaction();
        $Transaction->user_id=$user->id;
        $Transaction->amount=($amount/100);
        $Transaction->p_reference=$details['reference'];
        $Transaction->reference=0;
        $Transaction->content="Order payment by card";
        $Transaction->status=-1;
        $Transaction->save();

        return response([
            'status'=>true,
            'message'=>"Your payment was successfull",
            'order'=>$order,
            'misc'=>appController::getMiscAction($user->id)
        ],200);
    }

    public static function verifyToken($ref, $gateway='paystack'){
        $headers = array(
            'Authorization: Bearer '.env('SECRET_KEY'),
        );

        $url='https://api.paystack.co/transaction/verify/'.$ref;

        if($gateway == 'flutterwave'){
            $url='https://api.flutterwave.com/v3/transactions/'.$ref.'/verify';
            $headers=[
                'Content-Type: application/json',
                'Authorization: Bearer '.env('FLUTTERWAVE_SECRET')
            ];
        }


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST,false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result=curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);


        if($http_code != 200) return false;

        $data=json_decode($result);

//        Transaction already exist then we can't verify this
        $txn_already_exist = transaction::where('p_reference', $ref)->first();

        error_log(json_encode($result));
        if(!$data || $txn_already_exist){
            return false;
        }



        if($data->status != 'success') return false;

        $statuses = ['successful', 'success'];
        if(!in_array($data->data->status, $statuses)) return false;

        $txn = $data->data;

        if ($gateway == 'paystack') {
            $txn->amount =($txn->amount/100);
        }
        if($gateway == 'flutterwave'){
            $txn->reference = $txn->tx_ref;
        }
        return $txn;
    }

    public static function getTransaction($ref){
        $user=Auth::user();

        $headers = array(
            'Authorization: Bearer '.env('SECRET_KEY'),
        );

        $url='https://api.paystack.co/transaction/verify/'.$ref;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST,false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result=curl_exec($ch);
        curl_close($ch);
        $data=json_decode($result);

        if(!$data){
            return ['status'=>false];
        }

        return $data;
    }

    public static function SaveCard($reference){
        $result=PaymentController::getTransaction($reference);
        if(!$result->status){
            return response(['status'=>false,'message'=>'Reference'],200);
        }

        $data=collect($result->data->authorization);
        $customer=collect($result->data->customer);

        $card=new card();
        $card->user_id=Auth::user()->id;
        $card->account_name=$data['account_name'] == null ? '':$data['account_name'];
        $card->last4=$data['last4'];
        $card->exp_month=$data['exp_month'];
        $card->exp_year=$data['exp_year'];
        $card->reference=$reference;
        $card->card_type=$data['card_type'];
        $card->customer_code=$customer['customer_code'];
        $card->authorization_code=$data['authorization_code'];
        $card->status=1;

        $card->save();

//        return response()->json([
//            'status'=>true,
//            'message'=>'Card added succesfully.',
//            'data'=>$card
//        ]);

    }

    public static function chargeCard($card,$amount,$email){
        $user=Auth::user();

        $headers = array(
            'Authorization: Bearer '.env('SECRET_KEY'),
            'Content-type: Application/json'
        );
        $fields=[
            'authorization_code'=>$card->authorization_code,
            'amount'=>$amount,
            'email'=>$email,
            'send_invoices'=>true
        ];
        $url='https://api.paystack.co/transaction/charge_authorization';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST,true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result=curl_exec($ch);
        curl_close($ch);
        $data=json_decode($result);


        if(!$data){
            return false;
        }

        if($data->data->status != 'success'){
            return false;
        }

        return $data;
    }

    public static function creditUser($amount, $user_id, $content=null, $narration=null, $ref=null){
        $wallet = balance::where('user_id', $user_id)->first();
        if(!$wallet){
            $wallet = new balance();
            $wallet->user_id = $user_id;
        }
        $wallet->amount += $amount;
        $wallet->save();

        $User = User::find($user_id);

        $ref = $ref == null ? mt_rand(100000,999999999999).'RT' : $ref;
        $Transaction=new transaction();
        $Transaction->user_id=$User->id;
        $Transaction->amount=$amount;
        $Transaction->p_reference=$ref;
        $Transaction->reference=$ref;
        if($content){
            $Transaction->content=$content;
        }else if($narration){
            $Transaction->content=$narration;
        }else{
            $Transaction->content='Wallet credited';
        }
        $Transaction->status=1;
        $Transaction->save();

        //notify customer
        $narration = $narration ? $narration : 'Your wallet has been credited with NGN'.number_format($amount, 2);
        NotificationController::PushNotification('Wallet credited with NGN'.number_format($amount, 2),$narration, [$User->device_id],['type'=>'message','id'=>0], 1);

        try{
            $notification=new Notification();
            $notification->type='message';
            $notification->phone=$User->phone;
            $notification->model_id=$User->id;
            $notification->email=$User->email;
            $notification->device=$User->device_id;
            $notification->title='Wallet credited with NGN'.number_format($amount, 2);
            $notification->text='Your wallet has been credited with NGN'.number_format($amount, 2);
            $notification->save();
        }
        catch (\Exception $e){

        }

        return $ref;
    }

    public static function debitUser($amount, $user_id, $content="Debit", $ref=null, $debitVendorWallet=false){
        $wallet = '';
        if(!$debitVendorWallet){
            $wallet = balance::where('user_id', $user_id)->first();
        }else{
            $wallet = balance::where('vendor_id', $user_id)->first();
        }
        if(!$wallet){
            return false;
        }
        else if($wallet->amount < $amount){
            return false;
        }

        $wallet->amount -= $amount;
        $wallet->save();

        $User = User::find($user_id);

        $ref = $ref == null ? mt_rand(100000,999999999999).'RT' : $ref;

        $Transaction=new transaction();
        $Transaction->user_id=$User->id;
        $Transaction->amount=$amount;
        $Transaction->p_reference=$ref;
        $Transaction->reference=$ref;
        $Transaction->content=$content;
        $Transaction->status=-1;
        $Transaction->save();

        try{
            //notify customer
            NotificationController::PushNotification('Wallet debited with NGN'.$amount,'Your wallet has been debited with NGN'.$amount, [$User->device_id],['type'=>'message','id'=>0]);

            $notification=new Notification();
            $notification->type='message';
            $notification->phone=$User->phone;
            $notification->model_id=$User->id;
            $notification->email=$User->email;
            $notification->device=$User->device_id;
            $notification->title='Wallet debited with NGN'.$amount;
            $notification->text='Your wallet has been debited with NGN'.$amount;
            $notification->save();
        }
        catch (\Exception $exception){}

        return $ref;
    }

    public function receivePaymentHook(Request  $request){
        $details = $request->input();
        $details = $details['data'];
        //rideeats-hook
        if(!$request->headers->has('verif-hash')){
            return response('', 401);
        }
        if($request->headers->get('verif-hash') != 'rideeats-hook'){
            return response('', 401);
        }

        try {
            //Payment wasn't succcessfull

            if(!in_array(strtolower($details['status']), ['successful', 'successful'])){
                return response(['Not successful'], 200);
            }

            $reference = '';
            if(isset($details['tx_ref'])){
                $reference = $details['tx_ref'];
            }else if(isset($details['reference'])){
                $reference = $details['reference'];
            }
            // Not a Payforme
            if(str_starts_with($reference,'payforme_') == false){
                return response(['not a payforme'], 200);
            }
            $ref = explode('_', $reference);
            $token = $ref[1];

            $Plink = Payforme::where('token', $token)->first();
            //Reference not found
            if(!$Plink) return response(['Payforme not found'], 200);

            //Payforme expired
            if($Plink->expired_at != null) return response(['Payforme expired'], 200);

            $friend_name = '';
            if(array_key_exists('customer', $details)){
                $friend_name = $details['customer']['email'];
            }
            else if(array_key_exists('fullname', $details)){
                $friend_name = $details['fullname'];
            }
            $Plink->payments()->attach($Plink->user_id, [
                'friend_name'=> $friend_name,
                'amount'=> $details['amount']
            ]);

            self::creditUser($details['amount'], $Plink->user_id, null, "A friend has just contributed ".number_format($details['amount'])." to your order.");

            $Plink->status = 'started';

            $Plink = Payforme::find($Plink->id);

            if($Plink->progress >= $Plink->amount){
                $Plink->status = 'completed';
                $Plink->expired_at = date('Y-m-d H:i:s');
                $Plink->save();

                if($Plink->type == 'cart'){
                    $User = User::find($Plink->user_id);
                    $temp = collect(json_decode($Plink->data))->toArray();
                    $temp['reference'] = 'payforme_'.$Plink->token;
                    $Order = OrderController::saveOrder($temp, $User);
                    self::debitUser($Plink->amount, $Plink->user_id, "Your order was placed successfully.");
                    NotificationController::PushNotification('Your Payforme is completed', "Your order has been placed automatically", [$User->device_id],['type'=>'message','id'=>0], 1);
                }
            }
        }
        catch (\Exception $exception){
            error_log($exception);
            $runtimelog = RunTimeLog();
            $runtimelog->description = 'Error generated from verifying flutterwave payment';
            $runtimelog->text = json_encode($exception);
            $runtimelog->save();
        }

        return response($Plink, 200);
    }

}
