<?php

namespace App\Http\Controllers;

use App\DeliverySystem;
use App\item;
use App\itemType;
use App\order;
use App\Payforme;
use App\Review;
use App\streets;
use App\transaction;
use App\User;
use App\vendor;
use App\vendorphotos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;


class appController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('jwt.auth')->only(['getTransactions','updateDevice','getPayForMe']);
    }



    public function getItemsTypes(){
        $types=itemType::all();

        return $types;
    }

    public function getRestaurants(Request $request){
        $restaurants=vendor::where('status', 1)->whereNull('deleted_at');
        $items = [];


        if($request->has('search')){
            $restaurants=$restaurants->where('name','like','%'.$request->input('search').'%');

            $items=item::where('status',1);
            $items=$items->where('name','like','%'.$request->input('search').'%')->get();
        }

        $restaurants=$restaurants
            ->with('street')
//            ->with('reviews','reviews.user')
//            ->orderBy('rating', 'DESC')
            ->paginate(15);

        return ['restaurants'=>$restaurants,'foods'=>$items];
    }

    public function getRestaurant(Request $request, $id, $item_id){
        $restaurant=vendor::where('id', $id)->orWhere('uuid', $id)->with('street')->with('reviews','reviews.user')->first();
        if(!$restaurant){
            return response([], 404);
        }
        $item = null;
        $item = Item::where('id',$item_id)->orWhere('uuid', $item_id)->first();

        return response([
            'restaurant'=> $restaurant,
            'item'=> $item,
        ], 200);
    }

    public function getDeliveryData(){
        $streets=streets::all();
        $delivery_system=DeliverySystem::all();

        return [
            'streets' => $streets,
            'delivery_systems' => $delivery_system
        ];
    }

    public function getMisc(Request $request){
        $response = appController::getMiscAction($request->input('id'));
        return response($response,200);
    }

    public function updateDevice(Request $request){
        $user = Auth::user();
        if($request->has('device')){
            if($user){
                $user->device_id = $request->input('device');
                $user->save();

                return User::find($user->id);
            }
        }
    }

    public static function getMiscAction($id){
        //        GET ITEMS TYPES
        $foodtypes=itemType::all();

//        GET FEATURED RESTAURANTS
        $restaurants=vendor::where([
            'status'=>1,
            'featured'=>1
        ])->whereNull('deleted_at')->with('street')->with('reviews','reviews.user')->orderBy('sort_id','asc')->get();

//        GET FEATURED FOODS
        $items=item::where([
            'status'=>1,
            'featured'=>1
        ])->whereNull('deleted_at')->orderBy('sort_id','asc')->get();

//      GET ALL ACTIVE ORDERS OF THIS USER
        $active_orders = [];
        if(isset($id)){
            $active_orders = order::where('user_id',$id)->whereIn('status',[1,2,3])->count();
        }

        $order_delay_time = DB::table('config')->where('name', 'delay_time')->orderBy('id', 'desc')->first();
        $announcement = DB::table('config')->where('name', 'announcement')->orderBy('id', 'desc')->first();
        if($order_delay_time) $order_delay_time =  $order_delay_time->value;
        if($announcement) $announcement =  $announcement->value;

        $response=[
            'itemTypes'=>$foodtypes,
            'restaurants'=>$restaurants,
            'items'=>$items,
            'orders'=>$active_orders,
            'delay_time'=>$order_delay_time,
            'announcement'=>$announcement,
        ];
        return $response;
    }
    public function getItems(Request $request){
        $items=item::where('status',1)->whereNull('deleted_at');

        if($request->has('type')){
            $items=$items->where('item_type_id',$request->input('type'));
        }

        if($request->has('search')){
            $items=$items->where('name','like','%'.$request->input('search').'%');
        }


        $items=$items->paginate(15);
        return $items;
    }


    public function getTransactions($id){
        $user=Auth::user();
        $transactions=transaction::where(['user_id'=>$id]);

        if($user->vendor != null){
            $transactions =  $transactions->orWhere('vendor_id',$user->vendor->id);
        }

        if($user->rider != null){
            $transactions =  $transactions->orWhere('rider_id',$user->rider->id);
        }
        return $transactions->orderBy('id','desc')->paginate(15);
    }

    public function reviews($id){
        $reviews=Review::where('vendor_id',$id)->with('user')->paginate(10);

        return $reviews;
    }

    public function getPayForMe($id){
        $user=Auth::user();

        $pfm = Payforme::where('user_id', $user->id)->orderBy('id', 'desc')->paginate(10);

        return $pfm;
    }
}
