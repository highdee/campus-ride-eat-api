<?php

namespace App\Http\Controllers;

use App\balance;
use App\item;
use App\Notification;
use App\order;
use App\transaction;
use App\User;
use App\vendor;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $user=Auth::user();
        $details=$request->input();
        $details=collect(json_decode($details['data']));

        if(!isset($details['gateway'])) $details['gateway'] = 'paystack';

        $txn = PaymentController::verifyToken($details['reference'], $details['gateway']);

        if(!$txn){
            return response(['status'=>false,'message'=>"Unable to verify your payment"],200);
        }

        // MAKING SURE USERS ARE NOT PAYING LESSER THAN THE AMOUNT TO BE PAID
        if(!PaymentController::ValidateCheckoutAmount($details, ($txn->amount))){
            return response([
                'status'=>false,
                'message'=>"Checkout total doesn't correspond to the amount to be paid",
            ],200);
        }


        if(!$txn->status){
            return response(['status'=>false,'message'=>"unable to verify your payment"],200);
        }

        $details['reference'] = $txn->reference;
        $Order=OrderController::saveOrder($details,$user,$request->input('save_card'));

        $Transaction=new transaction();
        $Transaction->user_id=$user->id;
        $Transaction->amount=$txn->amount;
        $Transaction->p_reference=$txn->reference;
        $Transaction->reference=$txn->reference;
        $Transaction->content='Placed order';
        $Transaction->status=-1;
        $Transaction->save();


        return response(['status'=>true,'message'=>"order was placed successfully",'order'=>$Order,'misc'=>appController::getMiscAction($user->id)],200);
    }

    public static function saveOrder($details,$user,$save_card = 0){
        //==================== SORTING ORRDER ========================//
        $vendors=[];
        //=============== GETTING ALL VENDORS ======================
        foreach ($details['foods'] as $food){
            $item=item::find($food->item_id);
            $vendors[$item->vendor_id]['foods'][]=['item_id'=> $item->id,'price'=>$item->price,'quantity'=>$food->quantity];
        }
        //===========================================================
        $Order=[];
        //==================RECORDING SALES FOR EACH VENDOR==========
        foreach ($vendors as $key => $vendor){
            $reference= mt_rand(1000,9999);

            $total=OrderController::getSum($vendor['foods']);
            $vendor_details=vendor::find($key);
            $vendor_user=User::find($vendor_details->user_id);

            //saving order
            $Order= new order();
            $Order->user_id=$user->id;
            $Order->vendor_id=$key;
            $Order->method=$details['method'];
            $Order->street=$details['street'];
            $Order->transaction_id=0;
            $Order->data=json_encode($vendor['foods']);
            $Order->address=$details['address'];
            $Order->type=$details['delivery_type'];
            $Order->note=$details['note'];
            $Order->total=$total;
            $Order->delivery_fare=OrderController::getDeliveryFee($vendor_details->name,$details['delivery_fares']);
            $Order->reference=$reference;
            $Order->p_reference=$details['reference'];
            $Order->status=1;
            $Order->save();


            try{
                //Sending push notification
                if($vendor_user){
                    NotificationController::PushNotification('New Order',"You have a new booking",[$vendor_user->device_id],['type'=>'order','id'=>$Order->id]);
                }

                //Notify customer
                NotificationController::PushNotification('Order placed at '.$vendor_details->name,"Your order was received and will be processed shortly.",[$user->device_id],['type'=>'order','id'=>$Order->id]);

                $notification=new Notification();
                $notification->type='order';
                $notification->phone=$vendor_user->phone;
                $notification->model_id=$Order->id;
                $notification->email=$vendor_user->email;
                $notification->device=$vendor_user->device_id;
                $notification->title="NEW ORDER RECEIVED";
                $notification->text="A new order has been received for your kitchen";
                $notification->save();

                $notification=new Notification();
                $notification->type='admin';
                $notification->phone=$vendor_user->phone;
                $notification->model_id=$Order->id;
                $notification->email=$vendor_user->email;
                $notification->device=$vendor_user->device_id;
                $notification->title="NEW ORDER RECEIVED FOR ".$vendor_details->name;
                $notification->text="A new order has been received from ".$user->fullname;
                $notification->save();

            }catch (\Exception $exception){}
        }
        //========================================================



        if($save_card == '1'){
            PaymentController::SaveCard($details['reference']) ;
        }

        return $Order;
    }

    public static function getDeliveryFee($kitchen,$fees){
        $fare=0;
        foreach ($fees as $fee){
            if(in_array($kitchen,explode(',',$fee->res_names))){ //res_names='all restaurant names in thesame street'
                $fare=$fee->cost;
            }
        }
        return $fare;
    }
    public static function getSum($data){
        $sum=0;

        foreach ($data as $item){
//            Log::channel('stderr')->info((int) $item['quantity']);
            $sum+=((float) $item['price'] * (int) $item['quantity']);
        }
        return $sum;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(order $order)
    {
        //
    }
}
