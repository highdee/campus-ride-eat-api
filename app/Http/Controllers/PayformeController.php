<?php

namespace App\Http\Controllers;

use App\Payforme;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PayformeController extends Controller
{
    public function __construct(){
        $this->middleware('jwt.auth');
    }

    //
    public function get($id){
        $user=Auth::user();

        $data = Payforme::where('user_id', $user->id)->where('id', $id)->first();
        if(!$data) return response(null, 404);

        return response([
            'status'=>'success',
            'data'=> $data
        ], 200);
    }
    public function createToken(Request $request){
        $user=Auth::user();
        $amount = 0;
        $token = date('Ymdhis').mt_rand(1, 100).$user->id.mt_rand(100, 99999999);
        if($request->amount) $amount = $request->amount;

        $Plink = new Payforme();
        $Plink->user_id = $user->id;
        $Plink->amount = $amount;
        $Plink->type = 'cart';
        $Plink->token = $token;
        if($request->data){
            $Plink->data = $request->data;
        }
        $Plink->save();

        $text = $Plink->share_text;
        $Plink = Payforme::find($Plink->id);

        return response([
            'status'=>'success',
            'url'=>$Plink->url,
            'share_text'=>$text,
            'id'=> $Plink->id,
            'misc'=> appController::getMiscAction($user->id)
        ],200);
    }
}
