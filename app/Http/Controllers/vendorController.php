<?php

namespace App\Http\Controllers;

use App\balance;
use App\delivery;
use App\item;
use App\itemType;
use App\Notification;
use App\openingtime;
use App\order;
use App\rider;
use App\Services\UserService;
use App\transaction;
use App\User;
use App\vendor;
use App\vendorphotos;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use JWTAuth;

class vendorController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('jwt.auth')->except(['getItems','getPhotos']);
    }


    public function processOrder(Request $request,$order_id,$action){
        $user=auth::user();

        if(!$user->vendor && $request->action_by != 'customer'){
            return response(['message'=>"you are not a vendor"],400);
        }

        $order=order::where(['id'=>$order_id, 'user_id'=>$user->id])
            ->with('vendor')
            ->with('review','review.user')
            ->with('street')
            ->with('rider','rider.user')
            ->first();
        if($user->vendor){
            $order=order::where(['id'=>$order_id,'vendor_id'=>$user->vendor->id])
                ->with('vendor')
                ->with('review','review.user')
                ->with('street')
                ->with('rider','rider.user')
                ->first();
        }

        if(!$order){
            return response(['message'=>"Order was not found"],404);
        }

        if($order->status > 3 && $order->status != 10){
            return response(['message'=>"Order has already been processed"],404);
        }

        if($action == 'reject'){

            if($request->action_by == 'customer'){
                if(!$order->customer_can_cancel){
                    return response(['message'=>"Order can not be canceled at the moment"],404);
                }
                $order->status = -3;
                $order->cancel_comment = $request->input('comment');
                $order->canceled_at = date('Y-m-d H:i:s');
                $order->save();

                //notify customer
                NotificationController::PushNotification('You have canceled order with ref('.$order->reference.")", 'You payment will be reversed into your rideeat wallet', [$order->owner->device_id],['type'=>'order','id'=>$order->id]);

                $vendor_user = User::find($order->vendor()->first()->user_id);
                //nofity vendor
                NotificationController::PushNotification('Order canceled by '.$order->owner->first_name, $order->cancel_comment, [$vendor_user->device_id],['type'=>'order','id'=>$order->id]);
            }
            else{
                $order->status = -1;
                $order->cancel_comment = $request->input('comment');
                $order->canceled_at = date('Y-m-d H:i:s');
                $order->save();


                //notify customer
                NotificationController::PushNotification('Order canceled by '.$order->vendor->name, $order->cancel_comment, [$order->owner->device_id],['type'=>'order','id'=>$order->id]);

                $notification=new Notification();
                $notification->type='message';
                $notification->phone=$order->owner->phone;
                $notification->model_id=$order->owner->id;
                $notification->email=$order->owner->email;
                $notification->device=$order->owner->device_id;
                $notification->title='Order canceled by '.$order->vendor->name;
                $notification->text=strlen($order->cancel_comment) == 0 ? "We are sorry, this vendor is unable to process your order currently. Your payment will be reversed into your wallet." : $order->cancel_comment;
                $notification->save();
            }

            //  Reverse payment back to user
            PaymentController::creditUser(($order->total + $order->delivery_fare), $order->owner->id, "Payment reversal");

            return response([
                'status'=>true,
                'data'=>$order,
                'message'=>'Order was canceled successfully'
            ],200);
        }

        if($order->status < 1){
            return response([
                'status'=>false,
                'data'=>'',
                'message'=>'Order has been canceled'
            ],404);
        }

        //If this is just an acceptance of order
        if($order->status == 1){
            $order->status=10;
            $order->accepted_at=date('Y-m-d H:i:s');
            $order->save();

            //      CREDIT VENDOR
            vendorController::creditVendor($user->vendor->id, $order->total, $order->p_reference);

            //REMOVE CHARGES
            vendorController::debitVendor($user->vendor->id,(env('SERVICE_CHARGE')/100)*$order->total,true);

            $notification=new Notification();
            $notification->type='message';
            $notification->phone=$order->owner->phone;
            $notification->model_id=$order->owner->id;
            $notification->email=$order->owner->email;
            $notification->device=$order->owner->device_id;
            $notification->title='Order accepted by '.$order->vendor->name;
            $notification->text= "Your order was accepted, you will be notified once the order has been processed.";
            $notification->save();

            //notify customer
            NotificationController::PushNotification('Order accepted 😀', 'Your order at '.$order->vendor->name.' was accepted, you will be notified once the order has been processed.', [$order->owner->device_id],['type'=>'order','id'=>$order->id]);

            return response([
                'status'=>true,
                'data'=>$order,
                'message'=>'Order was accepted.'
            ],200);

        }

        //The remaining section will run when order status is 10( accepted)
        $order->status=2;
        $order->save();


        $address=$user->vendor->name.' '.substr($order->address,0,50);

        if($order->type == 'delivery'){
            $riders=[];

            $result=rider::where(['status'=>1,'occupied'=>0])->get();

            foreach ($result as $rider){
                $riders[]=$rider->user->device_id;
            }

            $delivery = new delivery();
            $delivery->user_id = $order->user['id'];
            $delivery->amount = $order->delivery_fare;
            $delivery->order_id = $order->id;
            $delivery->status = 0;
            $delivery->save();


            //notify riders
            NotificationController::PushNotification('New Delivery', $address, $riders, ['type'=>'delivery','id'=>$order->id]);

            $notification=new Notification();
            $notification->type='message';
            $notification->phone=$order->owner->phone;
            $notification->model_id=$order->owner->id;
            $notification->email=$order->owner->email;
            $notification->device=$order->owner->device_id;
            $notification->title='Order processed by '.$order->vendor->name;
            $notification->text= "Your order was processed and a rider will come and pick this order soon";
            $notification->save();



            return response([
                'status'=>true,
                'data'=>$order,
                'message'=>'Order was processed and a rider will come and pick this order soon'
            ],200);
        }
        else{
            $order->status=4;
            $order->save();

            $order=order::where(['id'=>$order->id,'vendor_id'=>$user->vendor->id])->with('vendor')->with('street')->with('rider','rider.user')->first();
            NotificationController::PushNotification($user->vendor->name,"Your order is ready for pickup.",[$order->user['device_id']],['type'=>'order','id'=>$order->id]);

            $notification=new Notification();
            $notification->type='message';
            $notification->phone=$order->owner->phone;
            $notification->model_id=$order->owner->id;
            $notification->email=$order->owner->email;
            $notification->device=$order->owner->device_id;
            $notification->title='Order processed by '.$order->vendor->name;
            $notification->text= "Your order was processed successfully. Kindly come for a pickup.";
            $notification->save();

            $notification=new Notification();
            $notification->type='admin';
            $notification->phone=$order->owner->phone;
            $notification->model_id=$order->owner->id;
            $notification->email=$order->owner->email;
            $notification->device=$order->owner->device_id;
            $notification->title='Order processed by '.$order->vendor->name;
            $notification->text= "Order is waiting for pickup";
            $notification->save();


            return response([
                'status'=>true,
                'data'=>$order,
                'message'=>'Order was processed successfully and a customer will come for a pickup soon'
            ],200);
        }

    }

    public static function creditVendor($vendor_id, $amount, $reference=0){
        //increasing vendor balance on the platform
        $balance=balance::where('vendor_id',$vendor_id)->first();
        if(!$balance){
            $balance=new balance();
            $balance->vendor_id=$vendor_id;
            $balance->amount=0;
        }
        $balance->amount+=$amount;
        $balance->save();

        $vendor = vendor::find($vendor_id);
        $device_id = $vendor->user()->first()->device_id;

        $Transaction=new transaction();
        $Transaction->user_id=$vendor->id;
        $Transaction->vendor_id=$vendor->id;
        $Transaction->amount=$amount;
        $Transaction->p_reference=$reference;
        $Transaction->reference=$reference;
        $Transaction->status=1;
        $Transaction->save();

        NotificationController::PushNotification($amount."NGN has landed in your account.",'Your balance has been credited with '.$amount.'NGN',[$device_id],['type'=>'balance','id'=>$vendor_id]);
    }

    public static function debitVendor($vendor_id,$amount,$notify){
        //increasing vendor balance on the platform
        $balance=balance::where('vendor_id',$vendor_id)->first();
        if(!$balance){
            $balance=new balance();
            $balance->vendor_id=$vendor_id;
            $balance->amount=0;
        }
        $balance->amount-=$amount;
        $balance->save();

        $vendor = vendor::find($vendor_id);
        $device_id = $vendor->user()->first()->device_id;
        $user = User::find($vendor->user_id);

        //saving transaction
        $Transaction=new transaction();
        $Transaction->user_id=$user->id;
        $Transaction->vendor_id=$vendor_id;
        $Transaction->amount=$amount;
        $Transaction->p_reference='Service_charge'.mt_rand(10,9999999);
        $Transaction->reference='Service_charge'.mt_rand(10,9999999);
        $Transaction->status=-1;
        $Transaction->save();

        if($notify) NotificationController::PushNotification($amount."NGN has debited from your account.",'A service charge of 10% has been deducted from your balance.',[$device_id],['type'=>'balance','id'=>$vendor_id]);
    }

    public function getTransactions($id){
        $transactions=transaction::where(['vendor_id'=>$id])->orderBy('id','desc')->paginate(15);

        return $transactions;
    }

    public function getVendor(Request $request){
        $user=auth::user();

        if(!$user->vendor){
            return response([],404);
        }

        $vendor=vendor::where('id',$user->vendor->id)->first();
        $vendor['recent_transactions']=$vendor->recent_transactions();

        return response([
            'status'=>200,
            'data'=>$vendor
        ]);
    }

    public function update_vendor(Request $request){
        $this->validate($request,[
            'type' => 'required',
            'name' => 'required|max:40',
            'phone' => 'required',
            'address' => 'required|max:100',
            'street'    =>'required'
        ]);

        $user=auth::user();
        $details=$request->input();

        if(!$user->vendor){
            return response([],404);
        }

        $vendor=vendor::find($user->vendor->id);
        $vendor->name=$details['name'];
        $vendor->phone=$details['phone'];
        $vendor->address=$details['address'];
        $vendor->street=$details['street'];
        $vendor->type=$details['type'];
        $vendor->save();


        return response([
            'status' => true,
            'message' => 'Your profile has been updated',
            'data'  => $vendor
        ]);
    }

    public function create(Request $request){
    $this->validate($request,[
        'type' => 'required',
        'name' => 'required|max:40',
        'phone' => 'required',
        'address' => 'required|max:100',
        'street'    =>'required'
    ]);

    $data=$request->input();
    $user=Auth::user();


    //VALIDATING THE VENDOR'S NAME IF IT DOESN'T EXIST BEFORE
    $vendor=vendor::where(['name'=>$data['name']])->first();
    if($vendor){
        if($vendor->user_id != $user->id && ($vendor->status  == 1 || $vendor->status  == 0)) {
            return response([
                'status' => false,
                'message' => 'A vendor already exist with this business name',
            ]);
        }
        if($vendor->user_id != $user->id && $vendor->status == -1){
            $vendor=new vendor();
            $vendor->user_id=$user->id;
        }
    }

    if(!$vendor){
        $vendor=new vendor();
        $vendor->user_id=$user->id;
    }

    $vendor->type=$data['type'];
    $vendor->name=$data['name'];
    $vendor->phone=$data['phone'];
    $vendor->street=$data['street'];
    $vendor->address=$data['address'];
    $vendor->uuid=substr(md5(rand()), 0, 7);
    $vendor->status=0;
    $vendor->save();

    $user=User::find($user->id);

    return response([
        'status' => true,
        'message' => 'Your request has been submitted',
        'data'  => $user
    ]);
}

    public function add_food(Request $request){
        $this->validate($request,[
            "name"=>"required",
            "price"=>"required",
            "type"=>"required",
            "time"=>"required",
            "description"=>"required|max:400",
        ]);

        $data=$request->input();

        $user=auth::user();
        $vendor=$user->vendor;
        $filename='';

        $itemType=itemType::where('title',$data['type'])->first();
        if(!$itemType){
            return response([
                'status'=>false,
                'message'=>'Food type not found.',
            ],200);
        }

        if($request->input("image")) {
            $file = $request->input('image');
            $file = base64_decode($file);
            $imagename = date('dmyhis') . mt_rand(0, 99999) . '' . str_replace('-', '', $request->input('filename'));
            file_put_contents(storage_path() . "/foods/large/" . $imagename, $file);
            $filename = env('APP_URL'). "/foods/large/" . $imagename;
        }else{
            return response([
                'status'=>false,
                'message'=>'No image was found',
            ],200);
        }

        $food=new item();
        $food->vendor_id=$vendor->id;
        $food->name=$data['name'];
        $food->price=$data['price'];
        $food->type=$data['type'];
        if(is_numeric($data['time'])){
            $food->time=$data['time'];
        }
        $food->item_type_id=$itemType->id;
        $food->filename=$filename;
        $food->description=$data['description'];
        $food->uuid=substr(md5(rand()), 0, 7);
        $food->save();

        return response([
            'status'=>true,
            'message'=>'Food item was added successfully',
            'data'=>$food
        ],200);
    }
    public function update_food(Request $request,$id){
        $this->validate($request,[
            'name'=>'required',
            'price'=>'required',
            'time'=>'required',
            'available'=>'required',
            'type'=>'required',
            'description'=>'required',
        ]);

        $data=$request->input();

        $user=auth::user();
        $vendor=$user->vendor;
        $filename='';

        $itemType=itemType::where('title',$data['type'])->first();
        if(!$itemType){
            return response([
                'status'=>false,
                'message'=>'Food type not found.',
            ],200);
        }

        $food=item::where('id',$id)->first();
        $prevValue = $food->available;
        $food->vendor_id=$vendor->id;
        $food->name=$data['name'];
        $food->price=$data['price'];
        $food->time=$data['time'];
        $food->available=$data['available'];
        $food->type=$data['type'];
        $food->item_type_id=$itemType->id;

        if($request->input("image")) {
            $file = $request->input('image');
            $file = base64_decode($file);
            $imagename = date('dmyhis') . mt_rand(0, 99999) . '' . str_replace('-', '', $request->input('filename'));
            file_put_contents(storage_path() . "/foods/large/" . $imagename, $file);
            $filename = env('APP_URL') . "/foods/large/" . $imagename;
            $food->filename=$filename;
        }

        $food->description=$data['description'];
        $food->save();

        try{
            if($prevValue != $data['available'] && $data['available'] == 1){
                $favorites = UserService::getFavourite($food);
                $devices = User::whereIn('id', $favorites)->pluck('device_id');

                NotificationController::PushNotification(
                    $vendor->name,
                    $food->name." is now available.",
                    $devices,
                    ['type'=>'food','restaurant_id'=>$vendor->id, 'item_id'=>$food->id]
                );
            }
        }catch (\Exception $exception){}

        return response([
            'status'=>true,
            'message'=>'Food item was updated successfully',
            'data'=>$food
        ],200);
    }

    public function getItems(Request $request,$id){
        $foods=item::where('vendor_id',$id)->whereNull('deleted_at');

        if($request->has('search')){
            $foods=$foods->where('name','like','%'.$request->input('search').'%');
        }

        $foods=$foods->paginate(15);
        return $foods;
    }

    public function getItem($id){
        $food=item::where('id',$id)->first();
        return $food;
    }

    public function getOrders(){
        $user=Auth::user();

        if($user->vendor){
            $orders=order::where('vendor_id',$user->vendor->id)->orWhere('user_id',$user->id);
        }
        else{
            $orders=order::where('user_id',$user->id);
        }
        $orders=$orders->with('vendor')->orderBy('id','desc')->paginate(15);
//        $orders=$orders->join('vendors', 'vendor_id','=','vendors.id')->orderBy('id','desc')->paginate(15);
        return $orders;
    }

    public function getOrder($id){
        $user=Auth::user();

        if($user->vendor){
            $order=order::where(['id'=>$id])->where(function ($query) use ($user) {
                $query->where(['vendor_id'=>$user->vendor->id])->orWhere(['user_id'=>$user->id]);
            })->with('street')->with('rider','rider.user');
        }else if($user->rider){
            $order=order::where(['id'=>$id])->with('street')->with('rider','rider.user');
        }else{
            $order=order::where(['id'=>$id])->with('street')->with('rider','rider.user');
        }

        $order=$order->with('vendor')->with('review','review.user')->first();

        if(!$order){
            return response([],404);
        }

//        $order['foods']=$order->foods();


        return $order;
    }


    public function deleteItem($id){
        $user=Auth::user();

        $food=item::where(['id'=>$id,'vendor_id'=>$user->vendor->id])->first();

        if($food){
            $food->delete();
            return response([
                'status'=>true,
                'message'=>'Food item was deleted successfully',
            ],200);
        }
    }

    public function update_photos(Request $request){
        if($request->input("images")) {
            $images=json_decode($request->input('images'));

            $user=auth::user();

            if(!$user->vendor){
                return response([],404);
            }

            $imgs=[];
            foreach ($images as $file){
                $file = base64_decode($file);
                $imagename = date('dmyhis') . mt_rand(0, 99999) . '.jpg';
                file_put_contents(storage_path() . "/vendors_images/" . $imagename, $file);
                $filename = env('APP_URL') . "/vendors_images/" . $imagename;

                $photo=new vendorphotos();
                $photo->vendor_id=$user->vendor->id;
                $photo->filename=$filename;
                $photo->save();

                $imgs[]=$photo;
            }

            return response([
                'status'=>true,
                'message'=>'Images uploaded',
                'data'=>$imgs
            ],200);
        }
    }

    public function getPhotos($id){
        $foods=vendorphotos::where('vendor_id',$id)->paginate(15);

        return $foods;
    }

    public function update_picture(Request $request){
        $user=auth::user();

        if(!$user->vendor){
            return response([],404);
        }

        if($request->input("image")){
            $file = $request->input('image');
            $file = base64_decode($file);
            $imagename = date('dmyhis') . mt_rand(0, 99999) . '' . str_replace('-', '', $request->input('filename'));
            file_put_contents(storage_path() . "/profile_pictures/" . $imagename, $file);
            $filename = env('APP_URL') . "/profile_pictures/" . $imagename;

            $vendor=vendor::find($user->vendor->id);
            $vendor->filename=$filename;
            $vendor->save();

            return response([
                'status'=>true,
                'message'=>'Image uploaded',
                'data'=>$vendor
            ],200);
        }

        return response([
            'status'=>false,
            'message'=>'null'
        ],402);
    }

    public function update_banner(Request $request){
        $user=auth::user();

        if(!$user->vendor){
            return response([],404);
        }

        if($request->input("image")){
            $file = $request->input('image');
            $file = base64_decode($file);
            $imagename = date('dmyhis') . mt_rand(0, 99999) . '' . str_replace('-', '', $request->input('filename'));
            file_put_contents(storage_path() . "/profile_pictures/" . $imagename, $file);
            $filename = env('APP_URL') . "/profile_pictures/" . $imagename;

            $vendor=vendor::find($user->vendor->id);
            $vendor->banner=$filename;
            $vendor->save();

            return response([
                'status'=>true,
                'message'=>'Image uploaded',
                'data'=>$vendor
            ],200);
        }

        return response([
            'status'=>false,
            'message'=>'null'
        ],402);
    }

    public function update_opening_closing(Request $request){
        $this->validate($request,[
            'day'=>'required',
        ]);
        $user=auth::user();

        if(!$user->vendor){
            return response([],404);
        }

        $data=$request->input();
        $opcl=openingtime::where(['vendor_id'=>$user->vendor->id,'day'=>$data['day']])->first();

        if(!$opcl){
            $opcl=new openingtime();
            $opcl->day=$data['day'];
            $opcl->vendor_id=$user->vendor->id;
        }

        if(isset($data['opening'])){
            $opcl->opening=$data['opening'];
        }
        if(isset($data['closing'])){
            $opcl->closing=$data['closing'];
        }

        $opcl->save();

        $vendor=vendor::find($user->vendor->id);

        return response([
            'status'=>true,
            'message'=>'Time update',
            'data'=>$vendor
        ]);

    }


}
