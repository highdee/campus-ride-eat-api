<?php

namespace App\Http\Controllers;

use App\Notification;
use App\rider;
use App\streets;
use App\User;
use App\vendor;
use App\VendorDelivery;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class VendorDeliveryController extends Controller
{
    public function getOrders(){
        $user=Auth::user();
        if($user->vendor != null){
            return response([
                'data'=>VendorDelivery::where('vendor_id', $user->vendor->id)->orderBy('id', 'desc')->paginate(15)
            ], 200);
        }

        return response([
            'data'=>VendorDelivery::where('rider_id', $user->rider->id)->orderBy('id', 'desc')->paginate(15)
        ], 200);
    }

    public function getAvailableOrders(){
        $user=Auth::user();
        if($user->rider == null){
            return response([
                'status'=>false,
                'message'=>"Empty"
            ], 400);
        }

        return response([
            'data'=>VendorDelivery::where('picked_at', null)->where('canceled_at', null)->orderBy('id', 'desc')->paginate(15)
        ], 200);
    }

    public function getOrder(VendorDelivery $order){

        return response([
            'data'=>$order
        ], 200);
    }

    public function createOrder(Request $request, streets $source, streets $destination){
        $this->validate($request, [
            "source_address"=>"required",
            "destination_address"=>"required",
            "description"=>"required",
        ]);

        $cost_row = $this->getCost($source, $destination);
        if(!$cost_row){
            return response([
                'status'=>false,
                'message'=>"Delivery is not currently available at this location"
            ], 400);
        }

        $user=Auth::user();
        if($user->vendor == null){
            return response([
                'status'=>false,
                'message'=>"You are not allowed to create delivery order"
            ], 400);
        }

        $details=$request->input();

        $Order = new VendorDelivery();
        $Order->vendor_id = $user->vendor->id;
        $Order->uuid = mt_rand(1000,99999);
        $Order->source_street_id = $source->id;
        $Order->source_address = $details['source_address'];
        $Order->destination_street_id = $destination->id;
        $Order->destination_address = $details['destination_address'];
        $Order->price = $cost_row->cost;
        $Order->description = $details['description'];
        $Order->save();

        // CHARGE USER
        $details['reference']=mt_rand(100000,999999999999).'RT';
        if($details['payWith'] == 'p_wallet'){
            $result = PaymentController::debitUser(($Order->price), $user->id, "(".$details['reference'].") Order placed");
        }else{
            $result = PaymentController::debitUser(($Order->price), $user->vendor->id, "(".$details['reference'].") Order placed", null, true);
        }

        if(!$result){
            return response(['status'=>false,'message'=>"Sorry, you don't have sufficient money in your wallet to perform this transaction."],400);
        }

        //Notify customer
        NotificationController::PushNotification('Delivery Order Created Successfully ',"Your order was received and rider will arrive shortly.",[$user->device_id],['type'=>'vorder','id'=>$Order->id]);

        //notify riders
        $result = rider::where(['status'=>1,'occupied'=>0])->get();
        $riders=[];
        foreach ($result as $rider){
            $rd = User::find($rider->user_id);
            $riders[]=$rd->device_id;
        }

        $address = ($Order->source_street->name.', '.$Order->source_address);

        NotificationController::PushNotification('New Delivery Order', $address.'. '.$Order->description, $riders, ['type'=>'vdelivery','id'=>$Order->id]);

        $notification=new Notification();
        $notification->type='admin';
        $notification->phone=$user->phone;
        $notification->model_id=$Order->id;
        $notification->email=$user->email;
        $notification->device=$user->device_id;
        $notification->title="NEW DELIVERY ORDER RECEIVED";
        $notification->text="A new order has been received from ".$user->vendor->name;
        $notification->save();



        return response([
            'status'=>true,
            'message'=>"Order was placed successfully",
            'data'=>$Order
        ], 200);
    }

    public function getCostAction(streets $source, streets $destination){
        $cost_row = $this->getCost($source, $destination);
        if(!$cost_row){
            return response([
                'status'=>false,
                'message'=>"Delivery is not currently available at this location"
            ], 400);
        }


        return response([
            'status'=>true,
            'data'=>[
                'source_area'=>$source->name,
                'source'=>$source->id.'',
                'destination_area'=>$destination->name,
                'destination'=>$destination->id.'',
                'costs'=>[
                    ["$source->name to $destination->name", $cost_row->cost]
                ],
                'total'=>($cost_row->cost)
            ]
        ], 200);
    }

    public function getCost($source, $destination){
        $cost_row = DB::table('source_delivery_sytem_destination')->where([
            'source_id'=>$source->id,
            'destination_id'=>$destination->id,
            'delivery_id'=>1 //bike
        ])->first();

        return $cost_row;
    }

    public function pickOrder(Request $request, VendorDelivery $order){
        $user=Auth::user();
        if($user->rider == null){
            return response([
                'status'=>false,
                'message'=>"You are not allowed to pick this order"
            ], 400);
        }

        if($user->rider->occupied == 1){
            return response([
                'status'=>false,
                'message'=>"You have a pending order. Please deliver first before picking another"
            ], 400);
        }

        if($order->picked_at != null){
            return response([
                'status'=>false,
                'message'=>"Order has already been picked up."
            ], 400);
        }

        $order->rider_id = $user->rider->id;
        $order->picked_at = date("Y-m-d H:i:s");
        $order->save();

        $vendor = User::where('id', $order->vendor_id)->first();

        //Notify customer
        NotificationController::PushNotification('Delivery order picked by rider',"Your order has been picked up by ".$user->rider->name, [$vendor->device_id],['type'=>'vorder','id'=>$order->id]);

        return response([
            'status'=>true,
            'message'=>"Order was picked up successfully",
            'data'=>$order
        ], 200);
    }

    public function completeOrder(VendorDelivery $order, $code){
        $user=Auth::user();
        if($user->rider == null){
            return response([
                'status'=>false,
                'message'=>"You are not a rider"
            ], 400);
        }

        if($order->rider_id != $user->rider->id){
            return response([
                'status'=>false,
                'message'=>"You are not authorized to complete order"
            ], 400);
        }

        if($order->picked_at == null){
            return response([
                'status'=>false,
                'message'=>"Order cannot be completed"
            ], 400);
        }

        if($order->uuid != $code){
            return response([
                'status'=>false,
                'message'=>"Order reference is not correct"
            ], 400);
        }


        $order->completed_at = date("Y-m-d H:i:s");
        $order->save();

        $vendor = User::where('id', $order->vendor_id)->first();

        //Notify customer
        NotificationController::PushNotification('Delivery order completed',"Order delivered to".$order->destination_address, [$vendor->device_id, $user->device_id],['type'=>'vorder','id'=>$order->id]);

        return response([
            'status'=>true,
            'message'=>"Order was delivered successfully",
            'data'=>$order
        ], 200);
    }

    public function cancelOrder(VendorDelivery $order){
        error_log('not vendor3232');
        $user=Auth::user();

        if($user->vendor == null){
            error_log('not vendor');

            return response([
                'status'=>false,
                'message'=>"You are not a vendor"
            ], 400);
        }

        if($order->vendor_id != $user->vendor->id){
            error_log('not the owner');
            return response([
                'status'=>false,
                'message'=>"You are not authorized to cancel order"
            ], 400);
        }

        if($order->completed_at != null){
            error_log('not complered');
            return response([
                'status'=>false,
                'message'=>"Order cannot be canceled"
            ], 400);
        }

        $order->canceled_by = 'vendor';
        $order->canceled_at = date("Y-m-d H:i:s");
        $order->save();

        vendorController::creditVendor($user->vendor->id, $order->price, $order->p_reference);

        //Notify customer
        NotificationController::PushNotification('Delivery order Canceled',"Order has been canceled by you", [$user->device_id],['type'=>'vorder','id'=>$order->id]);

        error_log('response sending');
        return response([
            'status'=>true,
            'message'=>"Order was delivered successfully",
            'data'=>$order
        ], 200);
    }
}
