<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactUsController extends Controller
{
    public function message(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required'
        ]);

        $name = $request->name;
        $email = $request->email;
        $message = $request->message;

        $data = [
            'name' => $name,
            'email' => $email,
            'content' => $message
        ];

        Mail::send('mails.contact-us', $data, function ($m) {
            $m->to('rideeat080@gmail.com')->subject('Contact Us Email');
        });

        session()->flash('success', 'Thanks for contacting us, We will get back to you as soon as possible');
        return redirect()->back();
    }
}
