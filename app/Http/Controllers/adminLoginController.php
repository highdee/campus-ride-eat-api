<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class adminLoginController extends Controller
{
    public function getlogin(){
        return view('admin.login');
    }
    public function login(Request $request){
        $this->validate($request, [
            'username'=>'required|string',
            'password'=>'required',
        ]);
        if(Auth::guard('admin')->attempt(['username'=>$request->username,'password'=>$request->password])){
            return redirect()->intended('campus-admin/');
        }else{
            session()->flash('msg', 'Username and password Incorrect');
            session()->flash('status', 'error');

            return redirect('campus-admin/login');
        }
    }
}
