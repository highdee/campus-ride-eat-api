<?php

namespace App\Http\Controllers;

use App\bankDetails;
use App\Mail\sendAdminNotification;
use App\Services\MailChimpService;
use App\User;
use App\vendor;
use App\Withdrawal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use JWTAuth;

class userController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function logEvent(Request $request){
        $user=Auth::user();

        try{
            $ml = new MailChimpService();
            $ml->addEvent($user->email, $request->name);
        }catch (\Exception $exception){}

        return response([], 200);
    }

    public function getUser(Request $request){
        $user=Auth::user();
        if(!empty($request->query('device'))){
            $user->device_id=$request->query('device');
            $user->save();
        }
        return $user;
    }

    public function updateDetails(Request $request){
        $this->validate($request,[
            'name' => 'required',
            'phone' => 'required',
        ]);

        $details=$request->input();
        $user=Auth::user();

        $user->phone = $details['phone'];
        $user->fullname = $details['name'];
        $user->save();


        return response([
            'status' => true,
            'message' => 'Your details was updated successfully.',
            'user'  => User::find(Auth::user()->id),
        ]);

    }

    public function updateBankDetails(Request $request){
        $this->validate($request,[
            'account_number' => 'required',
            'name' => 'required',
            'bank' => 'required',
        ]);

        $details=$request->input();
        $user=Auth::user();

        $bank_details=bankDetails::where('user_id',$user->id)->first();
        if(!$bank_details){
            $bank_details=new bankDetails();
        }

        $bank_details->user_id=$user->id;
        $bank_details->name=$details['name'];
        $bank_details->bank=$details['bank'];
        $bank_details->number=$details['account_number'];
        $bank_details->save();

        $user=User::find($user->id);

        if($details['withdraw'] == '1'){
            $w_request = new Withdrawal();
            if($user->rider != null){
                $w_request->rider_id = $user->rider->id;
                $w_request->amount = $user->rider->balance;
            }
            else{
                $w_request->vendor_id = $user->vendor->id;
                $w_request->amount = $user->vendor->balance;
            }
            $w_request->save();

            Mail::to(env('MAIL_USERNAME'))->send(new sendAdminNotification([
                'content' => '<p>A CUSTOMER HAS REQUESTED FOR A WITHDRAWAL OF <h3>'. $w_request->amount .'</h3></p>'
            ]));

            return response([
                'status' => true,
                'message' => 'Your withdrawal request was sent successfully.',
                'data'  => $bank_details,
                'w_request' => $w_request
            ]);
        }

        return response([
            'status' => true,
            'message' => 'Your bank details was updated successfully.',
            'data'  => $bank_details,
            'w_request' => null
        ]);
    }

    public function getWithdrawalRequest(){
        $user=Auth::user();

        $w_requests=null;
        $balance = 0;

        if($user->rider != null){
            $w_requests = Withdrawal::where('rider_id',$user->rider->id)->where('accepted_at',null)->where('rejected_at',null)->first();
            $balance = $user->rider->balance;
        }elseif ($user->vendor != null){
            $w_requests = Withdrawal::where('vendor_id',$user->vendor->id)->where('accepted_at',null)->where('rejected_at',null)->first();
            $balance = $user->vendor->balance;
        }

        return response([
            'balance' => $balance,
            'withdrawal_limit' => 500,
            'data' => $w_requests
        ],200);
    }

    public function getNotificationStatus($v_id){
        $user=Auth::user();

        return response([
            'opcl'=> (bool)DB::table('user_vendor')
                ->where('user_id', $user->id)
                ->where('vendor_id', $v_id)
                ->where('type', 'opcl')
                ->where('deleted_at', null)->first(),

            'menu'=> (bool)DB::table('user_vendor')
                ->where('user_id', $user->id)
                ->where('vendor_id', $v_id)
                ->where('type', 'menu')
                ->where('deleted_at', null)->first()
        ]);
    }

    public function toggleUserVendorNotification(vendor $vendor, $command, $type){
        $user=Auth::user();

        DB::table('user_vendor')
            ->where('user_id', $user->id)
            ->where('vendor_id', $vendor->id)
            ->where('type', $type)
            ->where('deleted_at', null)
            ->orderBy("id", "desc")
            ->update([
                'deleted_at'=>date('Y-m-d H:i:s'),
                'removed_by'=>'user'
            ]);

        if($command == 'start'){
            DB::table('user_vendor')->insert([
                'user_id'=>$user->id,
                'vendor_id'=>$vendor->id,
                'added_by'=>'user',
                'type'=>$type,
                'created_at'=>date('Y-m-d H:i:s')
            ]);
        }

        return response([
            'status'=>true,
            "message"=>"Update was successful",
//            'user'=> User::find($user->id)
        ]);
    }
}
