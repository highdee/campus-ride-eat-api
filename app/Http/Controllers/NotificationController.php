<?php

namespace App\Http\Controllers;


use App\Mail\customerNotification;
use App\User;
use Illuminate\Support\Facades\Mail;

class NotificationController extends Controller
{
    //

    public static function PushNotification($title,$text,$to,$payload=[]){

        $url = 'https://fcm.googleapis.com/fcm/send';
        $notification = array('title' =>$title, 'body' => substr($text,0,strlen($text) > 100 ? 100:strlen($text)));
        $headers = array(
            'Authorization: key=',
            'Content-type: Application/json'
        );

        $fields = array(
            'registration_ids' => $to,
            'data' => $message = array(
                'data'=>$payload,
                'click_action' => 'FLUTTER_NOTIFICATION_CLICK',
            ),
            'priority'=>'high',
            'notification' => $notification,
            'android' => [
                'notification'=>[
                    'channel_id'=>'mychannel_campus',
                ]
            ]
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_exec($ch);
        curl_close($ch);


        NotificationController::dispatchToEmails($title, $text, $to);
    }

    public static function dispatchToEmails($title, $text, $to){
        try {
            $email_data = [
                'title'=>$title,
                'content'=>$text,
            ];
            $email_users = User::whereIn('device_id', $to)->pluck('email');

            Mail::bcc($email_users)->send(new customerNotification($email_data));
        }catch (\Exception $msg){}
    }
}
