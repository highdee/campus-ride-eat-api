<?php

namespace App\Http\Controllers;

use App\item;
use App\Mail\resetMail;
use App\Mail\verificationEmail;
use App\Services\MailChimpService;
use App\User;
use App\vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use JWTAuth;
class loginController extends Controller
{
    // public function get(){
    //     //return User::all();
    //     return env('APP_URL');
    // }


    public function sortFoods(Request $request){
        $this->validate($request, [
            'old_sort'=>'required',
            'new_sort'=>'required',
        ]);

        $old_sort = item::where('sort_id', $request->old_sort)->first();
        $old_sort->sort_id = $request->new_sort;

        $new_sort = item::where('sort_id', $request->new_sort)->first();
        $new_sort->sort_id = $request->old_sort;

        if($old_sort->save() && $new_sort->save()){
            $data = item::where('featured', 1)->orderBy('sort_id', 'asc')->get();
            return response([
                'status'=>true,
                'data'=>$data
            ]);

        }
        return response([
            'status'=>false,
        ]);
    }
    public function sortVendors(Request $request){
        $this->validate($request, [
            'old_sort'=>'required',
            'new_sort'=>'required',
        ]);

        $old_sort = vendor::where('sort_id', $request->old_sort)->first();
        $old_sort->sort_id = $request->new_sort;

        $new_sort = vendor::where('sort_id', $request->new_sort)->first();
        $new_sort->sort_id = $request->old_sort;

        if($old_sort->save() && $new_sort->save()){
            $data = DB::table('vendors')->where('featured', 1)
                                            ->where('deleted_at', null)
                                            ->orderBy('sort_id', 'asc')->get();
            return response([
                'status'=>true,
                'data'=>$data
            ]);
        }
        return response([
            'status'=>false,
        ]);
    }

    public function create_account(Request $request){
        $this->validate($request,[
            'email'=>'required|email|unique:users',
            'name'=>'required',
            'phone'=>'required',
            'password'=>'required'
        ]);

        $details=$request->input();


        $User=new User();
        $User->fullname=$details['name'];
        $User->email=$details['email'];
        $User->phone=$details['phone'];
        $User->device_id=isset($details['device_id']) ? $details['device_id']:'';
        $User->status=1;
        $User->password=bcrypt($details['password']);
        $User->remember_token=mt_rand(1000,9999);



//        $link=env('APP_URL')."/verify-account/".$User->remember_token;
//        Mail::to($details['email'])->sendNow(new verificationEmail([
//            'code'=>$User->remember_token
//        ]));

        $User->save();

        try {
            $MLS = new MailChimpService();
            $MLS->addMemberToMailChimpList($User->email, $User->lastname, $User->firstname);
        }catch (\Exception $exception){}

//        LOGIN USER AFTER REGISTERATION
        $credentials = $request->only('email', 'password');

        if($token=JWTAuth::attempt($credentials)){
            $user=User::where('id',auth::user($token)->id)->first();
            $user->save();
            return response()->json([
                'status'=>true,
                'msg'=>"Login Successfull",
                'token'=>$token,
                "user"=>$user
            ]);
        }else{
            return response()->json(
                [
                    "status"=>false,
                    "message"=>"Invalid login details"
                ]
            );
        }
//

        return response()->json([
            'status'=>true,
            'msg'=>"Your registration was successfull"
        ]);

    }

    public function verify_account($token){
        if(!isset($token)){
            return response()->json([
                'status'=>false,
                'msg'=>"Token not found"
            ]);
        }


        $User=User::where('remember_token',$token)->first();

        if(!$User){
            return response()->json([
                'status'=>false,
                'msg'=>"Account not found"
            ]);
        }

        $User->email_verified_at=date('Y-m-d h:i:s');
        $User->remember_token=null;
        $User->save();



        return response()->json([
            'status'=>true,
            'msg'=>"Account verified"
        ]);
    }

    public function login(Request $request){
        $this->validate($request,[
            'email'=>'required|email',
            'password'=>'required'
        ]);

        $credentials = $request->only('email', 'password');

        if($token=JWTAuth::attempt($credentials)){
            $details=$request->input();
            $user=User::where('id',auth::user($token)->id)->first();
            $user->device_id=isset($details['device_id']) ? $details['device_id']:$user->device_id ;
            $user->save();
            return response()->json([
                'status'=>true,
                'msg'=>"Login Successful",
                'token'=>$token,
                "user"=>$user,
                'misc'=>appController::getMiscAction($user->id)
            ]);
        }else{
            return response()->json(
                [
                    "status"=>false,
                    "message"=>"Invalid login details"
                ]
            );
        }
    }

    public function forget_password(Request $request){
        $this->validate($request,[
            'email'=>'required',
        ]);

        $User=User::where('email',$request->input('email'))->first();

        if(!$User){
            return response()->json([
                'status'=>false,
                'msg'=>"Account not found"
            ]);
        }

        $User->remember_token=mt_rand(0100,9999);

        $User->save();

        $token = $User->remember_token;

        Mail::to($User->email)->sendNow(new resetMail([
            'token'=> $token,
            'email'=> $User->email,
        ]));

        return response()->json([
            'status'=>true,
            'msg'=>"Reset mail has been sent"
        ]);
    }

    public function verify_password(Request $request){
        $token=$request->query('token');

        if(!isset($token)){
            return response()->json(['status'=>false,'msg'=>"Token not found" ]);
        }

        $User=User::where('remember_token',$token)->first();

        if(!$User){
            return response()->json([
                'status'=>false,
                'msg'=>"Expired link"
            ]);
        }

        return response()->json(['status'=>true,'msg'=>"correct token" ]);
    }

    public function reset_password(Request $request){
        $this->validate($request,[
            'password'=>'required|confirmed',
            'token'=>'required'
        ]);

        $User=User::where('remember_token',$request->input('token'))->first();

        if(!$User){
            return response()->json([
                'status'=>false,
                'msg'=>"Account not found"
            ]);
        }

        $User->password=bcrypt($request->input('password'));
        $User->remember_token=mt_rand(99999,99999999).str_random(12);
        $User->save();

        return response()->json(['status'=>true,'msg'=>"Password reset was successful" ]);
    }
}
