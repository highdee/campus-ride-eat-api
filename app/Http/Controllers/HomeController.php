<?php

namespace App\Http\Controllers;

use App\Payforme;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth')->except(['payforme']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function payforme(Request $request, $token){
        $Plink = Payforme::where('token', $token)->first();
        if(!$Plink){
            return view('pages.payforme')->with([
                'ip_address'=>'',
                'amount'=>0,
                'ref'=>'',
                'status'=>'not_found',
            ]);
        }
        if($Plink->status == 'completed'){
            return view('pages.payforme')->with([
                'ip_address'=>'',
                'amount'=>0,
                'ref'=>'',
                'status'=>'completed',
            ]);
        }
        $token = 'payforme_'.$Plink->token.'_'.date('Ymdhis').mt_rand(100,99999);
        $user = User::find($Plink->user_id);
        $splitted = explode(' ', $user->fullname);
        $name = $splitted[0];

        return view('pages.payforme')->with([
            'ip_address'=>$request->ip(),
            'name'=>$name,
            'amount'=>$Plink->amount,
            'ref'=>$token,
            'status' => 'good'
        ]);
    }



}
