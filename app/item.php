<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class item extends Model
{
    //
    use SoftDeletes;

    public function __construct(array $attributes = [])
    {
//        error_log('Logging item');
    }

    public function getPriceAttribute(){
        return filter_var($this->attributes['price'], FILTER_SANITIZE_NUMBER_FLOAT);
    }

    protected $appends =['share_text', 'shareable_link', 'rating','vendor_details', 'item_type'];

    public function getShareTextAttribute(){
        $vendor = vendor::find($this->vendor_id);

        $text = 'Check out '.$this->name.' by '.$vendor->name.' on Rideeats Mobile app';
        $text = $text.' '.$this->getShareableLinkAttribute();

        return $text;
    }
    public function getShareableLinkAttribute(){
        $vendor = vendor::find($this->vendor_id);

        $link = '';
        if($vendor) $link = $vendor->shareable_link.'/'.$this->uuid;

        return str_replace(' ','-', $link);
    }

    public function getVendorDetailsAttribute(){
        $vendor=vendor::where('id', $this->vendor_id)->with('reviews','reviews.user')->first();
        if($vendor){
            $street=streets::find($vendor->street);
            return [
                'name'=>$vendor->name,
                'street'=>$street,
            ];
        }else{
            return [];
        }
    }
    public function getRatingAttribute(){
        return '0.0';
    }
    public function item_type(){
        return $this->belongsTo('App\itemType','item_type_id','id');
    }
    public function getItemTypeAttribute(){
        return $this->item_type()->first();
    }
}
