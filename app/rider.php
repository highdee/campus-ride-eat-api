<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class rider extends Model
{
    //

    use SoftDeletes;

    protected $fillable=['status'];

    protected $appends =['available_delivery','stats','opcl','rating','balance','delivery_count','transaction_count'];


    public function recent_transactions(){
        $txns=transaction::where(['rider_id'=>$this->id])->limit(10)->get();
        return $txns;
    }

    public function getAvailableDeliveryAttribute(){
        return delivery::where(['status'=>0])->count();
    }
    public function getDeliveryCountAttribute(){
        $count=$this->hasMany('App\delivery')->count();
        return $count;
    }

    public function getTransactionCountAttribute(){
        $count=$this->hasMany('App\transaction')->count();
        $count=$count < 100 ? $count : '99+';
        return $count;
    }

    public function getBalanceAttribute(){
        $amount=0;
        $balance=$this->hasOne('App\balance','rider_id','id')->first();
        if($balance){
            $amount=$balance->amount;
        }
        return $amount;
    }

    public function getRatingAttribute(){
        return '0.0';
    }

    public function getOpclAttribute(){
//        return [];
        return $this->opcl()->get();
    }
    public function opcl(){
        return $this->hasMany('App\openingtime','rider_id','id');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
    public function owner(){
        return $this->belongsTo('App\User');
    }
    public function getStatsAttribute(){
        $foods=item::where(['vendor_id'=>$this->id])->count();
        $photos=vendorphotos::where(['vendor_id'=>$this->id])->count();

        return [
            'food'=>0,
            'photos'=>0
        ];
    }

    public function transaction(){
        return $this->hasMany('App\transaction');
    }
    public function orders(){
        return $this->hasMany('App\order');
    }
    public function deliveries(){
        return $this->hasMany('App\delivery','rider_id','id');
    }
}
