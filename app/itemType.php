<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class itemType extends Model
{
    //

    protected $appends=['itemsCount'];


    public function getItemsCountAttribute(){
        return $this->items()->count();
    }

    public function items(){
        return $this->hasMany('App\item','item_type_id','id');
    }
}
