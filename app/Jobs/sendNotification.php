<?php

namespace App\Jobs;

use App\item;
use App\Mail\customerNotification;
use App\Mail\sendOrderNotification;
use App\Notification;
use App\order;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class sendNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $data;
    public function __construct($data)
    {
        $this->data = $data;
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $notification = Notification::find($this->data->id);

        if($notification->model_id == null && $notification->type != 'admin'){
            return false;
        }

        if($notification->type == 'order'){
            $order = order::where('id',$notification->model_id)->with('vendor')->first();
            $vendor = $order->vendor;
            $vendor_user = User::find($vendor->user_id);

            $order_data = json_decode($order->data);

            $data = [];

            foreach ($order_data as $ord){
                $item = item::find($ord->item_id);
                $data[] = [
                    'qty' => $ord->quantity,
                    'amount' => number_format($ord->price, 2),
                    'title' => $item->name,
                ];
            }

            $payload = [
                'title' => $notification->title,
                'body' => $notification->text,
                'note' => $order->note,
                'date' => date('d, M-Y H:i A', strtotime($notification->created_at)),
                'method' => $order->method == 0 ? 'PICKUP ORDER' : 'RIDER DELIVERY',
                'total' => number_format($order->total, 2),
                'items' => $data,
                'customer' => false
            ];

            // Notify vendor
            Mail::to($vendor_user->email)->sendNow(new sendOrderNotification($payload));
            // echo 'Notification sent to '.$vendor_user->email."\n";

            // Notify Customer
            $customer_user = User::find($order->user_id);
            $payload['title'] = "Order placed at ".$vendor->name;
            $payload['body'] = "Your order was received and will be processed shortly";
            $payload['customer'] = true;
            Mail::to($customer_user->email)->sendNow(new sendOrderNotification($payload));

        }

        if($notification->type == 'message'){
            $payload = [
                'title' => $notification->title,
                'content' => $notification->text,
                'date' => date('d, M-Y H:i A', strtotime($notification->created_at))
            ];

            $user = User::find($notification->model_id);

            Mail::to($user->email)->sendNow(new customerNotification($payload));
            //echo 'Notification sent to '.$user->email."\n";
        }

        if($notification->type == 'admin'){
            $payload = [
                'title' => $notification->title,
                'content' => $notification->text,
                'date' => date('d, M-Y H:i A', strtotime($notification->created_at))
            ];
            Mail::to(env('MAIL_USERNAME'))->sendNow(new customerNotification($payload));
        }

        $notification->sent_at = date('Y-m-d H:i:s');
        $notification->save();
    }
}
