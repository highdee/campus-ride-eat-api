<?php

namespace App\Providers;

use App\item;
use App\Notification;
use App\Observers\ItemObserver;
use App\Observers\NotificationObserver;
use App\Observers\ReviewObserver;
use App\Review;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Notification::observe(NotificationObserver::class);
        Review::observe(ReviewObserver::class);
        item::observe(ItemObserver::class);
    }
}
