<?php

namespace App;

use Illuminate\Support\Facades\DB;
use function GuzzleHttp\Psr7\str;
use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    //switch(status){
    //            case -3:
//              return 'canceled by customer'
    //            case -2:
//              return 'canceled by admin'
//            case -1:
//              return 'canceled by vendor'
    //        case 1:
    //          return 'Pending';
    //        case 2:
    //          return 'Processed';
    //        case 3:
    //          return 'On the way';
    //        case 4:
    //          return 'Completed';
    //        case 5:
    //          return 'Completed By Admin';
//            case 10:
//            return 'Accepted'; but not processed
    //        default:
    //          return 'Unknown';
    //      }

    protected $appends=['time_left_in_seconds', 'customer_can_cancel', 'order_status_color','order_status_for_customer', 'order_status','time','foods','user','delivery','food_names'];

    protected $casts=[
        'created_at' => 'datetime:D, d-M-Y',
    ];

    public function getTimeLeftInSecondsAttribute($order_delay_time = null){
        $result = 0;
        if($order_delay_time == null){
            $order_delay_time = $this->getHighestFoodTime();
        }
//        $order_delay_time = DB::table('config')->where('name', 'delay_time')->orderBy('id', 'desc')->first();

        $delay_time = $order_delay_time;
        $created = strtotime($this->accepted_at ? $this->accepted_at : $this->created_at);
        $time = strtotime(date('Y-m-d H:i:s'));
        $diff = $time - $created;
        $result = ($delay_time * 60) - $diff;

        return $result;
    }

    public function getCustomerCanCancelAttribute(){
        if($this->status  == 4){
            return false;
        }
        $result = false;
        $diff_seconds = 0;
        $order_delay_time = DB::table('config')->where('name', 'delay_time')->orderBy('id', 'desc')->first();
        if($order_delay_time){
            $diff_seconds = $this->getTimeLeftInSecondsAttribute($order_delay_time->value);
        }

        if($this->status > 1){
            $diff_seconds = $this->getTimeLeftInSecondsAttribute();
        }

        if($diff_seconds < 0){
            $statuses = [1, 2, 10];
            if(in_array($this->status, $statuses)){
                $result = true;
            }
        }
        return $result;
    }

    public function getOrderStatusAttribute(){
        switch($this->status){
            case -3:
                return 'canceled by customer';
            case -2:
              return 'canceled by rideeat';
            case -1:
              return 'canceled by vendor';
            case 1:
              return 'Pending';
            case 2:
              return 'Processed';
            case 3:
              return 'On the way';
            case 4:
              return 'Completed';
            case 5:
              return 'Completed by rideeat';
            case 10:
                return 'Accepted'; //but not processed
            default:
              return 'Unknown';
        }
    }

    public function getOrderStatusForCustomerAttribute(){
        switch($this->status){
            case -3:
                return 'Canceled by customer';
            case -2:
                return 'Canceled';
            case -1:
                return 'Canceled';
            case 1:
                return 'Pending';
            case 2:
                return 'Processed';
            case 3:
                return 'On the way';
            case 4:
                return 'Completed';
            case 5:
                return 'Completed';
            case 10:
                return 'Accepted'; //but not processed
            default:
                return 'Unknown';
        }
    }

    public function getOrderStatusColorAttribute(){
        switch($this->status){
            case -3:
                return 'red';
            case -2:
                return 'red';
            case -1:
                return 'red';
            case 1:
                return 'orangeaccent';
            case 2:
                return 'blue';
            case 3:
                return 'blue';
            case 4:
                return 'green';
            case 5:
                return 'green';
            case 10:
                return 'blue';
            default:
                return 'red';
        }
    }

    public function getReviewAttribute(){
        return $this->review()->with('user')->first();
    }

    public function getTimeAttribute(){
        return date('H:i A',strtotime($this->created_at));
    }

    public function getDeliveryAttribute(){
        return $this->belongsTo('App\delivery','id','order_id')->first();
    }

    public function getFoodNamesAttribute(){
        $names='';

        foreach ($this->foods() as $food){
            $names.=$food->name .'+';
        }

        if(strlen($names) > 0){
            $names=substr($names,0,(strlen($names)-1));
        }

        return $names;
    }

    public function getFoodsAttribute(){
        return $this->foods();
    }
    public function getUserAttribute(){
        $user= $this->user()->first();

        if($user)
        return [
            'id'=>$user->id,
            'fullname'=>$user->fullname,
            'phone'=>$user->phone,
            'device_id'=>$user->device_id,
        ];

        return [
            'id'=>"",
            'fullname'=>"",
            'phone'=>"",
            'device_id'=>"",
        ];
    }
    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }
    public function owner(){
        return $this->belongsTo('App\User','user_id', 'id');
    }
    public function vendor(){
        return $this->hasOne('App\vendor','id','vendor_id');
    }

    public function rider(){
        return $this->belongsTo('App\rider');
    }

    public function street(){
        return $this->belongsTo('App\streets','street','id');
    }

    public function street_details(){
        return $this->hasOne('App\streets','id','street');
    }

    public function review(){
        return $this->hasOne('App\Review','order_id','id');
    }

    public function methods(){
        return $this->hasOne('App\DeliverySystem','id','method');
    }

//    public function cancelOrder(){
//        return $this->hasOne('','','');
//    }

    public function foods(){
        $items=json_decode($this->data);
        $foods=[];

        foreach ($items as $itm){
            $item=item::find($itm->item_id);
            if($item){
                $item->price=$itm->price;
                $item->quantity=$itm->quantity;
                $foods[]=$item;
            }
        }

        return $foods;
    }

    public function getHighestFoodTime(){
        $highest = 0;
        foreach ($this->foods() as $food){
            if($food->time > $highest) $highest = $food->time;
        }

        return $highest;
    }

}
