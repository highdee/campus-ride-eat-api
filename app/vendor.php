<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class vendor extends Model
{
    use SoftDeletes;

    protected $fillable=['status'];

    protected $today='';
    public $days=[
        'Sun'=>'SUNDAY',
        'Mon'=>'MONDAY',
        'Tue'=>'TUESDAY',
        'Wed'=>'WEDNESDAY',
        'Thu'=>'THURSDAY',
        'Fri'=>'FRIDAY',
        'Sat'=>'SATURDAY',
    ];

    public function __construct(array $attributes = []) {
        $this->today=$this->days[date('D')];
    }

    protected $appends =['share_text', 'shareable_link', 'reviews_count','reviews','total_transaction','stats','opcl','opcl_today','items','photos','balance','order_count'];

    public function getShareTextAttribute(){
        $text = 'Check out '.$this->name.' Menu on Rideeats. Food Ordering & Delivery Mobile App';
        $text = $text.' '.$this->getShareableLinkAttribute();

        return $text;
    }
    public function getShareableLinkAttribute(){
        $link = env('APP_URL')."/";
//        $link = "rideeat://";
        $link = $link.'restaurant/'.$this->name.'/'.$this->uuid;

        return str_replace(' ','-', $link);
    }
    public function getReviewsCountAttribute(){
        return $this->reviews()->count();
    }

    public function getReviewsAttribute(){
        $reviews = $this->reviews()->orderBy('id','desc')->limit(10)->get();

        $rvs = [];
        foreach ($reviews as $rv){
            $rvs[] = [
                "vendor_id"=>$rv->vendor_id,
                "rate"=>$rv->rate,
                "content"=>$rv->content,
                "user"=>[
                    'fullname'=>$rv->user->fullname
                ]
            ];
        }
        return $rvs;
    }

    public function getTotalTransactionAttribute(){
        $txns=$this->transaction()->count();
        return $txns > 99 ? '99+':$txns;
    }

    public function recent_transactions(){
        $txns=transaction::where(['vendor_id'=>$this->id])->limit(10)->get();
        return $txns;
    }

    public function getOrderCountAttribute(){
        $count=$this->hasMany('App\order')->where('status',1)->count();
        return $count;
    }

    public function getBalanceAttribute(){
        $amount=0;
        $balance=$this->hasOne('App\balance')->first();
        if($balance){
            $amount=$balance->amount;
        }
        return $amount;
    }

    public function getRatingAttribute(){
        $reviews=$this->reviews()->get();
        $ratings=0;
        foreach ($reviews as $review){
            $ratings+=$review->rate;
        }

        $ratings= $ratings < 1 ? $ratings:$ratings/count($reviews);

//        $this->rating = round($ratings, 1);
//        $this->save();

        $ratings = number_format($ratings,1);
        return $ratings.'';
    }

    public function getItemsAttribute(){
        return $this->items()->get();
    }
    public function getPhotosAttribute(){
        return $this->photos()->get();
    }
    public function getOpclAttribute(){
        return $this->opcl()->get();
    }

    public function getOpclTodayAttribute(){
        $res=openingtime::where(['vendor_id'=>$this->id,'day'=>$this->today])->first();

        return $res;
    }
    public function getStatsAttribute(){
        $foods=item::where(['vendor_id'=>$this->id])->count();
        $photos=vendorphotos::where(['vendor_id'=>$this->id])->count();

        return [
            'food'=>$foods,
            'photos'=>$photos
        ];
    }

    public function reviews(){
        return $this->hasMany('App\Review')->orderBy('id','desc');
    }

    public function items(){
        return $this->hasMany('App\item')->whereNull('deleted_at')->orderBy('id','desc');
    }
    public function photos(){
        return $this->hasMany('App\vendorphotos');
    }

    public function opcl(){
        return $this->hasMany('App\openingtime','vendor_id','id');
    }
    public function orders(){
        return $this->hasMany('App\order','vendor_id','id');
    }
    public function v_street(){
        return $this->hasOne('App\streets','id','street');
    }
    public function street(){
        return $this->hasOne('App\streets','id','street');
    }
    public function transaction(){
        return $this->hasMany('App\transaction','vendor_id','id');
    }
    public function user(){
        return $this->belongsTo('App\User', 'user_id','id');
    }

}
