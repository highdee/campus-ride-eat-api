<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromCollection, WithHeadings
{
    public function __construct()
    {
      
    }
    public function headings(): array
    {
        return [
            'FULLNAME',
            'EMAIL',
            'PHONE',
            'USER TYPE',
        ];
    }
    public function collection()
    {
        $temp=[];
        $users=User::where('id','>',0)->orderBy('fullname','asc')->get();
        foreach ($users as $user){
            if($user->vendor){
                $type = 'Vendor';
            }elseif($user->rider){
                $type='Rider';
            }else{
                $type="Customer";
            }
            $temp[]=[
                'name'=>$user->fullname,
                'email'=>$user->email,
                'phone'=>$user->phone,
                'user_type'=>$type,
            ];
        }
        $user=collect($temp);

        return $user;
    }

    // public function collection()
    // {
    //     return User::all();
    // }
}
