<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Helper\Table;
use Illuminate\Database\Eloquent\SoftDeletes;

class streets extends Model
{
   use SoftDeletes;

    protected $appends =['prices'];

    public function getPricesAttribute(){
        $data=DB::table('source_delivery_sytem_destination')->where(['source_id'=>$this->id])->get();
        foreach ($data as $d){
            $delivery_system=DeliverySystem::where(['id'=>$d->delivery_id,'available'=>1])->first();
            $d->system=$delivery_system;
        }
        return $data;
    }
}
