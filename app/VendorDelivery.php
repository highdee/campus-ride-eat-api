<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorDelivery extends Model
{
    protected $casts=[
        'created_at' => 'datetime:D, d-M-Y',
    ];

    protected $appends = ['vendor', 'rider', 'source_street', 'destination_street', 'status'];

    public function getStatusAttribute(){

        if($this->canceled_at != null){
            return "Canceled by ".$this->canceled_by;
        }
        else if($this->picked_at == null){
            return "Waiting";
        }
        else if($this->picked_at != null && $this->completed_at == null){
            return "On the way";
        }
        else if($this->picked_at != null && $this->completed_at == null){
            return "On the way";
        }
        else if($this->picked_at != null && $this->completed_at != null){
            return "Delivered";
        }else{
            return '';
        }
    }

    public function getVendorAttribute(){
        return $this->vendor()->first();
    }

    public function getRiderAttribute(){
        return $this->rider()->with('user')->first();
    }

    public function getSourceStreetAttribute(){
        return $this->street('source_street_id')->first();
    }

    public function getDestinationStreetAttribute(){
        return $this->street('destination_street_id')->first();
    }

    public function vendor(){
        return $this->hasOne('App\vendor','id','vendor_id');
    }

    public function rider(){
        return $this->belongsTo('App\rider');
    }

    public function street($col){
        return $this->belongsTo('App\streets',$col,'id');
    }
}
