<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payforme extends Model
{
    //
    protected $casts=[
        'created_at' => 'datetime:D d M, Y ha'
    ];

    protected $appends = ['progress', 'item_name', 'url', 'share_text', 'status_color', 'status_level'];

    public function getStatusLevelAttribute(){
        $level = 0;
        if($this->status == 'started') $level = 1;
        if($this->status == 'completed') $level = 2;

        return $level;
    }

    public function getStatusColorAttribute(){
        $status = strtolower($this->status);
        switch ($status){
            case 'pending':
                return '0xffff0000';
            case 'started':
                return '0xffff9800';
            case 'completed':
                return '0xff39da8a';
        }
    }
    public function getShareTextAttribute(){
        $user = User::find($this->user_id);
        return 'Rideeats PAY FOR ME. Your friend '.$user->first_name.' needs you to pay for a food order. '.env('APP_URL').'/pay-for-me/'.$this->token;
    }
    public function getUrlAttribute(){
        return env('APP_URL').'/pay-for-me/'.$this->token;
    }

    public function getItemNameAttribute(){
        $title = [];
        $data = [];
        if($this->data != null){
            $data = json_decode($this->data);

            foreach ($data->foods as $datum){
                $item = item::find($datum->item_id);
                if($item){
                    $title[] = $item->name;
                }
            }
        }
        $title = join(', ', $title);
        return $title;
    }

    public function getProgressAttribute(){
        $payments = $this->payments()->get();
        $sum = 0;

        foreach ($payments as $payment){
            $sum+=$payment->pivot->amount;
        }
        return $sum;
    }

    public function payments(){
        return $this->belongsToMany('App\User', 'customer_payment_friend')->withPivot(['amount']);
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
