<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Withdrawal extends Model
{
    //

    protected $casts = [
        'created_at' => 'datetime:M d, Y H:i A'
    ];

    public function rider(){
        return $this->belongsTo(rider::Class, 'rider_id', 'id');
    }

    public function vendor(){
        return $this->belongsTo(vendor::Class, 'vendor_id', 'id');
    }
}
