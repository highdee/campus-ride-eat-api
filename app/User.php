<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','created_at','updated_at','email_verified_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends=['vendor', 'first_name', 'rider', 'bankDetails', 'wallet'];

    public function getFirstNameAttribute(){
        $splitted = explode(' ', $this->fullname);
        $name = $splitted[0];
        return $name;
    }
    public function getWalletAttribute(){
        $wallet = $this->wallet()->first();
//        error_log($wallet);
        if(!$wallet){
            $wallet = [
                'user_id' => $this->id,
                'amount' => 0
            ];
        }
        return $wallet;
    }

    public function getBankDetailsAttribute(){
        return $this->bankDetails()->first();
    }

    public function getVendorAttribute(){
        return $this->vendor()->first();
    }

    public function getRiderAttribute(){
        return $this->rider()->first();
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {

        return [];
    }


    public function vendor(){
        return $this->belongsTo('App\vendor','id','user_id');
    }

    public function bankDetails(){
        return $this->belongsTo('App\bankDetails','id','user_id');
    }

    public function wallet(){
        return $this->belongsTo('App\balance','id','user_id');
    }

    public function rider(){
        return $this->belongsTo('App\rider','id','user_id');
    }



}
