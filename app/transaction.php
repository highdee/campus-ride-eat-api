<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transaction extends Model
{
    //

    public $appends=['provider','user'];

    protected $casts = [
        'created_at' => 'datetime:D, d-M-Y H:i A',
    ];

    public function getProviderAttribute(){
        if($this->vendor_id != null){
            return vendor::find($this->vendor_id);
        }else if($this->rider_id != null){
            return rider::find($this->rider_id);
        }
    }

    public function getUserAttribute(){
        $user= $this->belongsTo('App\User','user_id','id')->first();
        if($user == null){
            return [
                'id'=>'anonymous',
                'fullname'=>'anonymous',
                'phone'=>'anonymous',
                'device_id'=>'anonymous',
            ];
        }
        return [
            'id'=>$user->id,
            'fullname'=>$user->fullname,
            'phone'=>$user->phone,
            'device_id'=>$user->device_id,
        ];
    }
    public function adUser(){
        return $this->hasOne('App\User','id','user_id');
    }

}
