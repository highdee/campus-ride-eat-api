<?php

namespace App\Services;

use App\Http\Controllers\NotificationController;
use App\Jobs\sendPushNotification;
use App\openingtime;
use App\order;
use App\User;
use App\vendor;
use Illuminate\Support\Facades\DB;

class UserService {

    public static function getFavourite($item){
        try {
            $favorites = DB::table('user_vendor')
                ->where('vendor_id', $item->vendor_id)
                ->where('deleted_at', null)
                ->where('type', 'menu')
                ->pluck('user_id');
        }catch (\Exception $msg){
            //
        }

        return $favorites;
    }

    public static function saveFavouriteVendor($user_id){
        $orders = order::where('user_id', $user_id)->get();
        $orders = collect($orders)->groupBy('vendor_id')->toArray();

        $fav_vendors=[];
        foreach ($orders as $vendor_id => $v_orders){
            $perc = (count($v_orders) / count($orders)) * 100;

            if(count($v_orders) > 2 && $perc >= 30){
                $fav_vendors[] = $vendor_id;
            }
        }

        $favorites = DB::table('user_vendor')
            ->where('user_id', $user_id)
            ->pluck('vendor_id');
        $favorites = collect($favorites)->toArray();

        foreach ($fav_vendors as $vendor_id){
            if(!in_array($vendor_id, $favorites)){
                DB::table('user_vendor')->insert([
                    'user_id'=>$user_id,
                    'vendor_id'=>$vendor_id,
                    'type'=>'opcl',
                    'created_at'=>date('Y-m-d H:i:s')
                ]);
                DB::table('user_vendor')->insert([
                    'user_id'=>$user_id,
                    'vendor_id'=>$vendor_id,
                    'type'=>'menu',
                    'created_at'=>date('Y-m-d H:i:s')
                ]);
            }
        }
    }

    public static function getVendorOPTime(){
        $ch = number_format(date('H'));

        if($ch < 7) return;

        $vendors = vendor::all();

        $day = strtoupper(date('l'));

        $hour = number_format(date('h'));

        $last_hour = strtotime(date('Y-m-d h:i:s'));
        $last_hour = $last_hour + (60 * 60);
        $last_hour = number_format(date('h', $last_hour));



        $mer = strtoupper(date('a'));
        $current_time = ($hour.$mer);
        foreach ($vendors as $vendor) {
            $tm = openingtime::where('day', $day)->where('vendor_id',$vendor->id)->first();
            if(!$tm){
                continue;
            }


            $favorites = DB::table('user_vendor')
                ->where('vendor_id', $vendor->id)
                ->where('deleted_at', null)
                ->where('type', 'opcl')
                ->pluck('user_id');
            $devices = User::whereIn('id', $favorites)->pluck('device_id');


            if(count($devices) == 0) continue;

            if(str_replace(' ','', $tm->opening) == $current_time){
               sendPushNotification::dispatch([
                   'header'=>$vendor->name,
                   'body'=>"We are open. You can now order meals from our menu.",
                   'devices'=>$devices,
                   'data'=>['type'=>'vendor', 'restaurant_id'=>$vendor->id]
               ]);
            }
            else if(str_replace(' ','', $tm->closing) == ($last_hour.''.$mer)){
                sendPushNotification::dispatch([
                    'header'=>$vendor->name,
                    'body'=>"we close in the next hour, kindly place your order before closing time.",
                    'devices'=>$devices,
                    'data'=>['type'=>'vendor', 'restaurant_id'=>$vendor->id]
                ]);
            }
        }
    }


}
