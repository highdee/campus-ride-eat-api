<?php

namespace App\Services;

use MailchimpMarketing\ApiClient;
use MailchimpMarketing\Configuration;

class MailChimpService{

    protected $client;

    public function __construct(){
        $this->client = new ApiClient();
        $this->client->setConfig([
            'apiKey' => env('MAILCHIMP_KEY'),
            'server' => 'us1',
        ]);
    }

    public function addMemberToMailChimpList($email, $lastname, $firstname){
        $response = $this->client->lists->addListMember(
            "1caf3c9718", [
                "email_address" => $email,
                "full_name" => $firstname.' '.$firstname,
                "status"=>"subscribed"
            ]
        );
    }

    public function addEvent($email, $eventname){
        $response = $this->client->lists->createListMemberEvent(
            "1caf3c9718", md5($email), [
                "name"=> $eventname
            ]
        );
        error_log(json_encode($response));
    }
}
