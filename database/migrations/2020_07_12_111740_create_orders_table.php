<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->integer('vendor_id');
            $table->integer('rider_id')->nullable();
            $table->integer('method');
            $table->integer('street');
            $table->integer('transaction_id');
            $table->longText('data');
            $table->string('address');
            $table->text('note');
            $table->float('total');
            $table->float('delivery_fare');
            $table->string('reference');
            $table->integer('status');
            $table->timestamps();

            $table->index(['id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
