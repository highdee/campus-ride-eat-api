<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('name');
            $table->text('address')->nullable();
            $table->string('facebook')->nullable();
            $table->string('instagram')->nullable();
            $table->string('twitter')->nullable();
            $table->string('id_card')->nullable();
            $table->string('driving_license')->nullable();
            $table->date('dob')->nullable();
            $table->string('relation_status')->nullable();
            $table->string('kin_name')->nullable();
            $table->string('kin_phone')->nullable();
            $table->string('kin_address')->nullable();
            $table->string('kin_identity')->nullable();
            $table->string('vehicle_type')->nullable();
            $table->string('vehicle_reg_number')->nullable();
            $table->integer('status')->default(0);
            $table->integer('occupied')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riders');
    }
}
