<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_deliveries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('uuid');
            $table->integer('vendor_id');
            $table->integer('rider_id')->nullable();
            $table->integer('source_street_id');
            $table->longText('source_address');
            $table->integer('destination_street_id');
            $table->longText('destination_address');
            $table->double('price');
            $table->longText('description')->nullable();
            $table->longText('reason_for_cancel')->nullable();
            $table->string('canceled_by')->nullable();
            $table->dateTime('completed_at')->nullable();
            $table->dateTime('picked_at')->nullable();
            $table->dateTime('canceled_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_deliveries');
    }
}
