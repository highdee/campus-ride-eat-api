<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VendorFavorite extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_vendor', function (Blueprint $blueprint){
            $blueprint->bigIncrements('id');
            $blueprint->integer('vendor_id');
            $blueprint->integer('user_id');
            $blueprint->string('type')->nullable();
            $blueprint->string('added_by')->default('system');
            $blueprint->string('removed_by')->nullable();
            $blueprint->dateTime('deleted_at')->nullable();
            $blueprint->dateTime('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
