@extends('inc.layout')
@section('deliveriesActive')
    active
@endsection
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1 mb-0">Deliveries</h5>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item"><a href="/campus-admin"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active">Delivery List
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        @include('inc.notification')
        <section id="basic-datatable">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body card-dashboard">
                                <div class="table-responsive">
                                    <table class="table" id="arrange-table">
                                        <thead>
                                        <tr>
                                            <th>User</th>
                                            <th>Rider</th>
                                            <th>Source</th>
                                            <th>Destination</th>
                                            <th>Amount</th>
                                            <th>Picked at</th>
                                            <th>Delivered at</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>
                                        <tbody class="table-hover">
                                        @foreach($deliveries as $del)
                                            {{-- onclick="window.location.href='/admin/del_show/{{$del->id}}'" --}}
                                            <tr>
                                                <td>{{$del->user? $del->user['fullname'] :''}}</td>
                                                <td>{{$del->rider? $del->rider['name'] : ''}}</td>
                                                <td>{{$del->source}}</td>
                                                <td>{{$del->destination}}</td>
                                                <td>{{$del->amount}}</td>
                                                <td>{{$del->picked ? date('d, M Y H:i a', strtotime($del->picked)) : ''}}</td>
                                                <td>{{$del->delivered ? date('d, M Y H:i a', strtotime($del->delivered)) : ''}}</td>
                                                <td class="text-center">
                                                    {{$del->status == 1 ? 'Picked up' : 'Delivered'}}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>User</th>
                                            <th>Rider</th>
                                            <th>Source</th>
                                            <th>Destination</th>
                                            <th>Amount</th>
                                            <th>Picked at</th>
                                            <th>Deliveried at</th>
                                            <th>Status</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
