@extends('inc.layout')
@section('hoverToOpen')
	menu-collapsed
@endsection
@section('configActive')
	active
@endsection
@section('delayActive')
	active
@endsection
@section('content')
	<div class="content-header row">
    </div>
    <div class="content-body">
        <section class="page-user-profile">
            <div class="row">
                <div class="col-12">
                    <div class="row m-0">
                        <div class="col-lg-12">
                            <div class="tab-content">
                                <div class="tab-pane active" id="feed" aria-labelledby="feed-tab" role="tabpanel">
                                    <div class="row justify-content-center">
                                        <div class="col-md-6 pl-0">
                                            @include('inc.notification')
                                            <div class="card">
                                                <div class="card-content">
                                                    <div class="card-body">
                                                        <div class="d-flex justify-content-between">
                                                            <h4 class="card-title">Delay Time</h4>
                                                        </div>
                                                        <form action="/campus-admin/update-delay-time" method="POST">
                                                            @csrf
                                                            <div class="form-group">
                                                                <label for="time">Delay Time (minutes)</label>
                                                                <input type="number" value="{{$delay->value}}" name="delay_time" class="form-control" id="time">
                                                            </div>
                                                            <div class="form-group">
                                                                <button class="btn btn-success" type="submit">Update</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- user profile content section start -->
                </div>
            </div>
        </section>
    </div>
@endsection
