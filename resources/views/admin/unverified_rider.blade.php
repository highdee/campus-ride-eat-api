@extends('inc.layout')
@section('hoverToOpen')
	menu-collapsed
@endsection
@section('content')
	<div class="content-header row">
    </div>
    <div class="content-body">
        @include('inc.notification')
        
        <section class="page-user-profile">
            <div class="row">
                <div class="col-12">
                    <div class="row m-0">
                        <div class="col-lg-12">
                            <div class="tab-content">
                                <div class="tab-pane active" id="feed" aria-labelledby="feed-tab" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-12 pl-0">
                                            <div class="card">
                                                <div class="card-content">
                                                    <div class="card-body">
                                                        <h4 class="card-title">Unverified Rider details</h4>
                                                        <form action="/campus-admin/verify_rider" method="POST" id="verify">
                                                            @csrf
                                                            <div class="row">
                                                                <input type="hidden" name="rider_id" value="{{$rider->id}}" id="">
                                                                
                                                                <div class="form-group col-sm-6">
                                                                    <label for="user">User's name</label>
                                                                    <input type="text" disabled class="form-control" name="user" value="{{$rider->user?$rider->user->fullname:''}}">
                                                                </div>
                                                                <div class="form-group col-sm-6">
                                                                    <label for="name">Rider's name</label>
                                                                    <input type="text" class="form-control" name="name" value="{{$rider->name}}">
                                                                </div>
                                                                <div class="form-group col-sm-6">
                                                                    <label for="type">Vehicle Type</label>
                                                                    <input type="text" class="form-control" name="type" value="{{$rider->vehicle_type}}">
                                                                </div>
                                                                <div class="form-group col-sm-6">
                                                                    <label for="reg">Vehicle Registration Number</label>
                                                                    <input type="text" class="form-control" name="reg_no" id="reg" value="{{$rider->vehicle_reg_number}}">
                                                                </div>
                                                                <div class="form-group col-sm-6">
                                                                    <label for="fc">Facebook</label>
                                                                    <input type="text" class="form-control" name="facebook" id="fc" value="{{$rider->facebook}}">
                                                                </div>
                                                                <div class="form-group col-sm-6">
                                                                    <label for="ph">Phone</label>
                                                                    <input type="text" disabled class="form-control" name="phone" id="phone" value="{{$rider->user?$rider->user->phone:''}}">
                                                                </div>
                                                                <div class="form-group col-sm-6">
                                                                    <label for="kname">Kin Name</label>
                                                                    <input type="text" class="form-control" name="kin_name" id="kname" value="{{$rider->kin_name}}">
                                                                </div>
                                                                <div class="form-group col-sm-6">
                                                                    <label for="kin">Kin Phone</label>
                                                                    <input type="text" class="form-control" name="kin_phone" id="kin" value="{{$rider->kin_phone}}">
                                                                </div>
                                                                <div class="form-group col-sm-6">
                                                                    <label for="kin">Kin Address</label>
                                                                    <textarea name="kin_address" class="form-control" id="" cols="30" rows="5">{{$rider->kin_address}}</textarea>
                                                                </div>
                                                                <div class="form-group col-sm-6">
                                                                    <label for="add">Address</label>
                                                                    <textarea name="address" class="form-control" id="" cols="30" rows="5">{{$rider->address}}</textarea>
                                                                </div>
                                                            </div>
                                                        </form>
                                                        <form action="/campus-admin/cancel_rider_request" id="cancel" method="POST">
                                                            @csrf
                                                            <input type="hidden" name="rider_id" value="{{$rider->id}}">
                                                        </form>
                                                        <div class="d-flex my-0">
                                                            <input type="submit" value="Verify rider" form="verify" class="btn btn-primary">
                                                            <input type="submit" class="btn btn-danger ml-1" value="Cancel rider request" form="cancel">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- user profile content section start -->
                </div>
            </div>
        </section>
    </div>
@endsection
