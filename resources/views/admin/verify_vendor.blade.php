@extends('inc.layout')
@section('hoverToOpen')
	menu-collapsed
@endsection
@section('content')
	<div class="content-header row">
    </div>
    <div class="content-body">
        @include('inc.notification')
        
        <section class="page-user-profile">
            <div class="row">
                <div class="col-12">
                    <div class="row m-0">
                        <div class="col-lg-12">
                            <div class="tab-content">
                                <div class="tab-pane active" id="feed" aria-labelledby="feed-tab" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-12 pl-0">
                                            <div class="card">
                                                <div class="card-content">
                                                    <div class="card-body">
                                                        <h4 class="card-title">Unverified Vendor details</h4>
                                                        <form action="/campus-admin/verify_vendor" method="POST" id="verify">
                                                            @csrf
                                                            <div class="row">
                                                                <input type="hidden" name="vendor_id" value="{{$vendor->id}}" id="">
                                                                
                                                                <div class="form-group col-sm-6">
                                                                    <label for="user">User's name</label>
                                                                    <input type="text" class="form-control" name="user" value="{{$vendor->user?$vendor->user->fullname:''}}">
                                                                </div>
                                                                <div class="form-group col-sm-6">
                                                                    <label for="name">Vendor's name</label>
                                                                    <input type="text" class="form-control" name="name" value="{{$vendor->name}}">
                                                                </div>
                                                                <div class="form-group col-sm-6">
                                                                    <label for="type">Type</label>
                                                                    <input type="text" class="form-control" name="type" value="{{$vendor->type}}">
                                                                </div>
                                                                <div class="form-group col-sm-6">
                                                                    <label for="ph">Phone</label>
                                                                    <input type="text" class="form-control" name="phone" id="phone" value="{{$vendor->phone}}">
                                                                </div>
                                                                <div class="form-group col-sm-6">
                                                                    <label for="str">Street</label>
                                                                    <select name="street" id="str" class="form-control">
                                                                        @foreach ($streets as $str)
                                                                            <option value="{{$str->id}}" {{$str->id == $vendor->street?'selected':''}}>{{$str->name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    {{-- <input type="text" class="form-control" id="str" name="street" value="{{$vendor->v_street->name}}"> --}}
                                                                </div>
                                                                <div class="form-group col-sm-6">
                                                                    <label for="add">Address</label>
                                                                    <textarea name="address" class="form-control" id="" cols="30" rows="5">{{$vendor->address}}</textarea>
                                                                </div>
                                                            </div>
                                                        </form>
                                                        <form action="/campus-admin/cancel_vendor_request" id="cancel" method="POST">
                                                            @csrf
                                                            <input type="hidden" name="vendor_id" value="{{$vendor->id}}">
                                                        </form>
                                                        <div class="d-flex my-0">
                                                            <input type="submit" value="Verify vendor" form="verify" class="btn btn-primary">
                                                            <input type="submit" class="btn btn-danger ml-1" value="Cancel vendor request" form="cancel">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- user profile content section start -->
                </div>
            </div>
        </section>
    </div>
@endsection
