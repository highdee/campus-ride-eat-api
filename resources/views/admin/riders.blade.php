@extends('inc.layout')
@section('riderActive')
    active
@endsection
@section('content')
<div class="content-header row">
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">Riders</h5>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb p-0 mb-0">
                        <li class="breadcrumb-item"><a href="/campus-admin"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active">Rider List
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    @include('inc.notification')
    <section id="basic-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table zero-configuration">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Address</th>
                                            <th>Balance</th>
                                            <th>Vehicle type</th>
                                            <th>Vehicle Registration Number</th>
                                            <th>Status</th>
                                            <th>Active</th>
                                        </tr>  
                                    </thead>
                                    <tbody class="table-hover">
                                        @foreach($riders as $rider)
                                            <tr onclick="window.location.href='/campus-admin/rider/{{$rider->user_id}}'">                                
                                                <td>{{$rider->name}}</td>
                                                <td>{{$rider->address}}</td>
                                                <td>₦{{$rider->balance}}</td>
                                                <td>{{$rider->vehicle_type}}</td>
                                                <td>{{$rider->vehicle_reg_number}}</td>
                                                <td><p class="badge {{$rider->status==0?'badge-info':'badge-success'}}">{{$rider->status==0?'Pending':'Verified'}}</p></td>
                                                <td class="text-center">
                                                    <a href="/campus-admin/rider/{{$rider->user_id}}" class="btn btn-primary d-flex">view <i class="bx bx-right-arrow-alt"></i></a>
                                                </td>      
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Name</th>
                                            <th>Address</th>
                                            <th>Balance</th>
                                            <th>Vehicle type</th>
                                            <th>Vehicle Registration Number</th>
                                            <th>Status</th>
                                            <th>Active</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection