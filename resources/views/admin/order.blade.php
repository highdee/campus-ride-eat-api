@extends('inc.layout')
@section('hoverToOpen')
    menu-collapsed
@endsection
@section('content')
    <div class="content-header row">
    </div>
    <div class="content-body">
        @include('inc.notification')

        <section class="page-user-profile">
            <div class="row">
                <div class="col-12">
                    <div class="row m-0">
                        <div class="col-lg-12">
                            <div class="tab-content">
                                <div class="tab-pane active" id="feed" aria-labelledby="feed-tab" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-8 pl-0">
                                            <div class="card">
                                                <div class="card-content">
                                                    <div class="card-body">
                                                        <div class="crd-head d-flex justify-content-between align-items-center mb-1">
                                                            <h4 class="card-title mb-0">Order details</h4>
                                                            @if($order->status == 1 || $order->status == 2 || $order->status == 3)
                                                                <div class="crd-rgt d-flex">
                                                                    <button class="btn btn-danger mr-2" data-target="#cancelOrder" data-toggle="modal"> Cancel Order</button>
                                                                    <button class="btn btn-success" data-toggle="modal" data-target="#completeOrder"> Complete Order</button>

                                                                </div>
                                                            @endif
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <h5><small class="font-weight-bolder">User</small></h5>
                                                                <h6 class="font-weight-light">
                                                                    {{-- {{ $order->owner ? $order->owner['fullname']:'' }} --}}
                                                                    @if($order->owner)
                                                                        <a href="/campus-admin/user/{{$order->owner['id']}}"> {{$order->owner['fullname']}} </a>
                                                                    @endif
                                                                </h6>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <h5><small class="font-weight-bolder">Vendor</small></h5>
                                                                <h6 class="font-weight-light">
                                                                    @if($order->vendor)
                                                                        <a href="/campus-admin/vendor/{{$order->vendor['user_id']}}"> {{$order->vendor['name']}} </a>
                                                                    @endif
                                                                </h6>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <h5><small class="font-weight-bolder">Rider</small></h5>
                                                                <h6 class="font-weight-light">
                                                                    {{-- {{$order->rider ? $order->rider['name'] :''}} --}}
                                                                    @if($order->rider)
                                                                        <a href="/campus-admin/rider/{{$order->rider['user_id']}}">{{$order->rider['name']}}</a>
                                                                    @endif
                                                                </h6>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <h5><small class="font-weight-bolder">Food</small></h5>
                                                                <h6 class="font-weight-light">{{$order->food_names}}</h6>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <h5><small class="font-weight-bolder">Users' contact</small></h5>
                                                                <h6 class="font-weight-light">{{ $order->owner ? $order->owner['phone']:'' }}</h6>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <h5><small class="font-weight-bolder">Vendors' contact</small></h5>
                                                                <h6 class="font-weight-light">{{$order->vendor?$order->vendor['phone'] :''}}</h6>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <h5><small class="font-weight-bolder">Delivery fee</small></h5>
                                                                <h6 class="font-weight-light">{{$order->delivery_fare}}</h6>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <h5><small class="font-weight-bolder">Method</small></h5>
                                                                <h6 class="font-weight-light">{{$order->methods ? $order->methods['name']:'Pick up' }}</h6>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <h5><small class="font-weight-bolder">Street</small></h5>
                                                                <h6 class="font-weight-light">{{$order->street_details?$order->street_details->name:''}}</h6>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <h5><small class="font-weight-bolder">Address</small></h5>
                                                                <h6 class="font-weight-light">{{$order->address}}</h6>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <h5><small class="font-weight-bolder">Total</small></h5>
                                                                <h6 class="font-weight-light">₦{{$order->total}}</h6>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <h5><small class="font-weight-bolder">Order status</small></h5>
                                                                <h6 class="font-weight-light">
                                                                    @if ($order->status == 1)
                                                                        <span class="badge badge-light-primary">Pending</span>
                                                                    @elseif($order->status == 2)
                                                                        <span class="badge badge-light-info">Processed</span>
                                                                    @elseif($order->status == 3)
                                                                        <span class="badge badge-light-warning">On the way</span>
                                                                    @elseif($order->status == 4)
                                                                        <span class="badge badge-light-success">Completed</span>
                                                                    @elseif($order->status == 5)
                                                                        <span class="badge badge-light-success">Completed by rideeat</span>
                                                                    @elseif($order->status == 10)
                                                                        <span class="badge badge-light-success">Accepted</span>
                                                                    @elseif($order->status == -1)
                                                                        <span class="badge badge-light-danger">Canceled by vendor</span>
                                                                    @elseif($order->status == -2)
                                                                        <span class="badge badge-light-danger">Canceled by rideeat</span>
                                                                    @elseif($order->status == -3)
                                                                        <span class="badge badge-light-danger">Canceled by customer</span>
                                                                    @else
                                                                        <span class="badge badge-light-secondary">Unknown</span>
                                                                    @endif
                                                                </h6>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <h5><small class="font-weight-bolder">Rating</small></h5>
                                                                <h6 class="font-weight-light">{{$order->review?$order->review->rate:''}} stars</h6>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <h5><small class="font-weight-bolder">Review</small></h5>
                                                                <h6 class="font-weight-light">{{$order->review?$order->review->content:''}}</h6>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <h5><small class="font-weight-bolder">Reference</small></h5>
                                                                <h6 class="font-weight-light">{{$order->reference}}</h6>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <h5><small class="font-weight-bolder">Date</small></h5>
                                                                <h6 class="font-weight-light">{{$order->created_at->format('D, M-Y d h:i a')}}</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="cancelOrder" tabindex="-1" role="dialog" aria-labelledby="cancelOrder" aria-modal="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                <div class="modal-header" >
                                    <h5 class="modal-title" id="cancelOrder">Cancel Order</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <i class="bx bx-x"></i>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="/campus-admin/admin-cancel-order" method="POST">
                                        @csrf
                                        <input type="hidden" name="order_id" value="{{$order->id}}">
                                        <label for="comm">Comments:</label>
                                        <textarea class="form-control" name="comment" id="comm" cols="10" rows="5"></textarea>
                                        <fieldset class="mt-2">
                                            <div class="checkbox">
                                                <input type="checkbox" class="checkbox-input" name="pay_customer" id="checkbox1">
                                                <label for="checkbox1">Reverse payment to customer</label>
                                            </div>
                                        </fieldset>
                                        <fieldset class="mt-2">
                                            <div class="checkbox">
                                                <input type="checkbox" class="checkbox-input" name="notify_customer" id="checkbox3">
                                                <label for="checkbox3">Notify customer</label>
                                            </div>
                                        </fieldset>

                                        <fieldset class="mt-2">
                                            <div class="checkbox">
                                                <input type="checkbox" class="checkbox-input" name="debit_vendor" id="checkbox2">
                                                <label for="checkbox2">Debit vendor</label>
                                            </div>
                                        </fieldset>
                                        <fieldset class="mt-2">
                                            <div class="checkbox">
                                                <input type="checkbox" class="checkbox-input" name="notify_vendor" id="checkbox4">
                                                <label for="checkbox4">Notify vendor</label>
                                            </div>
                                        </fieldset>
                                        <button type="submit" class="btn btn-danger mt-2"> Cancel Order</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="completeOrder" tabindex="-1" role="dialog" aria-labelledby="completeOrder" aria-modal="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="completeOrder">Complete Order</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <i class="bx bx-x"></i>
                                    </button>
                                </div>
                                <div class="modal-body d-flex justify-content-center">
                                    <form action="/campus-admin/admin-complete-order" method="POST">
                                        @csrf
                                        <input type="hidden" name="order_id" value="{{$order->id}}">
                                        <fieldset>
                                            <div class="checkbox">
                                                <input type="checkbox" class="checkbox-input" name="notify_user_c" id="checkbox3c">
                                                <label for="checkbox3c">Notify User</label>
                                            </div>
                                        </fieldset>
                                        <fieldset class="mt-2">
                                            <div class="checkbox">
                                                <input type="checkbox" class="checkbox-input" name="notify_rider_c" id="checkbox4c">
                                                <label for="checkbox4c">Notify Rider</label>
                                            </div>
                                        </fieldset>
                                        <button type="submit" class="btn btn-success mt-2"> Complete Order</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

