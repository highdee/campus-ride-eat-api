@extends('inc.layout')
@section('hoverToOpen')
	menu-collapsed
@endsection
@section('content')
	<div class="content-header row">
    </div>
    <div class="content-body">
        @include('inc.notification')

        <section class="page-user-profile">
            <div class="row">
                <div class="col-12">
                    <div class="row m-0">
                        <div class="col-lg-12">
                            <div class="tab-content">
                                <div class="tab-pane active" id="feed" aria-labelledby="feed-tab" role="tabpanel">
                                    <div class="row">
                                        <div class="card">
                                            <div class="card-content">
                                                <div class="user-profile-images" style="height:232px;width:93vw;background-image: url('{{$food->filename}}');background-size:contain;background-position:center; background-repeat:no-repeat;">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8 pl-0">
                                            <div class="card">
                                                <div class="card-content">
                                                    <div class="card-body">
                                                        <div class="d-flex justify-content-between">
                                                            <h4 class="card-title">Food details</h4>
                                                            <form action="/campus-admin/delete-food" method="POST">
                                                                @csrf
                                                                <input type="hidden" name="food_id" value="{{$food->id}}">
                                                                <button type="submit" class="btn">
                                                                    <i class="bx bx-trash text-danger" style="font-size: 27px"></i>
                                                                </button>
                                                            </form>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <h5><small class="text-muted">Name</small></h5>
                                                                <h6 class="font-weight-light">{{$food->name}}</h6>
                                                            </div>
                                                            <div class="col-12">
                                                                <h5><small class="text-muted">Vendor</small></h5>
                                                                <h6 class="font-weight-light">{{$food->vendor_details?$food->vendor_details['name']:''}}</h6>
                                                            </div>
                                                            <div class="col-12">
                                                                <h5><small class="text-muted">Food type</small></h5>
                                                                <h6 class="font-weight-light">
                                                                    @if($food->item_type)
                                                                        {{$food->item_type->title}}
                                                                    @endif
                                                                </h6>
                                                            </div>
                                                            <div class="col-12">
                                                                <h5><small class="text-muted">Price</small></h5>
                                                                <h6 class="font-weight-light">{{$food->price}}</h6>
                                                            </div>
                                                            <div class="col-12">
                                                                <h5><small class="text-muted">Description</small></h5>
                                                                <h6 class="font-weight-light">{{$food->description}}</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- user profile content section start -->
                </div>
            </div>
        </section>
    </div>
@endsection
