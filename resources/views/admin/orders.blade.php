@extends('inc.layout')
@section('orderActive')
    active
@endsection
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1 mb-0">Orders</h5>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item"><a href="/campus-admin"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active">Orders List
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        @include('inc.notification')
        <section id="basic-datatable">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body card-dashboard">
                                <div class="table-responsive">
                                    <table class="table" id="arrange-table">
                                        <thead>
                                        <tr>
                                            <th>User</th>
                                            <th>Vendor</th>
                                            <th>Rider</th>
{{--                                            <th>Item</th>--}}
                                            <th>Price</th>
                                            <th>Delivery fee</th>
                                             <th>Status</th>
                                            <th>Date</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody class="table-hover">
                                        @foreach($orders as $order)
                                            {{-- onclick="window.location.href='/admin/order_show/{{$order->id}}'" --}}
                                            <tr>
                                                <td>
                                                    @if($order->owner)
                                                        <a href="/campus-admin/user/{{$order->owner['id']}}" class="deco"> {{$order->owner['fullname']}} </a>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($order->vendor)
                                                        <a href="/campus-admin/vendor/{{$order->vendor['user_id']}}" class="deco"> {{$order->vendor['name']}} </a>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($order->rider)
                                                        <a href="/campus-admin/rider/{{$order->rider['user_id']}}" class="deco">{{$order->rider['name']}}</a>
                                                    @endif
                                                </td>
{{--                                                <td>{{$order->food_names}}</td>--}}
                                                <td>₦{{$order->total}}</td>
                                                <td>{{$order->delivery_fare}}</td>
                                                <td>{{$order->order_status}}</td>
                                                <td>{{$order->created_at->format('D, M-Y d h:i a')}}</td>
                                                <td class="text-center">
                                                    <a href="/campus-admin/order/{{$order->id}}" class="btn btn-primary d-flex">view <i class="bx bx-right-arrow-alt"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>User</th>
                                            <th>Vendor</th>
                                            <th>Rider</th>
                                            <th>Item</th>
                                            <th>Price</th>
                                            <th>Delivery fee</th>
                                            {{-- <th>Reference</th> --}}
                                            <th>Date</th>
                                            <th></th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
