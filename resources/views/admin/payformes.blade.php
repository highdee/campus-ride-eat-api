@extends('inc.layout')
@section('pay4meActive')
    active
@endsection
@section('content')
<div class="content-header row">
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">Pay4me</h5>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb p-0 mb-0">
                        <li class="breadcrumb-item"><a href="/campus-admin"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active">Pay4me List
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    @include('inc.notification')
    <section id="basic-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table zero-configuration">
                                    <thead>
                                        <tr>
                                            <th>User</th>
                                            <th>Amount</th>
                                            <th>Type</th>
                                            <th>Creation Date</th>
                                            <th>Expire Date</th>
                                            <th>Status</th>
                                            <th>View</th>
                                        </tr>
                                    </thead>
                                    <tbody class="table-hover">
                                        @foreach($pays as $pay)
                                            <tr>
                                                <td>{{$pay->user->fullname}}</td>
                                                <td>₦{{$pay->amount}}</td>
                                                <td>{{$pay->type}}</td>
                                                <td>{{date('D, M-Y d h:i a', strtotime($pay->created_at))}}</td>
                                                <td>{{date('D, M-Y d h:i a', strtotime($pay->expired_at))}}</td>
                                                <td>
                                                    <span class="badge {{strtolower($pay->status) == 'pending'?'badge-primary': (strtolower($pay->status) == 'started'?'badge-secondary':'badge-success')}} ">
                                                        {{$pay->status}}
                                                    </span>
                                                </td>
                                                <td class="text-center">
                                                    <a href="/campus-admin/pay4me/{{$pay->id}}" class="btn btn-primary d-flex">
                                                        view <i class="bx bx-right-arrow-alt"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>User</th>
                                            <th>Amount</th>
                                            <th>Type</th>
                                            <th>Creation Date</th>
                                            <th>Expire Date</th>
                                            <th>Status</th>
                                            <th>View</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
