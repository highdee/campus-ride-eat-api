@extends('inc.layout')
@section('withdrawr')
    active
@endsection
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1 mb-0">Withdrawal Request</h5>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item"><a href="/campus-admin"><i class="bx bx-home-alt"></i></a>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        @include('inc.notification')
        <section id="basic-datatable">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body card-dashboard">
                                <div class="table-responsive">
                                    <table class="table" id="arrange-table">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>User type</th>
                                            <th>Amount</th>
                                            <th>Available Balance</th>
                                            <th>Status</th>
                                            <th>Date</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody class="table-hover">
                                            @foreach($withdrawals as $with)
                                                <tr onclick="window.location.href='/campus-admin/withdrawal/{{$with->id}}'">
                                                    <td>{{$with->rider_id?$with->rider->user->fullname:$with->vendor->user->fullname}}</td>
                                                    <td>{{$with->rider_id?'Rider':'Vendor'}}</td>
                                                    <td>₦{{$with->amount}}</td>
                                                    <td>₦{{$with->rider_id?$with->rider->balance:$with->vendor->balance}}</td>
                                                    <td>
                                                        @if($with->accepted_at)
                                                            <span class="badge badge-success">accepted</span>
                                                        @elseif($with->rejected_at)
                                                            <span class="badge badge-danger">rejected</span>
                                                        @else
                                                            <span class="badge badge-info">pending</span>
                                                        @endif
                                                    </td>
                                                    <td>{{$with->created_at->format('d, M Y H:i a')}}</td>
                                                    <td class="text-center">
                                                        <a href="/campus-admin/withdrawal/{{$with->id}}" class="btn btn-primary d-flex">view <i class="bx bx-right-arrow-alt"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>Name</th>
                                            <th>User type</th>
                                            <th>Amount</th>
                                            <th>Available Balance</th>
                                            <th>Status</th>
                                            <th>Date</th>
                                            <th></th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
