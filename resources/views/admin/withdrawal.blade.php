@extends('inc.layout')
@section('hoverToOpen')
    menu-collapsed
@endsection
@section('content')
    <div class="content-header row">
    </div>
    <div class="content-body">
        @include('inc.notification')

        <section class="page-user-profile">
            <div class="row">
                <div class="col-12">
                    <div class="row m-0">
                        <div class="col-lg-12">
                            <div class="tab-content">
                                <div class="tab-pane active" id="feed" aria-labelledby="feed-tab" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-8 pl-0">
                                            <div class="card">
                                                <div class="card-content">
                                                    <div class="card-body">
                                                        <div class="d-flex justify-content-between mb-2">
                                                            <h4 class="card-title mb-0">Withdrawal details</h4>
                                                            @if($with->accepted_at)
                                                                <span class="badge badge-success">accepted</span>
                                                            @elseif($with->rejected_at)
                                                                <span class="badge badge-danger">rejected</span>
                                                            @else
                                                                <span class="badge badge-info">pending</span>
                                                            @endif
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <h5><small class="font-weight-bolder">User</small></h5>
                                                                <h6 class="font-weight-light">{{$with->rider_id?$with->rider->user->fullname:$with->vendor->user->fullname}}</h6>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <h5><small class="font-weight-bolder">User type</small></h5>
                                                                <h6 class="font-weight-light">{{$with->rider_id?'Rider':'Vendor'}}</h6>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <h5><small class="font-weight-bolder">Amount</small></h5>
                                                                <h6 class="font-weight-light">₦{{$with->amount}}</h6>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <h5><small class="font-weight-bolder">Available balance</small></h5>
                                                                <h6 class="font-weight-light">₦{{$with->rider_id?$with->rider->balance:$with->vendor->balance}}</h6>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <h5><small class="font-weight-bolder">Status</small></h5>
                                                                <h6 class="font-weight-light">
                                                                    @if($with->accepted_at)
                                                                        <span class="badge badge-success">accepted</span>
                                                                    @elseif($with->rejected_at)
                                                                        <span class="badge badge-danger">rejected</span>
                                                                    @else
                                                                        <span class="badge badge-info">pending</span>
                                                                    @endif
                                                                </h6>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <h5><small class="font-weight-bolder">Date</small></h5>
                                                                <h6 class="font-weight-light">{{$with->created_at->format('D, M-Y d h:i a')}}</h6>
                                                            </div>
                                                            @if(!$with->accepted_at && !$with->rejected_at)
                                                                <div class="d-flex mt-2 col-md-12">
                                                                    <form action="/campus-admin/accept-withdrawal" method="POST" class="mr-2">
                                                                        @csrf
                                                                        <input type="hidden" name="withdrawal_id" value="{{$with->id}}">
                                                                        <input type="hidden" name="amount" value="{{$with->amount}}">
                                                                        <input type="submit" class="btn btn-success" value="Accept request">
                                                                    </form>

                                                                    <form action="/campus-admin/reject-withdrawal" method="POST">
                                                                        @csrf
                                                                        <input type="hidden" name="withdrawal_id" value="{{$with->id}}">
                                                                        <input type="submit" class="btn btn-danger" value="Reject request">
                                                                    </form>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- user profile content section start -->
                </div>
            </div>
        </section>
    </div>
@endsection

