@extends('inc.layout')
@section('featuredV')
    active
@endsection
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1 mb-0">Featured vendors</h5>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item"><a href="/campus-admin"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active">Featured Vendor List
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        @include('inc.notification')
        <p v-if="errorStatus" class="alert alert-danger"> @{{errorMsg}}</p>
        <section id="basic-datatable">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body card-dashboard">
                                <div class="">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Name</th>
                                            </tr>
                                        </thead>
                                        <tbody class="" v-if="!loading">
                                            <tr v-for="(vendor, index) in vendors">
                                                <td>
                                                    <p class="carets cursor-pointer" v-if="index != 0" @click="moveUp(index)"><i class="bx bx-caret-up"></i></p>
                                                    <p class="carets cursor-pointer" v-if="index != vendors.length - 1" @click="moveDown(index)"><i class="bx bx-caret-down"></i></p>
                                                </td>
                                                <td> @{{vendor.name}}</td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th></th>
                                                <th>Name</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
@section('script')
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        const app = new Vue({
            el: '#apps',
            data(){
                return {
                    vendors: {!! $vendors->toJson() !!},
                    loading:false,
                    errorMsg:'',
                    errorStatus:false,
                }
            },

            methods: {
                moveUp(index){
                    let hold = this.vendors;
                    let curr_id = hold[index].sort_id;
                    let old_id = hold[index - 1].sort_id;
                    let prev = hold[index - 1];
                    hold[index - 1] = hold[index]
                    hold[index] = prev
                    this.loading = true
                    axios.post('/api/v1/sort-featured-vendor', {
                        'old_sort':curr_id,
                        'new_sort':old_id
                    })
                    .then((response) => {
                        if(response.data.status){
                            this.vendors = response.data.data
                        }else{
                            this.errorStatus = true
                            console.log('error')
                            this.errorMsg = 'Error rearranging vendor, please check your internet connection'
                        }
                        this.loading = false
                        console.log(response)
                    })
                    .catch((error) => {
                        this.loading = false
                        console.log(error);
                    })
                },
                moveDown(index){
                    let hold = this.vendors;
                    let curr_id = hold[index].sort_id;
                    let old_id = hold[index + 1].sort_id;
                    let next = hold[index + 1];
                    hold[index + 1] = hold[index]
                    hold[index] = next
                    this.vendors={}
                    this.vendors = hold

                    this.loading=true
                    axios.post('/api/v1/sort-featured-vendor', {
                        'old_sort':old_id,
                        'new_sort':curr_id
                    })
                        .then((response) => {
                            if(response.data.status){
                                this.vendors = response.data.data
                            }else{
                                this.errorStatus = true
                                this.errorMsg = 'Error rearranging vendor, please check your internet connection'
                            }
                            this.loading = false
                            console.log(response)
                        })
                        .catch((error) => {
                            this.loading=false;
                            console.log(error);
                        })
                },
            },
            mounted(){
                // console.log(this.vendors)
            }
        })
    </script>
@endsection
