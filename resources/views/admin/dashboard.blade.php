@extends('inc.layout')
@section('adminHome')
	active
@endsection
@section('content')
	<div class="content-header row">
	</div>
	<div class="content-body">
		<!-- Dashboard Ecommerce Starts -->
		<section id="dashboard-ecommerce">
			<div class="row index">
				<div class="col-md-3 dashboard-users">
					<div class="">
						<div class="card text-center">
							<div class="card-content">
								<div class="card-body py-1">
									<div class="badge-circle badge-circle-lg badge-circle-light-danger mx-auto mb-50">
										<i class="bx bx-user font-medium-5"></i>
									</div>
									<div class="text-muted line-ellipsis">Users</div>
								<h3 class="mb-0">{{$user}}</h3>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3 dashboard-users">
					<div class="">
						<div class="card text-center">
							<div class="card-content">
								<div class="card-body py-1">
									<div class="badge-circle badge-circle-lg badge-circle-light-primary mx-auto mb-50">
										<i class="bx bx-store font-medium-5"></i>
									</div>
									<div class="text-muted line-ellipsis">Vendors</div>
									<h3 class="mb-0">{{$vendors}}</h3>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-12 dashboard-users">
					<div class="">
						<div class="card text-center">
							<div class="card-content">
								<div class="card-body py-1">
									<div class="badge-circle badge-circle-lg badge-circle-light-secondary mx-auto mb-50">
										<i class="bx bx-bicycle font-medium-5"></i>
									</div>
									<div class="text-muted line-ellipsis">Riders</div>
									<h3 class="mb-0">{{$riders}}</h3>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-12 dashboard-users">
					<div class="">
						<div class="card text-center">
							<div class="card-content">
								<div class="card-body py-1">
									<div class="badge-circle badge-circle-lg badge-circle-light-info mx-auto mb-50">
										<i class="bx bx-basket font-medium-5"></i>
									</div>
									<div class="text-muted line-ellipsis">Deliveries</div>
									<h3 class="mb-0">{{$delivery}}</h3>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-3 col-12 dashboard-users">
					<div class="">
						<div class="card text-center">
							<div class="card-content">
								<div class="card-body py-1">
									<div class="badge-circle badge-circle-lg badge-circle-light-success mx-auto mb-50">
										<i class="bx bx-money font-medium-5"></i>
									</div>
									<div class="text-muted line-ellipsis">Account Balance</div>
									<h3 class="mb-0">₦{{$balance}}</h3>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-3 col-12 dashboard-users">
					<div class="">
						<div class="card text-center">
							<div class="card-content">
								<div class="card-body py-1">
									<div class="badge-circle badge-circle-lg badge-circle-light-default mx-auto mb-50">
										<i class="bx bx-credit-card-alt font-medium-5"></i>
									</div>
									<div class="text-muted line-ellipsis">Credit Transaction</div>
									<h3 class="mb-0">₦{{$credit}}</h3>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-12 dashboard-users">
					<div class="">
						<div class="card text-center">
							<div class="card-content">
								<div class="card-body py-1">
									<div class="badge-circle badge-circle-lg badge-circle-light-warning mx-auto mb-50">
										<i class="bx bx-dollar font-medium-5"></i>
									</div>
									<div class="text-muted line-ellipsis">Withdrawn Transaction</div>
									<h3 class="mb-0">₦{{$debit}}</h3>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
@endsection
