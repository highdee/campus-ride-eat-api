@extends('inc.layout')
@section('hoverToOpen')
	menu-collapsed
@endsection
@section('content')
	<div class="content-header row">
    </div>
    <div class="content-body">
        @include('inc.notification')
        
        <section class="page-user-profile">
            <div class="row">
                <div class="col-12">
                    <div class="">
                        <div class="card-content">
                            <div class="card-body px-0">
                                <ul class="nav user-profile-nav justify-content-center justify-content-md-start nav-tabs border-bottom-0 mb-0" role="tablist">
                                    <li class="nav-item pb-0">
                                        <a class=" nav-link d-flex px-1 active" id="feed-tab" data-toggle="tab" href="#feed" aria-controls="feed" role="tab" aria-selected="true"><i class="bx bx-home"></i><span class="d-none d-md-block">Food Type</span></a>
                                    </li>
                                    <li class="nav-item pb-0">
                                        <a class="nav-link d-flex px-1" id="food-tab" data-toggle="tab" href="#food" aria-controls="food" role="tab" aria-selected="false"><i class="bx bx-dish"></i><span class="d-none d-md-block">Foods</span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row m-0">
                        <div class="col-lg-12">
                            <div class="tab-content">
                                <div class="tab-pane active" id="feed" aria-labelledby="feed-tab" role="tabpanel">
                                    <div class="row">
                                        <div class="card">
                                            <div class="card-content">
                                                <div class="user-profile-images" style="height:232px;width:93vw;background-image: url('{{$type->filename}}');background-size:contain;background-position:center; background-repeat:no-repeat;">
                                              
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8 pl-0">
                                            <div class="card">
                                                <div class="card-content">
                                                    
                                                    <div class="card-body">
                                                        <div class="form">
                                                            <form action="/campus-admin/updateType" method="post" id="update" enctype="multipart/form-data">
                                                                @csrf
                                                                <input type="hidden" name="type_id" value="{{$type->id}}">
                                                                <div class="form-group">
                                                                    <label for="title">Food Type</label>
                                                                    <input type="text" name="type" id="title" value="{{$type->title}}" class="form-control">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="img">Image</label>
                                                                    <input type="file" id="img" name="image" class="form-control">
                                                                </div>
                                                            </form>
                                                        </div>
                                                        
                                                        <div class="d-flex my-0">
                                                            <input type="submit" value="Update" form="update" class="btn btn-primary">
                                                            <button type="button" class="btn btn-danger ml-1" data-toggle="modal" data-target="#danger">Delete
                                                            </button>
                                                        </div>
                                                        <div class="modal fade text-left show" id="danger" tabindex="-1" role="dialog" aria-labelledby="myModalLabel120">
                                                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-body text-center">
                                                                        <h4>Are you sure?</h4>
                                                                        <i class="bx bx-trash text-danger" style="font-size: 90px"></i>
                                                                        <form action="/campus-admin/deleteType" method="POST" id="delete">
                                                                            @csrf
                                                                            <input type="hidden" name="type_id" value="{{$type->id}}">
                                                                            <input type="submit" value="Delete" class="btn btn-danger mt-2">
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane container-fluid" id="food" aria-labelledby="food-tab" role="tabpanel">
                                    @if(count($type->items) < 1)
                                        <div class="alert alert-primary alert-dismissible mb-2" role="alert">
                                            <div class="d-flex align-items-center">
                                                <i class="bx bx-star"></i>
                                                <span>
                                                    No Food Available Under This Catergory
                                                </span>
                                            </div>
                                        </div>
                                    @else 
                                        <section id="basic-datatable">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="card">
                                                        <div class="card-content">
                                                            <div class="card-body card-dashboard">
                                                                <div class="table-responsive">
                                                                    <table class="table zero-configuration">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>Name</th>
                                                                            <th>Vendor</th>
                                                                            <th>Price</th>
                                                                            <th>Description</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody class="table-hover">
                                                                        @foreach($type->items as $fd)
                                                                            <tr>
                                                                                <td>{{$fd->name}}</td>
                                                                                <td>{{$fd->vendor_details['name']}}</td>
                                                                                <td>₦{{$fd->price}}</td>
                                                                                <td>{{$fd->description}}</td>
                                                                            </tr>
                                                                        @endforeach
                                                                        </tbody>
                                                                        <tfoot>
                                                                            <tr>
                                                                                <th>Name</th>
                                                                                <th>Vendor</th>
                                                                                <th>Price</th>
                                                                                <th>Description</th>
                                                                            </tr>
                                                                        </tfoot>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    @endif
                                </div> 
                            </div>
                        </div>
                    </div>
                    <!-- user profile content section start -->
                </div>
            </div>
        </section>
    </div>
@endsection
