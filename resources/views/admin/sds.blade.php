@extends('inc.layout')
@section('sdsActive')
    active
@endsection
@section('content')
<button type="button" class="btn btn-primary mr-1 mb-1 pl-3 pr-3" data-toggle="modal" data-target="#default"><i class="bx bx-plus"></i>Setup Source Delivery System</button>
<div class="content-header row">
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">Source Delivery System</h5>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb p-0 mb-0">
                        <li class="breadcrumb-item"><a href="/campus-admin"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active">Source Delivery System List
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    @include('inc.notification')
    <div class="row">
        <div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
                <div class="modal-content" >
                    <div class="modal-header px-1">
                        <h3 class="modal-title" id="myModalLabel1">Setup Source Delivery System</h3>
                        <button type="button" class="close rounded-pill" data-dismiss="modal" aria-label="Close">
                            <i class="bx bx-x"></i>
                        </button>
                    </div>
                    <div class="modal-body px-1">
                        <section id="multiple-column-form">
                            <div class="row match-height">
                                <div class="col-12">
                                    <div class="card-content">

                                        <div class="card-body p-0">
                                            <form class="form" method="post" action="/campus-admin/addSds">
                                                @csrf
                                                <div class="form-body">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <h6>Source</h6>
                                                            <fieldset class="form-group">
                                                                <select class="form-control" id="source" name="from">
                                                                    @foreach ($streets as $street)
                                                                        <option value="{{$street->id}}">{{$street->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-12">
                                                            <h6>To</h6>
                                                            <fieldset class="form-group">
                                                                <select class="form-control" id="to" name="to">
                                                                    @foreach ($streets as $street)
                                                                        <option value="{{$street->id}}">{{$street->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-12">
                                                            <h6>Delivery System</h6>
                                                            <fieldset class="form-group">
                                                                <select class="form-control" id="delivery_system" name="delivery_system">
                                                                    @foreach ($delivery_system as $ds)
                                                                        <option value="{{$ds->id}}">{{$ds->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-12">
                                                            <label for="cost">Cost</label>
                                                            <fieldset>
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text" id="cost">₦</span>
                                                                    </div>
                                                                    <input id="cost" name="cost" type="number" min="0" class="form-control" placeholder="Cost" aria-describedby="cost">
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-12 d-flex justify-content-end mt-2">
                                                            <button type="submit" class="btn btn-primary btn-block mb-1 mt-0">Submit</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="collapsible-with-icon">
        <div class="collapsible collapse-icon accordion-icon-rotate">
            @foreach($streets as $street)
                <div class="card collapse-header">
                    <div id="headingCollapse{{$street->id}}" class="card-header collapsed" data-toggle="collapse" role="button" data-target="#collapse{{$street->id}}" aria-expanded="false" aria-controls="collapse{{$street->id}}">
                        <span class="collapse-title">
                            <span class="align-middle">{{$street->name}}</span>
                        </span>
                    </div>

                    <div id="collapse{{$street->id}}" role="tabpanel" aria-labelledby="headingCollapse{{$street->id}}" class="collapse" style="">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>Destination</th>
                                                <th>Delivery system</th>
                                                <th>Cost</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody class="table-hover">
                                            @for($i = 0; $i < count($sds); $i++)
                                                @if($sds[$i]->source_id == $street->id)
                                                    <tr>
                                                        <td>{{$sds[$i]->to}}</td>
                                                        <td>{{$sds[$i]->delivery_system}}</td>
                                                        <td>₦{{$sds[$i]->cost}}</td>
                                                        <td>
                                                            <button type="submit" class="btn" data-toggle="modal" data-target="#default{{$sds[$i]->id}}">
                                                                <i class="bx bx-edit-alt text-primary"></i>
                                                            </button>
                                                        </td>
                                                        <td>
                                                            <form action="/campus-admin/deleteSds" method="post">
                                                                @csrf
                                                                <input type="hidden" value="{{$sds[$i]->id}}" name="sds_id">
                                                                <button type="submit" class="btn"><i class="bx bx-trash"></i></button>
                                                            </form>
                                                        </td>
                                                    </tr>

                                                    <div class="row">
                                                        <div class="modal fade text-left" id="default{{$sds[$i]->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                                            <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
                                                                <div class="modal-content" >
                                                                    <div class="modal-header px-1">
                                                                        <h3 class="modal-title" id="myModalLabel1">Edit Source Delivery System</h3>
                                                                        <button type="button" class="close rounded-pill" data-dismiss="modal" aria-label="Close">
                                                                            <i class="bx bx-x"></i>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body px-1">
                                                                        <section id="multiple-column-form">
                                                                            <div class="row match-height">
                                                                                <div class="col-12">
                                                                                    <div class="card-content">

                                                                                        <div class="card-body p-0">
                                                                                            <form class="form" method="post" action="/campus-admin/updateSds">
                                                                                                @csrf
                                                                                                <input type="hidden" value="{{$sds[$i]->id}}" name="sds_id">
                                                                                                <div class="form-body">
                                                                                                    <div class="row">
                                                                                                        <div class="col-12">
                                                                                                            <h6>Source</h6>
                                                                                                            <fieldset class="form-group">
                                                                                                                <select class="form-control" id="source" name="from">
                                                                                                                    @for($j = 0; $j < count($streets); $j++)
                                                                                                                        <option value="{{$streets[$j]->id}}" {{$sds[$i]->from == $streets[$j]->name ?'selected':''}}>{{$streets[$j]->name}}</option>
                                                                                                                    @endfor
                                                                                                                </select>
                                                                                                            </fieldset>
                                                                                                        </div>
                                                                                                        <div class="col-12">
                                                                                                            <h6>To</h6>
                                                                                                            <fieldset class="form-group">
                                                                                                                <select class="form-control" id="to" name="to">
                                                                                                                    @for($k = 0; $k < count($streets); $k++)
                                                                                                                        <option value="{{$streets[$k]->id}}" {{$sds[$i]->to == $streets[$k]->name ?'selected':''}}>{{$streets[$k]->name}}</option>
                                                                                                                    @endfor
                                                                                                                </select>
                                                                                                            </fieldset>
                                                                                                        </div>
                                                                                                        <div class="col-12">
                                                                                                            <h6>Delivery System</h6>
                                                                                                            <fieldset class="form-group">
                                                                                                                <select class="form-control" id="delivery_system" name="delivery_system">
                                                                                                                    @for($x = 0; $x < count($delivery_system); $x++)
                                                                                                                        <option value="{{$delivery_system[$x]->id}}" {{$sds[$i]->delivery_system == $delivery_system[$x]->name?'selected':''}}>{{$delivery_system[$x]->name}}</option>
                                                                                                                    @endfor
                                                                                                                </select>
                                                                                                            </fieldset>
                                                                                                        </div>
                                                                                                        <div class="col-12">
                                                                                                            <label for="cost">Cost</label>
                                                                                                            <fieldset>
                                                                                                                <div class="input-group">
                                                                                                                    <div class="input-group-prepend">
                                                                                                                        <span class="input-group-text" id="cost">₦</span>
                                                                                                                    </div>
                                                                                                                    <input id="cost" value="{{$sds[$i]->cost}}" name="cost" type="number" min="0" class="form-control" placeholder="Cost" aria-describedby="cost">
                                                                                                                </div>
                                                                                                            </fieldset>
                                                                                                        </div>
                                                                                                        <div class="col-12 d-flex justify-content-end mt-2">
                                                                                                            <button type="submit" class="btn btn-primary btn-block mb-1 mt-0">Submit</button>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </form>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </section>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endfor
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Destination</th>
                                                <th>Delivery system</th>
                                                <th>Cost</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
