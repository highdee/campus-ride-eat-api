@extends('inc.layout')
@section('vendorActive')
    active
@endsection
@section('content')
<div class="content-header row">
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">Vendors</h5>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb p-0 mb-0">
                        <li class="breadcrumb-item"><a href="/campus-admin"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active">Vendor List
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    @include('inc.notification')
    <section id="basic-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table zero-configuration">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Featured</th>
                                            <th>Balance</th>
                                            <th>Street</th>
                                            <th>Address</th>
                                            <th>Status</th>
                                            <th></th>
                                        </tr>  
                                    </thead>
                                    <tbody class="table-hover">
                                        @foreach($vendors as $vendor)
                                            <tr>                                
                                                <td onclick="window.location='/campus-admin/vendor/{{$vendor->user_id}}'">{{$vendor->name}}</td>
                                                <td onclick="window.location='/campus-admin/vendor/{{$vendor->user_id}}'">{{$vendor->phone}}</td>
                                                <td onclick="window.location='/campus-admin/vendor/{{$vendor->user_id}}'">{{$vendor->user?$vendor->user->email:''}}</td>
                                                <td class="td-toggle">
                                                    <form action="/campus-admin/vendor-feature-status" method="POST" id="Statustoggler{{$vendor->id}}">
                                                        @csrf
                                                        <input type="hidden" name="vendor_id" value="{{$vendor->id}}">
                                                        <div class="toggle-btn input-group">
                                                            <label class="switch">
                                                                <input type="checkbox" {{$vendor->featured == 1? 'checked' : ''}} name="status" onclick="document.getElementById('Statustoggler{{$vendor->id}}').submit()">
                                                                <span class="slider round"></span>
                                                            </label>
                                                        </div>
                                                    </form>
                                                </td>
                                                <td onclick="window.location='/campus-admin/vendor/{{$vendor->user_id}}'">₦{{$vendor->balance}}
                                                <td onclick="window.location='/campus-admin/vendor/{{$vendor->user_id}}'">{{$vendor->v_street?$vendor->v_street->name:''}}</td>
                                                <td onclick="window.location='/campus-admin/vendor/{{$vendor->user_id}}'">{{$vendor->address}}</td>
                                                <td>
                                                    <p class="badge @if($vendor->status==0) badge-info @elseif($vendor->status==1) badge-success @else badge-danger @endif">
                                                        @if($vendor->status==0)
                                                            pending
                                                        @elseif($vendor->status==1)
                                                            verified
                                                        @else
                                                            banned
                                                        @endif
                                                        {{-- {{$vendor->status==0?'Pending':'Verified'}} --}}
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <a href="/campus-admin/vendor/{{$vendor->user_id}}" class="btn btn-primary d-flex">view <i class="bx bx-right-arrow-alt"></i></a>
                                                </td>       
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Featured</th>
                                            <th>Balance</th>
                                            <th>Street</th>
                                            <th>Address</th>
                                            <th>Status</th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection