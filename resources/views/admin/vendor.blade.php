@extends('inc.layout')
@section('hoverToOpen')
	menu-collapsed
@endsection
@section('app-asset')

@endsection
@section('content')
	<div class="content-header row">
    </div>
    <div class="content-body">
        @include('inc.notification')
        <!-- page user profile start -->
        <section class="page-user-profile">
            <div class="row">
                <div class="col-12">
                    <!-- user profile heading section start -->
                    <div class="">
                        <div class="card-content">
                            <!-- user profile nav tabs start -->
                            <div class="card-body px-0">
                                <ul class="nav user-profile-nav justify-content-center justify-content-md-start nav-tabs border-bottom-0 mb-0" role="tablist">
                                    <li class="nav-item pb-0">
                                        <a class=" nav-link d-flex px-1 active" id="feed-tab" data-toggle="tab" href="#feed" aria-controls="feed" role="tab" aria-selected="true"><i class="bx bx-home"></i><span class="d-none d-md-block">Dashboard</span></a>
                                    </li>
                                    <li class="nav-item pb-0">
                                        <a class="nav-link d-flex px-1" id="order-tab" data-toggle="tab" href="#order" aria-controls="order" role="tab" aria-selected="false"><i class="bx bx-package"></i><span class="d-none d-md-block">Orders</span></a>
                                    </li>
                                    <li class="nav-item pb-0">
                                        <a class="nav-link d-flex px-1" id="item-tab" data-toggle="tab" href="#item" aria-controls="item" role="tab" aria-selected="false"><i class="bx bx-stats"></i><span class="d-none d-md-block">Items</span></a>
                                    </li>
                                    <li class="nav-item pb-0">
                                        <a class="nav-link d-flex px-1" id="gallery-tab" data-toggle="tab" href="#gallery" aria-controls="gallery" role="tab" aria-selected="false"><i class="bx bx-images"></i><span class="d-none d-md-block">Galleries</span></a>
                                    </li>
                                    <li class="nav-item pb-0">
                                        <a class="nav-link d-flex px-1" id="transaction-tab" data-toggle="tab" href="#transaction" aria-controls="transaction" role="tab" aria-selected="false"><i class="bx bx-money"></i><span class="d-none d-md-block">Transactions</span></a>
                                    </li>
                                    <li class="nav-item pb-0">
                                        <a class="nav-link d-flex px-1" id="review-tab" data-toggle="tab" href="#review" aria-controls="review" role="tab" aria-selected="false"><i class="bx bxs-conversation"></i><span class="d-none d-md-block">Reviews</span></a>
                                    </li>
                                    <li class="nav-item pb-0">
                                        <a class="nav-link d-flex px-1" id="bank-tab" data-toggle="tab" href="#bank" aria-controls="bank" role="tab" aria-selected="false"><i class="bx bx-credit-card-alt"></i><span class="d-none d-md-block">Bank Details</span></a>
                                    </li>
                                </ul>
                            </div>
                            <!-- user profile nav tabs ends -->
                        </div>
                    </div>
                    <!-- user profile heading section ends -->

                    <!-- user profile content section start -->
                    <div class="row">
                        <!-- user profile nav tabs content start -->
                        <div class="col-lg-12">

                            <div class="tab-content">
                                <div class="tab-pane active" id="feed" aria-labelledby="feed-tab" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="card">
                                                <div class="card-content">
                                                    <div class="card-body">
                                                        <div class="d-flex justify-content-between mb-2">
                                                            <div class="d-flex align-items-center">
                                                                <h4 class="card-title mb-0">Vendor details</h4>
                                                                <span class="badge ml-2 {{$vendor->status == 1 ? 'badge-success' : 'badge-danger'}}">{{$vendor->status == 1 ? 'Active' : 'banned'}}</span>
                                                            </div>
                                                            <button class="btn btn-primary" data-toggle="modal" data-target="#creditWallet">Credit Wallet</button>
                                                        </div>
                                                        
                                                        <div class="row user-details">
                                                            <div class="col-6">
                                                                <h4><small class="text-muted">Name:</small></h4>
                                                                <h6 class="font-weight-light">{{$vendor->name}}</h6>
                                                            </div>
                                                            <div class="col-6">
                                                                <h4><small class="text-muted">Email:</small></h4>
                                                                <h6 class="font-weight-light text-lowercase">{{$vendor->user?$vendor->user->email:''}}</h6>
                                                            </div>
                                                            <div class="col-6">
                                                                <h4><small class="text-muted">Phone:</small></h4>
                                                                <h6 class="font-weight-light">{{$vendor->phone}}</h6>
                                                            </div>
                                                            <div class="col-6">
                                                                <h4><small class="text-muted">Vendor type:</small></h4>
                                                                <h6 class="font-weight-light">{{$vendor->type}}</h6>
                                                            </div>
                                                            <div class="col-6">
                                                                <h4><small class="text-muted">Street:</small></h4>
                                                                <h6 class="font-weight-light">{{$vendor->v_street?$vendor->v_street->name:''}}</h6>
                                                            </div>
                                                            <div class="col-6">
                                                                <h4><small class="text-muted">Address:</small></h4>
                                                                <h6 class="font-weight-light">{{$vendor->address}}</h6>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex mt-2">
                                                            @if($vendor->status == 0)
                                                                <form action="/campus-admin/verify_vendor" method="POST" class="mr-2">
                                                                    @csrf
                                                                    <input type="hidden" name="vendor_id" value="{{$vendor->id}}">
                                                                    <input type="submit" class="btn btn-info" value="Verify Vendor">
                                                                </form>
                                                            @else
                                                                <form action="/campus-admin/banvendor" method="Post" class="mr-2">
                                                                    @csrf
                                                                    <input type="hidden" value="{{$vendor->user_id}}" name="user_id">
                                                                    <input type="submit" 
                                                                        class="btn {{$vendor->status == 1 ? 'btn-danger' : 'btn-success'}}" 
                                                                        value="{{$vendor->status == 1? 'Ban vendor': 'Activate vendor'}}" 
                                                                        data-toggle="tooltip" data-placement="top" title="" data-original-title="Click to {{ $vendor->status != 1 ? 'activate' : 'ban'}} vendor">               
                                                                </form>
                                                            @endif
                                                            
                                                            <form action="/campus-admin/deleteVendor" method="POST">
                                                                @csrf
                                                                <input type="hidden" name="user_id" value="{{$vendor->user_id}}">
                                                                <input type="submit" class="btn btn-danger" value="Delete">
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="card-content">
                                                        <select class="mt-1 ml-1" id="selt" onchange="myFunction()" style="border:none; border-bottom:1px solid #bbb;">
                                                            <option value="all">All</option>
                                                            <option value="fmonth">1 month</option>
                                                            <option value="smonth">6 month</option>
                                                            <option value="yr">1 year</option>
                                                        </select>
                                                        <div class="card-body text-center">
                                                            <div class="text-muted">Number of Orders</div>
                                                            <?php $order_fm=0;$order_sm=0;$order_year=0; ?>
                                                            @foreach ($vendor->orders as $order)
                                                                @if ($order->created_at >= date('Y-m-d',strtotime(date('Y-m-d').'-1 month')))
                                                                    <?php $order_fm++; ?>
                                                                @endif
                                                                @if($order->created_at >= date('Y-m-d',strtotime(date('Y-m-d').'-6 month')))
                                                                    <?php $order_sm++;?>
                                                                @endif
                                                                @if($order->created_at >= date('Y-m-d',strtotime(date('Y-m-d').'-12 month')))
                                                                    <?php $order_year++;?>
                                                                @endif
                                                            @endforeach
                                                            <h3 id="all">{{count($vendor->orders)}}</h3>
                                                            <h3 id="fmonth">{{$order_fm}}</h3>
                                                            <h3 id="smonth">{{$order_sm}}</h3>
                                                            <h3 id="year">{{$order_year}}</h3>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="card-content">
                                                        <div class="card-body text-center">
                                                            <div class="text-muted">Balance</div>
                                                            <h3>₦{{$vendor->balance}}</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane container-fluid" id="order" aria-labelledby="order-tab" role="tabpanel">
                                    @if(count($vendor->orders) < 1)
                                        <div class="alert alert-primary alert-dismissible mb-2" role="alert">
                                            <div class="d-flex align-items-center">
                                                <i class="bx bx-star"></i>
                                                <span>
                                                    No Order Available
                                                </span>
                                            </div>
                                        </div>
                                    @else 
                                        <div class="row">
                                            @foreach ($vendor->orders as $order)
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="card">
                                                        <div class="card-content">
                                                            <div class="card-body">
                                                                <div class="card-text">
                                                                    <p><b>User:</b> {{$order->owner?$order->owner['fullname']:''}}</p>
                                                                    <p><b>Food:</b> {{$order->food_names}}</p>
                                                                    <p><b>Street:</b> {{$order->street_details?$order->street_details->name:''}}</p>
                                                                    <p><b>Address:</b> {{$order->address}}</p>
                                                                    <p><b>Rider:</b> {{$order->rider?$order->rider->name:''}}</p>
                                                                    <p><b>Total price:</b> ₦{{$order->total}}</p>
                                                                    <p><b>Rating:</b> {{$order->review?$order->review->rate:''}}</p>
                                                                    <p><b>Review:</b> {{$order->review?$order->review->content:''}}</p> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                                
                                <div class="tab-pane container-fluid" id="item" aria-labelledby="item-tab" role="tabpanel">
                                    @if(count($vendor->items) < 1)
                                        <div class="alert alert-primary alert-dismissible mb-2" role="alert">
                                            <div class="d-flex align-items-center">
                                                <i class="bx bx-star"></i>
                                                <span>
                                                    No Item Available
                                                </span>
                                            </div>
                                        </div>
                                    @else
                                        <div class="row">
                                            @foreach ($vendor->items as $item)
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="card">
                                                        <div class="card-content">
                                                            <img class="card-img-top img-fluid" src="{{$item->filename.'/500'.'/500'}}" alt="item's image">
                                                            <div class="card-body">
                                                                <h4 class="card-title mb-1">{{$item->name}}</h4>
                                                                <p class="card-text">
                                                                    <p><b>Type:</b> {{$item->type}}</p>
                                                                    <p><b>Price:</b> ₦{{$item->price}}</p>
                                                                    <b>Description</b> <p>{{$item->description}}</p>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach 
                                        </div>
                                    @endif
                                </div>

                                <div class="tab-pane container-fluid" id="gallery" aria-labelledby="gallery-tab" role="tabpanel">
                                    @if(count($vendor->photos) < 1)
                                        <div class="alert alert-primary alert-dismissible mb-2" role="alert">
                                            <div class="d-flex align-items-center">
                                                <i class="bx bx-star"></i>
                                                <span>
                                                    No Image Available
                                                </span>
                                            </div>
                                        </div>
                                    @else 
                                        <div class="row">
                                            @foreach ($vendor->photos as $photo)
                                                <div class="col-md-4 col-sm-12">
                                                    <div class="card">
                                                        <div class="card-content">
                                                            <img class="card-img-top img-fluid" src="{{$photo->filename.'/500/500'}}">
                                                        </div>
                                                    </div>
                                                </div> 
                                            @endforeach
                                        </div>
                                    @endif
                                </div>

                                <div class="tab-pane container-fluid" id="transaction" aria-labelledby="transaction-tab" role="tabpanel">
                                    @if(count($vendor->transaction) < 1)
                                        <div class="alert alert-primary alert-dismissible mb-2" role="alert">
                                            <div class="d-flex align-items-center">
                                                <i class="bx bx-star"></i>
                                                <span>
                                                    No Transaction Available
                                                </span>
                                            </div>
                                        </div>
                                    @else 
                                        <section id="basic-datatable">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="card">
                                                        <div class="card-content">
                                                            <div class="card-body card-dashboard">
                                                                <div class="table-responsive">
                                                                    <table class="table zero-configuration">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>User</th>
                                                                            <th>Amount</th>
                                                                            <th>P_reference</th>
                                                                            <th>Reference</th>
                                                                            <th>Status</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody class="table-hover">
                                                                        @foreach($vendor->transaction as $trans)
                                                                            {{-- onclick="window.location.href='/admin/trans_show/{{$trans->id}}'" --}}
                                                                            <tr>
                                                                                <td>{{$trans->user['fullname']}}</td>
                                                                                <td>{{$trans->amount}}</td>
                                                                                <td>{{$trans->p_reference}}</td>
                                                                                <td>{{$trans->reference}}</td>
                                                                                <td class="text-center">
                                                                                    {{$trans->status==1?'Credit':'Debit'}}
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                        </tbody>
                                                                        <tfoot>
                                                                        <tr>
                                                                            <th>User</th>
                                                                            <th>Amount</th>
                                                                            <th>P_reference</th>
                                                                            <th>Reference</th>
                                                                            <th>Status</th>
                                                                        </tr>
                                                                        </tfoot>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    @endif
                                </div>

                                <div class="tab-pane container-fluid" id="review" aria-labelledby="review-tab" role="tabpanel">
                                    @if(count($vendor->orders) < 1)
                                        <div class="alert alert-primary alert-dismissible mb-2" role="alert">
                                            <div class="d-flex align-items-center">
                                                <i class="bx bx-star"></i>
                                                <span>
                                                    No Review Available
                                                </span>
                                            </div>
                                        </div>
                                    @else 
                                        <div class="row">
                                            @foreach ($vendor->orders as $order)
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="card">
                                                        <div class="card-content">
                                                            <div class="card-body">
                                                                <div class="card-text">
                                                                    <p><b>User:</b> {{$order->owner?$order->owner['fullname']:''}}</p>
                                                                    <p><b>Food:</b> {{$order->food_names}}</p>
                                                                    <p><b>Rating:</b> {{$order->review?$order->review->rate:''}}</p>
                                                                    <p><b>Review:</b> {{$order->review?$order->review->content:''}}</p> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>

                                <div class="tab-pane container-fluid" id="bank" aria-labelledby="bank-tab" role="tabpanel">
                                    @if($vendor->user->bankDetails == null)
                                        <div class="alert alert-primary alert-dismissible mb-2" role="alert">
                                            <div class="d-flex align-items-center">
                                                <i class="bx bx-star"></i>
                                                <span>
                                                    No Bank details Available
                                                </span>
                                            </div>
                                        </div>
                                    @else 
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <div class="card bank-details" style="background-color:transparent;">
                                                    <div class="card-content">
                                                        <div class="card-body">
                                                            <div class="card-text">
                                                                <b class="text-capitalize">{{$vendor->user->bankDetails->bank}}</b> 
                                                            </div>
                                                            <div class="card-no text-center">
                                                                <p> {{$vendor->user->bankDetails->number}} </p>
                                                            </div>
                                                            <div class="card-fi d-flex justify-content-between">
                                                                <h5 class="m-0">{{$vendor->user->bankDetails->name}}</h5>
                                                                <div class="card-imgs">
                                                                    <img src="{{asset('app-assets/images/lg.png')}}" width="100%" alt="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- user profile content section start -->
                </div>
            </div>
        </section>
        <!-- page user profile ends -->
        <div class="modal fade" id="creditWallet" tabindex="-1" role="dialog" aria-labelledby="creditWallet" aria-modal="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="creditWallet">Credit vendor wallet</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="bx bx-x"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="/campus-admin/credit-vendor" method="POST">
                            @csrf
                            <input type="hidden" name="vendor_id" value="{{$vendor->id}}">
                            <fieldset>
                                <label for="amt">Amount</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">₦</span>
                                    </div>
                                    <input type="text" name="amount" class="form-control" aria-describedby="basic-addon1">
                                </div>
                            </fieldset>
                            <button type="submit" class="btn btn-success mt-2"> Credit wallet</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    
@endsection

@section('script')
<script>
    document.getElementById('fmonth').style.display = 'none';
    document.getElementById('smonth').style.display = 'none';
    document.getElementById('year').style.display = 'none';
    function myFunction(){
        var x= document.getElementById("selt").value;
        if(x == 'smonth'){
            document.getElementById('smonth').style.display = 'block';
            document.getElementById('all').style.display = 'none';
            document.getElementById('fmonth').style.display = 'none';
            document.getElementById('year').style.display = 'none';
        }else if(x == 'fmonth'){
            document.getElementById('fmonth').style.display = 'block';
            document.getElementById('all').style.display = 'none';
            document.getElementById('year').style.display = 'none';
            document.getElementById('smonth').style.display = 'none';
        }else if(x == 'yr'){
            document.getElementById('year').style.display = 'block';
            document.getElementById('all').style.display = 'none';
            document.getElementById('smonth').style.display = 'none';
            document.getElementById('fmonth').style.display = 'none';
        }else{
            document.getElementById('all').style.display = 'block';
            document.getElementById('fmonth').style.display = 'none';
            document.getElementById('smonth').style.display = 'none';
            document.getElementById('year').style.display = 'none';
        }

    }
</script>
@endsection
