@extends('inc.layout')
@section('typeActive')
    active
@endsection
@section('content')
<button type="button" class="btn btn-primary mr-1 mb-1 pl-3 pr-3" data-toggle="modal" data-target="#default">
    <i class="bx bx-plus"></i>Add New Food Type
</button>
<div class="content-header row">
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">Food Types</h5>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb p-0 mb-0">
                        <li class="breadcrumb-item"><a href="/campus-admin"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active">Type List
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    @include('inc.notification') 
        <div class="row"  >
            <div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content" >
                        <div class="modal-header px-1">
                            <h3 class="modal-title" id="myModalLabel1">Add New Food Type</h3>
                            <button type="button" class="close rounded-pill" data-dismiss="modal" aria-label="Close">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body px-1">
                            <section id="multiple-column-form">
                                <div class="row match-height">
                                    <div class="col-12">
                                        <div class="card-content">
                                           
                                            <div class="card-body p-0">
                                                <form class="form" method="post" action="/campus-admin/add-item-type" enctype="multipart/form-data">
                                                    @csrf
                                                    <div class="form-body">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <label for="f-name-column">Food Type</label>
                                                                <div class="form-label-group">
                                                                    <input type="text" id="f-name-column" class="form-control" placeholder="Food Type" name="type">
                                                                </div>
                                                            </div>
                                                            <div class="col-12">
                                                                <label for="img">Image</label>
                                                                <div class="form-label-group">
                                                                    <input type="file" id="img" class="form-control" name="image">
                                                                </div>
                                                            </div>
                                                            <div class="col-12 d-flex justify-content-end">
                                                                <button type="submit" class="btn btn-primary btn-block mb-1 mt-0">Submit</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <section id="basic-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table zero-configuration">
                                    <thead>
                                        <tr>
                                            <th>Food types</th>
                                        </tr>  
                                    </thead>
                                    <tbody class="table-hover">
                                        @foreach($types as $type)
                                            <tr onclick="window.location.href='/campus-admin/type/{{$type->id}}'">                                
                                                <td>{{$type->title}}</td>       
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Food type</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection