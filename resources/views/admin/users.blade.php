@extends('inc.layout')
@section('userActive')
    active
@endsection
@section('content')
<div class="content-header row">
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="row breadcrumbs-top justify-content-between d-flex">
            <div class="col-8">
                <h5 class="content-header-title float-left pr-1 mb-0">Users</h5>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb p-0 mb-0">
                        <li class="breadcrumb-item"><a href="/campus-admin"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active">User List
                        </li>
                    </ol>
                </div>
            </div>
            <div class="d-flex align-items-center">
                <a class="btn btn-primary" href="/campus-admin/export-users">Export Excel</a>
                <button class="btn btn-info ml-2" data-toggle="modal" data-target="#mailUser">Send Notification</button>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    @include('inc.notification')
    <section id="basic-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table zero-configuration">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Balance</th>
                                            <th>Status</th>
                                            <th>User type</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody class="table-hover">
                                        @foreach($users as $user)
                                            <tr onclick="window.location.href='/campus-admin/user/{{$user->id}}'">
                                                <td>{{$user->fullname}}</td>
                                                <td>{{$user->email}}</td>
                                                <td>{{$user->phone}}</td>
                                                <td>
                                                    @if($user->wallet)
                                                        ₦{{$user->wallet['amount']}}
                                                    @else
                                                        ₦0
                                                    @endif

                                                </td>
                                                <td class="text-center">
                                                    <span class="badge {{$user->status == -1?'badge-danger':'badge-success'}}">
                                                        {{$user->status == -1?'Banned':'Active'}}
                                                    </span>
                                                </td>
                                                <td>
                                                    @if($user->vendor)
                                                        Vendor
                                                    @elseif($user->rider)
                                                        Rider
                                                    @else
                                                        Customer
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    <a href="/campus-admin/user/{{$user->id}}" class="btn btn-primary d-flex">view <i class="bx bx-right-arrow-alt"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Balance</th>
                                            <th>Status</th>
                                            <th>User type</th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="mailUser" tabindex="-1" role="dialog" aria-labelledby="mailUser" aria-modal="true">
                <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="mailUser">Send Notification to users</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="/campus-admin/notify-all-users" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label for="title">Subject</label>
                                    <input type="text" name="title" class="form-control" id="title">
                                </div>
                                <div class="form-group">
                                    <label for="comm">Message:</label>
                                    <textarea class="form-control" name="message" id="comm" cols="10" rows="5"></textarea>
                                </div>
                                <div class="form-group mb-0">
                                    <label>Send To:</label>
                                </div>
                                <ul class="list-unstyled mb-0">
                                    <li class="d-inline-block mr-2 mb-1">
                                        <fieldset>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" value="users" class="custom-control-input" name="send_to" id="customRadio1">
                                                <label class="custom-control-label" for="customRadio1">All Users</label>
                                            </div>
                                        </fieldset>
                                    </li>
                                    <li class="d-inline-block mr-2 mb-1">
                                        <fieldset>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" value="vendors" class="custom-control-input" name="send_to" id="customRadio2">
                                                <label class="custom-control-label" for="customRadio2">Vendors only</label>
                                            </div>
                                        </fieldset>
                                    </li>
                                    <li class="d-inline-block mr-2 mb-1">
                                        <fieldset>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" value="riders" class="custom-control-input" name="send_to" id="customRadio3">
                                                <label class="custom-control-label" for="customRadio3">Riders only</label>
                                            </div>
                                        </fieldset>
                                    </li>
                                </ul>
                                <button type="submit" class="btn btn-secondary mt-2"> Notify Users</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
