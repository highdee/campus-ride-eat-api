@extends('inc.layout')
@section('u_vendor')
    active
@endsection
@section('content')
<div class="content-header row">
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">Unverified Vendor</h5>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb p-0 mb-0">
                        <li class="breadcrumb-item"><a href="/campus-admin"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active">Vendor List
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    @include('inc.notification') 
    <section id="basic-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table zero-configuration">
                                    <thead>
                                        <tr>
                                            <th>User name</th>
                                            <th>Vendor name</th>
                                            <th>Type</th>
                                            <th>Phone</th>
                                            <th>Address</th>
                                            <th>Street</th>
                                        </tr>  
                                    </thead>
                                    <tbody class="table-hover">
                                        @foreach($vendors as $vendor)
                                            <tr onclick="window.location.href='/campus-admin/unverified-vendor/{{$vendor->id}}'">                                
                                                <td>{{$vendor->user?$vendor->user->fullname:''}}</td>
                                                <td>{{$vendor->name}}</td> 
                                                <td>{{$vendor->type}}</td>     
                                                <td>{{$vendor->phone}}</td> 
                                                <td>{{$vendor->address}}</td>
                                                <td>{{$vendor->v_street?$vendor->v_street->name:''}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>User name</th>
                                            <th>Vendor name</th>
                                            <th>Type</th>
                                            <th>Phone</th>
                                            <th>Address</th>
                                            <th>Street</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection