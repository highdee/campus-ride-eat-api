@extends('inc.layout')
@section('transActive')
    active
@endsection
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1 mb-0">Transactions</h5>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item"><a href="/campus-admin"><i class="bx bx-home-alt"></i></a></li>
                            <li class="breadcrumb-item active">Transactions List</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        @include('inc.notification')
        <section id="basic-datatable">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body card-dashboard">
                                <div class="table-responsive">
                                    <table class="table" id="arrange-table">
                                        <thead>
                                        <tr>
                                            <th>User</th>
                                            <th>Rider</th>
                                            <th>Vendor</th>
                                            <th>Amount</th>
                                            <th>P_reference</th>
                                            <th>Date</th>
                                            <th>Reference</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>
                                        <tbody class="table-hover">
                                        @foreach($transactions as $trans)
                                            {{-- onclick="window.location.href='/admin/trans_show/{{$trans->id}}'" --}}
                                            <tr>
                                                <td class="{{!$trans->adUser?'text-danger':''}}">{{$trans->adUser?$trans->adUser['fullname']:'deleted'}}</td>
                                                <td>{{$trans->rider_id ? $trans->provider['name'] :''}}</td>
                                                <td>{{$trans->vendor_id ? $trans->provider['name'] : ''}}</td>
                                                <td>{{$trans->amount}}</td>
                                                <td>{{$trans->p_reference}}</td>
                                                <td>{{$trans->created_at->format('d, M Y')}}</td>
                                                <td>{{$trans->reference}}</td>
                                                <td class="text-center">
                                                    {{$trans->status==1?'Credit':'Debit'}}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>User</th>
                                            <th>Rider</th>
                                            <th>Vendor</th>
                                            <th>Amount</th>
                                            <th>P_reference</th>
                                            <th>Date</th>
                                            <th>Reference</th>
                                            <th>Status</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
