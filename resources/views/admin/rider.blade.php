@extends('inc.layout')
@section('hoverToOpen')
	menu-collapsed
@endsection
@section('app-asset')

@endsection
@section('content')
	<div class="content-header row">
    </div>
    <div class="content-body">
        @include('inc.notification')
        <!-- page user profile start -->
        <section class="page-user-profile">
            <div class="row">
                <div class="col-12">
                    <!-- user profile heading section start -->
                    <div class="">
                        <div class="card-content">
                            <!-- user profile nav tabs start -->
                            <div class="card-body px-0">
                                <ul class="nav user-profile-nav justify-content-center justify-content-md-start nav-tabs border-bottom-0 mb-0" role="tablist">
                                    <li class="nav-item pb-0">
                                        <a class=" nav-link d-flex px-1 active" id="feed-tab" data-toggle="tab" href="#feed" aria-controls="feed" role="tab" aria-selected="true"><i class="bx bx-home"></i><span class="d-none d-md-block">Dashboard</span></a>
                                    </li>
                                    <li class="nav-item pb-0">
                                        <a class="nav-link d-flex px-1" id="order-tab" data-toggle="tab" href="#order" aria-controls="order" role="tab" aria-selected="false"><i class="bx bx-package"></i><span class="d-none d-md-block">Orders</span></a>
                                    </li>
                                    <li class="nav-item pb-0">
                                        <a class="nav-link d-flex px-1" id="deliveries-tab" data-toggle="tab" href="#deliveries" aria-controls="deliveries" role="tab" aria-selected="false"><i class="bx bx-stats"></i><span class="d-none d-md-block">Deliveries</span></a>
                                    </li>
                                    <li class="nav-item pb-0">
                                        <a class="nav-link d-flex px-1" id="transaction-tab" data-toggle="tab" href="#transaction" aria-controls="transaction" role="tab" aria-selected="false"><i class="bx bx-money"></i><span class="d-none d-md-block">Transactions</span></a>
                                    </li>
                                    <li class="nav-item pb-0">
                                        <a class="nav-link d-flex px-1" id="bank-tab" data-toggle="tab" href="#bank" aria-controls="bank" role="tab" aria-selected="false"><i class="bx bx-credit-card-alt"></i><span class="d-none d-md-block">Bank Details</span></a>
                                    </li>
                                </ul>
                            </div>
                            <!-- user profile nav tabs ends -->
                        </div>
                    </div>
                    <!-- user profile heading section ends -->

                    <!-- user profile content section start -->
                    <div class="row">
                        <!-- user profile nav tabs content start -->
                        <div class="col-lg-12">

                            <div class="tab-content">
                                <div class="tab-pane active" id="feed" aria-labelledby="feed-tab" role="tabpanel">
                                    <div class="row user-details">
                                        <div class="col-md-8">
                                            <div class="card">
                                                <div class="card-content">
                                                    <div class="card-body">
                                                        <div class="d-flex justify-content-between align-items-center mb-1">
                                                            <h4 class="card-title">Rider details</h4>
                                                            <button class="btn btn-primary" data-toggle="modal" data-target="#creditWallet">Credit Wallet</button>
                                                        </div>
                                                        <div class="row data-light">
                                                            <div class="col-12 col-sm-6">
                                                                <h4><small class="">Name:</small></h4>
                                                                <h6>{{$rider->name}}</h6>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <h4><small class="">Dob:</small></h4>
                                                                <h6>{{$rider->dob}}</h6>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <h4><small class="">Relation Status:</small></h4>
                                                                <h6>{{$rider->relation_status}}</h6>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <h4><small class="">Vehicle type:</small></h4>
                                                                <h6>{{$rider->vehicle_type}}</h6>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <h4><small class="">Vehicle Registration Number:</small></h4>
                                                                <h6>{{$rider->vehicle_reg_number}}</h6>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <h4><small class="">Address:</small></h4>
                                                                <h6>{{$rider->address}}</h6>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <h4><small class="">Facebook:</small></h4>
                                                                <h6>{{$rider->facebook}}</h6>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <h4><small class="">Instagram:</small></h4>
                                                                <h6>{{$rider->instagram}}</h6>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <h4><small class="">Twitter:</small></h4>
                                                                <h6>{{$rider->twitter}}</h6>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <h4><small class="">Next of Kin Name:</small></h4>
                                                                <h6>{{$rider->kin_name}}</h6>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <h4><small class="">Next of Kin Phone:</small></h4>
                                                                <h6>{{$rider->kin_phone}}</h6>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <h4><small class="">Next of Kin Address:</small></h4>
                                                                <h6>{{$rider->kin_address}}</h6>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex mt-2">
                                                            @if($rider->status == 0)
                                                                <form action="/campus-admin/verify_rider" method="POST" class="mr-2">
                                                                    @csrf
                                                                    <input type="hidden" name="rider_id" value="{{$rider->id}}">
                                                                    <input type="submit" class="btn btn-info" value="Verify Rider">
                                                                </form>
                                                            @else
                                                                <form action="/campus-admin/ban-rider" method="Post" class="mr-2">
                                                                    @csrf
                                                                    <input type="hidden" value="{{$rider->user_id}}" name="user_id">
                                                                    <input type="submit"
                                                                        class="btn {{$rider->status == 1 ? 'btn-success' : 'btn-danger'}}"
                                                                        value="{{$rider->status == 1?'active': 'banned'}}"
                                                                        data-toggle="tooltip" data-placement="top" title="" data-original-title="Click to {{ $rider->status != 1 ? 'activate' : 'ban'}} rider">
                                                                </form>
                                                            @endif
                                                            <form action="/campus-admin/delete-rider" method="POST">
                                                                @csrf
                                                                <input type="hidden" name="user_id" value="{{$rider->user_id}}">
                                                                <input type="submit" class="btn btn-danger" value="Delete">
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="card-content">
                                                        <select class="mt-1 ml-1" id="selt" onchange="myFunction()" style="border:none; border-bottom:1px solid #bbb;">
                                                            <option value="all">All</option>
                                                            <option value="fmonth">1 month</option>
                                                            <option value="smonth">6 month</option>
                                                            <option value="yr">1 year</option>
                                                        </select>
                                                        <div class="card-body text-center">
                                                            <div class="text-muted">Number of Deliveries</div>
                                                            <?php $del_fm=0;$del_sm=0;$del_year=0; ?>
                                                            @foreach ($rider->deliveries as $del)
                                                                @if ($del->created_at >= date('Y-m-d',strtotime(date('Y-m-d').'-1 month')))
                                                                    <?php $del_fm++; ?>
                                                                @endif
                                                                @if($del->created_at >= date('Y-m-d',strtotime(date('Y-m-d').'-6 month')))
                                                                    <?php $del_sm++;?>
                                                                @endif
                                                                @if($del->created_at >= date('Y-m-d',strtotime(date('Y-m-d').'-12 month')))
                                                                    <?php $del_year++;?>
                                                                @endif
                                                            @endforeach
                                                            <h3 id="all">{{count($rider->deliveries)}}</h3>
                                                            <h3 id="fmonth">{{$del_fm}}</h3>
                                                            <h3 id="smonth">{{$del_sm}}</h3>
                                                            <h3 id="year">{{$del_year}}</h3>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="card-content">
                                                        <div class="card-body text-center">
                                                            <div class="text-muted">Balance</div>
                                                            <h3>₦{{$rider->balance}}</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane container-fluid" id="order" aria-labelledby="order-tab" role="tabpanel">
                                    @if(count($rider->orders) < 1)
                                        <div class="alert alert-primary alert-dismissible mb-2" role="alert">
                                            <div class="d-flex align-items-center">
                                                <i class="bx bx-star"></i>
                                                <span>
                                                    No Order Available
                                                </span>
                                            </div>
                                        </div>
                                    @else
                                    <section id="basic-datatable">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="card">
                                                    <div class="card-content">
                                                        <div class="card-body card-dashboard">
                                                            <div class="table-responsive">
                                                                <table class="table zero-configuration">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>User</th>
                                                                        <th>Vendor</th>
                                                                        <th>Delivery fare</th>
                                                                        <th>Food</th>
                                                                        <th>Street</th>
                                                                        <th>Address</th>
                                                                        <th>Price</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody class="table-hover">
                                                                    @foreach($rider->orders as $order)
                                                                        <tr>
                                                                            <td>{{$order->owner?$order->owner['fullname']:''}}</td>
                                                                            <td>{{$order->vendor?$order->vendor->name:''}}</td>
                                                                            <td>₦{{$order->delivery_fare}}</td>
                                                                            <td>{{$order->food_names}}</td>
                                                                            <td>{{$order->street_details?$order->street_details->name:''}}</td>
                                                                            <td>{{$order->address}}</td>
                                                                            <td>₦{{$order->total}}</td>
                                                                        </tr>
                                                                    @endforeach
                                                                    </tbody>
                                                                    <tfoot>
                                                                    <tr>
                                                                        <th>User</th>
                                                                        <th>Vendor</th>
                                                                        <th>Delivery fare</th>
                                                                        <th>Food</th>
                                                                        <th>Street</th>
                                                                        <th>Address</th>
                                                                        <th>Price</th>
                                                                    </tr>
                                                                    </tfoot>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    @endif
                                </div>

                                <div class="tab-pane container-fluid" id="deliveries" aria-labelledby="deliveries-tab" role="tabpanel">
                                    @if(count($rider->deliveries) < 1)
                                        <div class="alert alert-primary alert-dismissible mb-2" role="alert">
                                            <div class="d-flex align-items-center">
                                                <i class="bx bx-star"></i>
                                                <span>
                                                    No Deliveries Available
                                                </span>
                                            </div>
                                        </div>
                                    @else
                                    <section id="basic-datatable">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="card">
                                                    <div class="card-content">
                                                        <div class="card-body card-dashboard">
                                                            <div class="table-responsive">
                                                                <table class="table zero-configuration">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>User</th>
                                                                        <th>Vendor</th>
                                                                        <th>From</th>
                                                                        <th>To</th>
                                                                        <th>Amount</th>
                                                                        <th>Picked at</th>
                                                                        <th>Delivered at</th>
                                                                        <th>Status</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody class="table-hover">
                                                                    @foreach($rider->deliveries as $del)
                                                                        <tr>
                                                                            <td>{{$del->user?$del->user->fullname:""}}</td>
                                                                            <td>{{$del->order->vendor?$del->order->vendor->name:''}}</td>
                                                                            <td>{{$del->source}}</td>
                                                                            <td>{{$del->destination}}</td>
                                                                            <td>₦{{$del->amount}}</td>
                                                                            <td>{{date_format(date_create($del->picked),'d M Y g:i a')}}</td>
                                                                            <td>{{date_format(date_create($del->delivered),'d M Y g:i a')}}</td>
                                                                            <td>{{$del->status==1?'Picked':'Delivered'}}</td>
                                                                        </tr>
                                                                    @endforeach
                                                                    </tbody>
                                                                    <tfoot>
                                                                    <tr>
                                                                        <th>User</th>
                                                                        <th>Vendor</th>
                                                                        <th>From</th>
                                                                        <th>To</th>
                                                                        <th>Amount</th>
                                                                        <th>Picked at</th>
                                                                        <th>Delivered at</th>
                                                                        <th>Status</th>
                                                                    </tr>
                                                                    </tfoot>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    @endif
                                </div>

                                <div class="tab-pane container-fluid" id="transaction" aria-labelledby="transaction-tab" role="tabpanel">
                                    @if(count($rider->transaction) < 1)
                                        <div class="alert alert-primary alert-dismissible mb-2" role="alert">
                                            <div class="d-flex align-items-center">
                                                <i class="bx bx-star"></i>
                                                <span>
                                                    No Transaction Available
                                                </span>
                                            </div>
                                        </div>
                                    @else
                                        <section id="basic-datatable">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="card">
                                                        <div class="card-content">
                                                            <div class="card-body card-dashboard">
                                                                <div class="table-responsive">
                                                                    <table class="table zero-configuration">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>User</th>
                                                                            <th>Amount</th>
                                                                            <th>P_reference</th>
                                                                            <th>Reference</th>
                                                                            <th>Status</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody class="table-hover">
                                                                        @foreach($rider->transaction as $trans)
                                                                            <tr>
                                                                                <td>{{$trans->user?$trans->user['fullname']:''}}</td>
                                                                                <td>{{$trans->amount}}</td>
                                                                                <td>{{$trans->p_reference}}</td>
                                                                                <td>{{$trans->reference}}</td>
                                                                                <td class="text-center">
                                                                                    {{$trans->status==1?'Successful':'Not successful'}}
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                        </tbody>
                                                                        <tfoot>
                                                                        <tr>
                                                                            <th>User</th>
                                                                            <th>Amount</th>
                                                                            <th>P_reference</th>
                                                                            <th>Reference</th>
                                                                            <th>Status</th>
                                                                        </tr>
                                                                        </tfoot>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    @endif
                                </div> 

                                <div class="tab-pane container-fluid" id="bank" aria-labelledby="bank-tab" role="tabpanel">
                                    @if($rider->user->bankDetails == null)
                                        <div class="alert alert-primary alert-dismissible mb-2" role="alert">
                                            <div class="d-flex align-items-center">
                                                <i class="bx bx-star"></i>
                                                <span>
                                                    No Bank details Available
                                                </span>
                                            </div>
                                        </div>
                                    @else 
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <div class="card bank-details" style="background-color:transparent;">
                                                    <div class="card-content">
                                                        <div class="card-body">
                                                            <div class="card-text">
                                                                <b class="text-capitalize">{{$rider->user->bankDetails->bank}}</b> 
                                                            </div>
                                                            <div class="card-no text-center">
                                                                <p> {{$rider->user->bankDetails->number}} </p>
                                                            </div>
                                                            <div class="card-fi d-flex justify-content-between">
                                                                <h5 class="m-0">{{$rider->user->bankDetails->name}}</h5>
                                                                <div class="card-imgs">
                                                                    <img src="{{asset('app-assets/images/lg.png')}}" width="100%" alt="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- user profile content section start -->
                </div>
            </div>

            <div class="modal fade" id="creditWallet" tabindex="-1" role="dialog" aria-labelledby="creditWallet" aria-modal="true">
                <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="creditWallet">Credit rider wallet</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="/campus-admin/credit-rider" method="POST">
                                @csrf
                                <input type="hidden" name="rider_id" value="{{$rider->id}}">
                                <fieldset>
                                    <label for="amt">Amount</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">₦</span>
                                        </div>
                                        <input type="text" name="amount" class="form-control" aria-describedby="basic-addon1">
                                    </div>
                                </fieldset>
                                <button type="submit" class="btn btn-success mt-2"> Credit wallet</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- page user profile ends -->

    </div>



@endsection

@section('script')
<script>
    document.getElementById('fmonth').style.display = 'none';
    document.getElementById('smonth').style.display = 'none';
    document.getElementById('year').style.display = 'none';
    function myFunction(){
        var x= document.getElementById("selt").value;
        if(x == 'smonth'){
            document.getElementById('smonth').style.display = 'block';
            document.getElementById('all').style.display = 'none';
            document.getElementById('fmonth').style.display = 'none';
            document.getElementById('year').style.display = 'none';
        }else if(x == 'fmonth'){
            document.getElementById('fmonth').style.display = 'block';
            document.getElementById('all').style.display = 'none';
            document.getElementById('year').style.display = 'none';
            document.getElementById('smonth').style.display = 'none';
        }else if(x == 'yr'){
            document.getElementById('year').style.display = 'block';
            document.getElementById('all').style.display = 'none';
            document.getElementById('smonth').style.display = 'none';
            document.getElementById('fmonth').style.display = 'none';
        }else{
            document.getElementById('all').style.display = 'block';
            document.getElementById('fmonth').style.display = 'none';
            document.getElementById('smonth').style.display = 'none';
            document.getElementById('year').style.display = 'none';
        }

    }
</script>
@endsection
