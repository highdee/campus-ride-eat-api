@extends('inc.layout')
@section('u_rider')
    active
@endsection
@section('content')
<div class="content-header row">
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">Unverified Riders</h5>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb p-0 mb-0">
                        <li class="breadcrumb-item"><a href="/campus-admin"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active">Riders List
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    @include('inc.notification') 
    <section id="basic-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table zero-configuration">
                                    <thead>
                                        <tr>
                                            <th>User Name</th>
                                            <th>Rider name</th>
                                            <th>Vehicle type</th>
                                            <th>Address</th>
                                            <th>Vehicle Registration number</th>
                                        </tr>  
                                    </thead>
                                    <tbody class="table-hover">
                                        @foreach($riders as $rider)
                                            <tr onclick="window.location.href='/campus-admin/unverified-rider/{{$rider->id}}'">                                
                                                <td>{{$rider->user?$rider->user->fullname:''}}</td>
                                                <td>{{$rider->name}}</td> 
                                                <td>{{$rider->vehicle_type}}</td>     
                                                <td>{{$rider->address}}</td> 
                                                <td>{{$rider->vehicle_reg_number}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>User Name</th>
                                            <th>Rider name</th>
                                            <th>Vehicle type</th>
                                            <th>Address</th>
                                            <th>Vehicle Registration number</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection