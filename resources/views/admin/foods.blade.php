@extends('inc.layout')
@section('foodActive')
    active
@endsection
@section('content')
<div class="content-header row">
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">Foods</h5>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb p-0 mb-0">
                        <li class="breadcrumb-item"><a href="/campus-admin"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active">Foods List
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    @include('inc.notification')
    <section id="basic-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table zero-configuration">
                                    <thead>
                                        <tr>
                                            <th>Food Name</th>
                                            <th>Vendor</th>
                                            <th>Type</th>
                                            <th>Price</th>
                                            <th>Available</th>
                                            <th>Feature</th>
                                            <th>Time</th>
                                            <th>Description</th>
                                        </tr>
                                    </thead>
                                    <tbody class="table-hover">
                                        @foreach($foods as $food)
                                            <tr>
                                                <td onclick="window.location.href='/campus-admin/food/{{$food->id}}'">{{$food->name}}</td>
                                                <td onclick="window.location.href='/campus-admin/food/{{$food->id}}'">{{$food->vendor_details?$food->vendor_details['name']:''}}</td>
                                                <td onclick="window.location.href='/campus-admin/food/{{$food->id}}'">
                                                    @if($food->item_type)
                                                        {{$food->item_type->title}}
                                                    @endif
                                                </td>
                                                <td onclick="window.location.href='/campus-admin/food/{{$food->id}}'">{{$food->price}}</td>
                                                <td onclick="window.location.href='/campus-admin/food/{{$food->id}}'">{{$food->available == 1?'Available':'Not available'}}</td>
                                                <td class="td-toggle">
                                                    <form action="/campus-admin/food-feature-status" method="POST" id="Statustoggler{{$food->id}}">
                                                        @csrf
                                                        <input type="hidden" name="food_id" value="{{$food->id}}">
                                                        <div class="toggle-btn input-group">
                                                            <label class="switch">
                                                                <input type="checkbox" {{$food->featured == 1? 'checked' : ''}} name="status" onclick="document.getElementById('Statustoggler{{$food->id}}').submit()">
                                                                <span class="slider round"></span>
                                                            </label>
                                                        </div>
                                                    </form>
                                                </td>
                                                <td onclick="window.location.href='/campus-admin/food/{{$food->id}}'">{{$food->time}}</td>
                                                <td onclick="window.location.href='/campus-admin/food/{{$food->id}}'">{{$food->description}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Food Name</th>
                                            <th>Vendor</th>
                                            <th>Type</th>
                                            <th>Price</th>
                                            <th>Available</th>
                                            <th>Feature</th>
                                            <th>Time</th>
                                            <th>Description</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
