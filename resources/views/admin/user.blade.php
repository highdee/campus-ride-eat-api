@extends('inc.layout')
@section('content')
<div class="content-header row">
</div>
<div class="content-body">
    @include('inc.notification')
    <!-- page user profile start -->
    <section class="page-user-profile">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="tab-content">
                            <div class="tab-pane active" id="feed" aria-labelledby="feed-tab" role="tabpanel">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="card">
                                            <div class="card-content">
                                                <div class="card-body">
                                                    <div class="d-flex justify-content-between mb-2 align-items-center">
                                                        <div class="d-flex align-items-center">
                                                            <h4 class="card-title mb-0">User details</h4>
                                                            <span class="badge text-lowercase ml-1 {{$user->status == 1 ? 'badge-success' : 'badge-danger'}}">{{$user->status == 1 ? 'Active' : 'banned'}}</span>
                                                        </div>
                                                        <div class="d-flex align-items-center">
                                                            <button class="btn btn-primary mr-1" data-toggle="modal" data-target="#creditWallet">Credit Wallet</button>
                                                            <button class="btn btn-secondary" data-toggle="modal" data-target="#mailUser">Send Mail</button>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-12">
                                                            <h5><small class="text-muted">Name</small></h5>
                                                            <h6 class="font-weight-light">{{$user->fullname}}</h6>
                                                        </div>
                                                        <div class="col-12">
                                                            <h5><small class="text-muted">Email</small></h5>
                                                            <h6 class="font-weight-light">{{$user->email}}</h6>
                                                        </div>
                                                        <div class="col-12">
                                                            <h5><small class="text-muted">Phone</small></h5>
                                                            <h6 class="font-weight-light">{{$user->phone}}</h6>
                                                        </div>
                                                        <div class="col-12">
                                                            <h5><small class="text-muted">Device id</small></h5>
                                                            <h6 class="font-weight-light">{{$user->device_id}}</h6>
                                                        </div>
                                                    </div>
                                                    <div class="mt-3 d-flex">
                                                        <form action="/campus-admin/banUser" method="Post">
                                                            @csrf
                                                            <input type="hidden" value="{{$user->id}}" name="user_id">
                                                            <input type="submit" class="btn {{ $user->status == 1 ? 'btn-danger' : 'btn-success'}}" value="{{ $user->status == 1 ? 'Ban user' : 'Activate user'}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Click to {{ $user->status == -1 ? 'activate' : 'ban'}} user">

                                                        </form>
                                                        <form action="/campus-admin/deleteUser"  method="POST" class="ml-1">
                                                            @csrf
                                                            <input type="hidden" value="{{$user->id}}" name="user_id">
                                                            <button class="btn btn-danger">Delete</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="card">
                                            <div class="card-content">
                                                <div class="card-body text-center">
                                                    <div class="text-muted">Balance</div>
                                                    <h3>₦{{$user->wallet['amount']}}</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="creditWallet" tabindex="-1" role="dialog" aria-labelledby="creditWallet" aria-modal="true">
                <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="creditWallet">Credit user wallet</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="/campus-admin/credit-user" method="POST">
                                @csrf
                                <input type="hidden" name="user_id" value="{{$user->id}}">
                                <fieldset>
                                    <label for="amt">Amount</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">₦</span>
                                        </div>
                                        <input type="text" name="amount" class="form-control" aria-describedby="basic-addon1">
                                    </div>
                                </fieldset>
                                <button type="submit" class="btn btn-success mt-2"> Credit wallet</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="mailUser" tabindex="-1" role="dialog" aria-labelledby="mailUser" aria-modal="true">
                <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="mailUser">Send mail to user</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="/campus-admin/mail-user" method="POST">
                                @csrf
                                <input type="hidden" name="user_id" value="{{$user->id}}">
                                <div class="form-group">
                                    <label for="title">Subject</label>
                                    <input type="text" name="title" class="form-control" id="title">
                                </div>
                                <div class="form-group">
                                    <label for="comm">Message:</label>
                                    <textarea class="form-control" name="message" id="comm" cols="10" rows="5"></textarea>
                                </div>
                                <button type="submit" class="btn btn-secondary mt-2"> Send Mail</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
