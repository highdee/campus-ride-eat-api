@extends('inc.layout')
@section('content')
<div class="content-header row">
</div>
<div class="content-body">
    <!-- page user profile start -->
    <section class="page-user-profile d-flex justify-content-center">
        <div class="card" style="width:35em; margin-top: 25px;">
            <div class="card-content">
                <div class="card-body">
                    <h4 class="card-title mb-1">Verify token</h4>
                    
                    <form action="/campus-admin/verify-user-credit" method="POST">
                        @csrf
                        @include('inc.notification')
                        <input type="hidden" name="user_id" value="{{$user->id}}">
                        <div class="form-group mb-1 mt-n2">
                            <label class="text-bold-600" for="name">Name</label>
                            <input type="text" class="form-control" value="{{$user->fullname}}" id="name">
                        </div>
                        <fieldset class="mb-1">
                            <label for="amt">Amount</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">₦</span>
                                </div>
                                <input type="text" value="{{$amount}}" class="form-control" aria-describedby="basic-addon1">
                            </div>
                        </fieldset>
                        <div class="form-group mb-1">
                            <label for="comm">Narration:</label>
                            <textarea class="form-control" name="narration" id="comm" cols="10" rows="5"></textarea>
                        </div>
                        <div class="form-group mb-1">
                            <label class="text-bold-600" for="exampleInputEmail1">Token</label>
                            <input type="text" class="form-control" name="token" id="exampleInputEmail1">
                        </div>
                        {{-- <fieldset class="mb-1">
                            <div class="checkbox">
                                <input type="checkbox" class="checkbox-input" name="notify_customer" id="checkbox3">
                                <label for="checkbox3">Notify customer</label>
                            </div>
                        </fieldset> --}}
                        <div class="form-group mb-0">
                            <button type="submit" class="btn btn-success">Verify Token</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
