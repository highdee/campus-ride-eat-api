@extends('inc.layout')
@section('pay4me')
    active
@endsection
@section('content')
<div class="content-header row">
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">Pay4me</h5>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb p-0 mb-0">
                        <li class="breadcrumb-item"><a href="/campus-admin"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active">Pay4me Details
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    @include('inc.notification')
    <section id="basic-datatable">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="card-text">
                                <p><b>User:</b> {{$pay->user->fullname}}</p>
                                <p><b>Order:</b>
                                    <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Quantity</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="table-hover">
                                                    @foreach($foods as $food)
                                                        <tr>
                                                            <td>{{$food['name']}}</td>
                                                            <td>{{$food['quantity']}}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Quantity</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                </p>
                                <p><b>Url:</b> {{$pay->url}}</p>
                                <p><b>Amount:</b> ₦{{$pay->amount}}</p>
                                <p><b>Created Date:</b> {{date('D, M-Y d h:i a', strtotime($pay->created_at))}}</p>
                                <p><b>Created Date:</b> {{date('D, M-Y d h:i a', strtotime($pay->expired_at))}}</p>
                                <p>
                                    <b>Status:</b>
                                    <span class="badge {{strtolower($pay->status) == 'pending'?'badge-primary': (strtolower($pay->status) == 'started'?'badge-secondary':'badge-success')}} ">
                                        {{$pay->status}}
                                    </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table zero-configuration">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Amount</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody class="table-hover">
                                        @foreach($friends as $friend)
                                            <tr>
                                                <td>{{$friend->friend_name}}</td>
                                                <td>₦{{$friend->amount}}</td>
                                                <td>{{date('D, M-Y d h:i a', strtotime($friend->created_at))}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Name</th>
                                            <th>Amount</th>
                                            <th>Date</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
