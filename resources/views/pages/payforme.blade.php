@extends('inc.nav')
@section('app-asset')
<!-- fonts -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Roboto slab&family=Roboto slab+Slab&display=swap" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<style>
    body{
        padding: 0;
        margin: 0;
        box-sizing: border-box;
        height: 100vh;
        width: 100vw;
        overflow: hidden;
        font-family: 'Roboto slab', sans-serif;
    }
    *{
        overflow: hidden;
    }
    .py-t{
        background-color: rgb(128, 32, 32);
        height: 100vh;
        background: url('../../../app-assets/images/bb.jpg');
        background-position: top;
        width: 100%;
        background-repeat: no-repeat;
        background-size: cover;
        box-shadow: 2px 3px 11px #232323;
        position: relative;
    }
    .py-t::after{
        display: block;
        background: #000000d6;
        position: absolute;
        left: 0px;
        right: 0px;
        top: 0px;
        bottom: 0px;
        z-index: 9999;
        margin: auto;
        content: '';
    }
    .py-t .content1, .py-t .content{
        background: white;
        text-align: center;
        /* padding: 10px 5px; */

    }
    .textbox{
        height: 50px;
    }
    .py-t .content2{
        background: white;
        text-align: center;
        padding: 10px 5px;
    }
    .py-t .content1 h4{
        font-size: 19px;
        text-align: center;
        font-family: 'Roboto slab', sans-serif;
        margin: 0;
        padding: 1px 5px;
    }
    .py-t .content h4{
        font-size: 19px;
        text-align: center;
        font-family: 'Roboto slab', sans-serif;
        margin: 0;
        padding: 1px 5px;
    }
    .main-cnt{
        margin-top: 20px;
    }
    button:focus{
        outline:none !important
    }
    .content1 .amt p{
        color: #ff8e17;
        font-family: 'Roboto slab', sans-serif;
        margin: 8px;
        font-size: 30px;
        font-weight: 400;
    }
    .content1 .com{
        width: 70%;
        margin: 5px auto;
    }
    .content1 .com p{
        font-family: 'Roboto slab', sans-serif;
        font-size: 11px;
        color: gray;
    }
    .content1 .btns{
        width: 80%;
        margin: 2px auto;
        padding-bottom: 20px;
    }
    .content1 .btns button{
        width: 70%;
        padding: 13px 12px;
        background: #ff8e17;
        border-radius: 33px;
        color: white;
        border: none;
        cursor: pointer;
        margin: 12px auto;
        margin-top: 0;
        font-size: 17px;
        transition: .2s;
    }
    .t-head p{
        margin-bottom:0px;
        padding-right:4px;
        font-size: 11px;
        color: #1e7e34;
    }
    .content1, .content2, .content{
        width: 100%;
        transition:all .3s;
        flex: 0 0 auto;
    }
    .content2{
        /* margin-right: -100%;  */
    }
    .content3{
        /* margin-right: -100%;  */
    }
    .content4{
        /* margin-right: -200%;  */
    }
    .content1 .btns button:hover{
        background: #9b6f01;
    }
    .py-t .fl-bx{
        display: flex;
        position: fixed;
        right: 0;
        left: 0;
        margin: 1px auto;
        max-width: 400px;
        bottom: -100%;
        max-height: 80%;
        border-top-left-radius:20px ;
        border-top-right-radius:20px ;
        z-index: 99999999999999;
    }
    .numbtns{
        display: flex;
        justify-content: center;
        align-items: center;
        width: 98%;
        flex-direction: column;
        margin: 10px auto;
        margin-top: 10px;
        /* background: lightgray; */
    }
    .numbtns .nums-in-3{
        display: flex;
        justify-content: space-between;
        align-items: center;
        margin-bottom:10px ;
        width: 75%;
    }
    .numbtns .nums-in-3 .num{
        margin: 2px 5px;
        background: #f5f5f5;
        border-radius: 50%;
        height: 65px;
        display: flex;
        cursor: pointer;
        width: 65px;
        font-size: 24px;
        align-items: center;
        color: black;
        justify-content: center;
        transition: .3s;
    }
    .numbtns .nums-in-3 .num:hover{
        background: #cacaca;

    }
    .textbox input{
        border: none;
    }
    .py{
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        height: auto;
    }
    .clear{
        width: 80px;
    }
    .vvv{
        visibility: hidden;
        opacity: 0;
    }
    .adv{
        /* justify-content: flex-end!important; */
        /* width: 74%!important; */
        margin-bottom: 20px;
    }
    .adv .clear i{
        padding: 8px 23px;
        margin-top: 15px;

    }
    .py{
        padding: 0px 20px;
    }
    .py button{
        padding: 7px 15px;
        border: none;
        border-radius: 5px;
        margin-top: 10px;
        width: 80%;
        background: #ff8e17;
        font-size: 18px;
        color: #ffffff;
        cursor: pointer;
        transition: .3s;
    }
    .py button:hover{
        background: #a17503;

    }
    .svg-bx{
        margin-top:10px ;
    }
    .t-head{
        display: flex;
        justify-content: space-between;
        align-items: center;
        margin: 2px auto;
        width: 65%;
        margin-top: 10px;
    }
    .spec{
        /* margin-right: 19%!important; */
    }
    #amt-screen{
        font-size: 25px;
        text-align: right;
        width: 61%;
        margin: auto;
        margin-top: 10px;
    }
    #emailController{
        width: 82%;
        margin: auto;
        margin-bottom: 10px;
        margin-top:10px;
        font-size: 14px;
        font-weight: 300;
        border-color:#d3d3d375;
    }
</style>
<style>
    .content .amt p{
        color: #ff8e17;
        font-family: 'Roboto slab', sans-serif;
        margin: 8px;
        font-size: 30px;
        font-weight: 400;
    }
    .content .com{
        width: 70%;
        margin: 5px auto;
    }
    .content .com p{
        font-family: 'Roboto slab', sans-serif;
        font-size: 11px;
        color: gray;
    }
    .content .btns{
        width: 80%;
        margin: 2px auto;
        padding-bottom: 20px;
    }
    .content .btns button{
        width: 70%;
        padding: 9px 12px;
        background: #ff8e17;
        border-radius: 33px;
        color: white;
        border: none;
        cursor: pointer;
        margin: 12px auto;
        margin-top: 0;
        font-size: 17px;
        transition: .2s;
    }
    .content .btns button:hover{
        background: #9b6f01;
    }
</style>

@endsection
@section('content')
<div class="py-t" >
    <p class="text-danger m-0" id="error_text"></p>
    @if($status == 'good')
    <div class="fl-bx">
        <div class="content1">
            <div class="svg-bx">
                <img src="{{asset('app-assets/images/OBJECTS.png') }}" alt="">
            </div>
            <div class="main-cnt">
                <h4><span>{{$name}}</span> has asked you to help pay for his/her order on Rideeats</h4>
                <div class="amt">
                    <p><span>&#8358;{{number_format($amount, 0)}}</span></p>
                </div>
                <div class="com">
                    <p>You'er a good friend, and only good friends pay for their friend's meal</p>
                </div>
                <div class="btns">
                    <button class="payfull">Pay &#8358;{{number_format($amount, 0)}}</button>
                    <button id="payPart">Choose to pay part </button>
                    <button style="background:#bd2130">Decline</button>
                </div>
            </div>
        </div>
        <div class="content2">
            <div class="cal">
                <div class="t-head">
                    <i class="fas fa-arrow-left"></i>
                    <p>Enter amount to pay</p>
                </div>
                <div class="textbox screen">
                    <p id="amt-screen"></p>
                </div>
                <p style="font-size: 11px;text-align: left;width: 65%;margin: auto;" class="text-danger info">Max is {{number_format($amount)}}</p>
                <div class="numbtns">
                    <div class="nums-in-3">
                        <div class="num" data='1'>1</div>
                        <div class="num" data='2'>2</div>
                        <div class="num" data='3'>3</div>
                    </div>
                    <div class="nums-in-3">
                        <div class="num" data='4'>4</div>
                        <div class="num" data='5'>5</div>
                        <div class="num" data='6'>6</div>
                    </div>
                    <div class="nums-in-3">
                        <div class="num" data='7'>7</div>
                        <div class="num" data='8'>8</div>
                        <div class="num" data='9'>9</div>
                    </div>
                    <div class="adv nums-in-3">
                        <div class="num spec vvv" >0</div>
                        <div class="num spec" data='0'>0</div>

                        <div onClick="clearInput()" class="clear"><i class="fas fa-backspace" id="clear"></i></div>
                    </div>
                    <div class="py">
                        <button class="paynow">Pay</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="content content3">
            <div class="t-head" style="width: 80%;margin-top: 20px;">
                <i class="fas fa-arrow-left"></i>
            </div>
            <div class="main-cnt" style="padding: 0px 0px;">
                <h4>Thank You For Proceeding</h4>
                <div class="com">
                    <p class="p-0 m-0">You will be paying:</p>
                </div>
                <div class="amt">
                    <p><span id="previewamount">&#8358;{{number_format($amount, 0)}}</span></p>
                </div>
                <form id="form">
                    <div class="">
                        <input id="emailController" type="text" class="form-control" placeholder="Enter your email" name="email" required />
                    </div>
                    <div style="width: 80%;text-align: center;margin: auto;font-size: 10px;" class="form-check p-0">
                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" required>
                        <label class="form-check-label" for="flexCheckDefault">
                            I agree to the <a target='_blank' href="/payforme/terms-conditions" class="text-warning">Terms and Conditions</a> of Rideeats Payforme
                        </label>
                    </div>
                    <div class="btns">
                        <button>Proceed</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="content content4">
            <div class="svg-bx">
                <img style="width: 200px;" src="{{asset('app-assets/images/pages/rideeatLogo.png') }}" alt="">
            </div>
            <div class="main-cnt" style="padding: 0px 15px;">
                <h4 style="color:#ff8e17">Thank You For Paying For A Friend</h4>
                <div class="bg-inner">
                    <p style="color: black;font-size: 18px;" class="bg-title">Your friend {{$name}} just ordered a meal through Rideeats Food Ordering & Delivery Mobile App</p>
                    <div class="bg-app-btn">
                        <a target="_blank" href="https://play.google.com/store/apps/details?id=com.highdee.Campus_Ride_eat" class="">
                            <img src="{{asset('app-assets/images/pages/googlePlaystore.png')}}" class="goog" alt="">
                        </a>
                        <a target="_blank" href="https://apps.apple.com/us/app/rideeats/id1559552064">
                            <img src="{{asset('app-assets/images/pages/app-store-logo.png')}}" class="apple" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    @if($status == 'completed')
        <div class="fl-bx" style="min-height: 400px;">
            <div class="content1">
                <div class="svg-bx">
                    <img style="width: 200px;" src="{{asset('app-assets/images/happy.gif') }}" alt="">
                </div>
                <div class="main-cnt" style="padding: 0px 15px;">
                    <h4 style="color:green">A Friend Already Paid</h4>
                    <p style="
                        font-size: 13px;
                        color: gray;
                        font-weight: 100;
                        width: 90%;
                        margin: auto;
                ">A good friend like you has made this payment</p>
                    <div class="bg-inner">
                        <div class="bg-app-btn">
                            <a style="text-decoration: none;
                            color: #ff9800;
                            font-weight: 200;
                            border: 1px solid #fff9e5;
                            padding: 5px 10px;
                            border-radius: 5px;"
                               href="/" class="">
                                <i class="fas fa-home"></i>
                                Rideeats Home
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if($status == 'not_found')
    <div class="fl-bx" style="min-height: 500px;">
        <div class="content1">
            <div class="svg-bx">
                <img style="width: 200px;" src="{{asset('app-assets/images/404.svg') }}" alt="">
            </div>
            <div class="main-cnt" style="padding: 0px 15px;">
                <h4 style="color:red">Invalid Payment Link</h4>
                <p style="
                        font-size: 12px;
                        color: gray;
                        font-weight: 100;
                        width: 90%;
                        margin: auto;
                ">The payment you used is invalid, Please get the correct link and try again</p>
                <div class="bg-inner">
                    <div class="bg-app-btn">
                        <a style="text-decoration: none;
                            color: #ff9800;
                            font-weight: 200;
                            border: 1px solid #fff9e5;
                            padding: 5px 10px;
                            border-radius: 5px;"
                            href="/" class="">
                            <i class="fas fa-home"></i>
                            Rideeats Home
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
@endsection
@section('app-js')
<script src="https://kit.fontawesome.com/a076d05399.js"></script>
<script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>
<script src="https://checkout.flutterwave.com/v3.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<script>
    window.onload=function () {
        setTimeout(() => {
            var fxup = document.querySelector('.fl-bx');
            fxup.style.bottom='-10px'
            fxup.style.transition='.3s'
        }, 500);
    }
</script>
@if($status == 'good')
    <script>
        var real_amount = {{$amount}};
        var amount = real_amount;
        function initialiseFlutterWave(){
            var email = $("#emailController").val();
            if(email.length == 0) return false;

            data={
                email:email,
                amount:amount
            };

            FlutterwaveCheckout({
                public_key: "FLWPUBK-5468a0759d1838b9bd0addb32dd163b3-X",
                tx_ref: '{{$ref}}',
                amount: data.amount,
                currency: "NGN",
                country: "NG",
                payment_options: "account, card, banktransfer, qr, ussd",
                customer: {
                    email: data.email,
                    // phone_number: data.phone,
                    // name: data.name,
                },
                callback: function (data) {
                    console.log(data);
                    var content1 = document.querySelector('.content1');
                    content1.style.marginLeft='-300%'
                    closePaymentModal();
                },
                onclose: function() {
                    // close modal
                },
                customizations: {
                    title: "Rideeats",
                    description: 'Pay for a friend',
                    logo: "https://rideeat.org/app-assets/images/logo.jpg",
                },
            });
        }

        function closePaymentModal() {
            document.getElementsByName('checkout')[0].setAttribute('style',
                'position:fixed;top:0;left:0;z-index:-1;border:none;opacity:0;pointer-events:none;width:100%;height:100%;');
            document.body.style.overflow = '';
        }
    </script>
    <script>
        var input='';
        var paypart = document.querySelector('#payPart');
        var payfull = $('.payfull');
        var paynow = $('.paynow');

        paypart.addEventListener('click', function () {
            var partcontent = document.querySelector('.content2');
            var selecttopay = document.querySelector('.content1');
            selecttopay.style.marginLeft='-100%'
            partcontent.style.marginRight='0'
        })

        var partback = $('.fa-arrow-left');
        partback.on('click', function () {
            var content1 = document.querySelector('.content1');
            content1.style.marginLeft='0%'
        })

        payfull.on('click', function () {
            $("#previewamount").html("&#8358;"+real_amount);

            var content1 = document.querySelector('.content1');
            content1.style.marginLeft='-200%'
        })

        paynow.on('click', function () {
            $("#error_text").html("");
            var amt = parseInt(input);

            if(amt < 10 || input.length == 0) {
                $("#error_text").html("Amount is too low");
                return false
            }

            amount = amt;
            $("#previewamount").html("&#8358;"+amount);
            var content1 = document.querySelector('.content1');
            content1.style.marginLeft='-200%'
        })

        $("#form").on('submit', function(e){
            e.preventDefault();
            initialiseFlutterWave({{$amount}})
        })


        function moneyformat(money) {
            var n = Number(money);
            var value = n.toLocaleString("en");
            return value;
        }

        function printNumb(numb) {
            $("#error_text").html("");

            var temp = input+''+numb
            temp = temp.replace(',', '');
            if(parseInt(temp) > real_amount) return false;
            input = parseInt(temp);
            document.getElementById('amt-screen').innerText = moneyformat(input);
        }

        function clearInput() {
            var temp = input.toString().split('');
            temp.pop();
            input = temp.join('');
            document.getElementById('amt-screen').innerText = input;
        }

        var number =  document.getElementsByClassName('num')
        for (let i = 0; i < number.length; i++) {
            number[i].addEventListener('click', function () {
                printNumb(number[i].getAttribute('data'))
            })
        }
    </script>
@endif
@endsection
