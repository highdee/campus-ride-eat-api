@extends('inc.nav')
@section('content')

        <!-- Hero banner -->
        <div class="bg-wrapper  abt-bg-img">
            <div class="abt-inner  width max-w">
                <div class="tc-wrap">
                    <h2 class="tc-title">RIDE EATS PARTNER RESTAURANTS T&C </h2>
                    <p class="abt-txt-secTitle">
                        GUIDELINES
                    </p>
                    <div class="abt-li-txt">
                        <p class="abt-d-txt">
                                <span class="abt-box">
                                    <span></span>
                                </span>
                            <span class="d-abt-txt"> Keep your menu up to date - by giving us at least three days’ notice of changes you require us to make for you or by making your own changes through Menu Manager. </span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">Ensure that all relevant menu items are available to be ordered by a customer during your opening hours, and accept and reject Pickup Orders as appropriate. You should not accept cash payment for Pickup Orders.</span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">Ensure that Pickup Orders are prepared using all due skill, care and diligence in line with best practice in your industry.</span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">Ensure that all Pickup Orders are packaged in a way that avoids tampering, minimizes spillages, and maintains the order at an appropriate temperature.</span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">Prepare Pickup Orders promptly, accurately and in accordance with the timescales communicated via the Platform. In particular, you must ensure that menu items:</span>
                        </p>
                    </div>
                </div>
                <div class="tc-wrap">
                    <p class="abt-txt-secTitle">
                        PICK UP ORDER REQUIREMENTS
                    </p>
                    <div class="abt-li-txt">
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    Take account of any information relating to customer allergies provided with the Pickup order.
                                </span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    Ensure that Pickup Orders are available for collection by customers in a timely manner.
                                </span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    Ensure that the order number on the Pickup Order packaging corresponds with the order number provided by Ride Eats before the Pickup Order is handed over to the relevant Rider.
                                </span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    Provide each customer with an official receipt for their Pickup order (and a VAT receipt, if applicable) if requested.
                                </span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    Ensure that customers are at all times treated in a professional manner by staff and are provided with access to a safe pick up area.
                                </span>
                        </p>
                    </div>
                </div>
                <div class="tc-wrap">
                    <p class="abt-txt-secTitle">
                        FOOD SAFETY AND HYGIENE
                    </p>
                    <p class="abt-d-txt">
                        We take customer safety very seriously and we’re confident that our Restaurants do too. Stringent food hygiene processes are essential to ensuring customers are safe and their food is amazing. This policy sets out the food hygiene and safety standards that we expect Restaurants to meet in order to sell their products through the Platform.
                    </p>
                    <p class="abt-d-txt">
                        What we expect from our Restaurant Partners:
                    </p>
                    <div class="abt-li-txt">
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    You are responsible for ensuring that the products that you sell are safe and compliant with Applicable Law.  If you are uncertain about any legal requirements, please seek advice from an expert.
                                </span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    We expect you to have a food safety management system in place that deals with all aspects of food safety and hygiene for your business in compliance with food hygiene regulations, including (but not limited to):
                                </span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    Cross-contamination;
                                </span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    Allergens;
                                </span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    Cleaning and Waste;
                                </span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    Maintenance;
                                </span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    Chilling, defrosting, freezing, cooking; reheating;
                                </span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    Temperature control: chilled and hot holding - in particular, while awaiting collection by a Rider;
                                </span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    Personal hygiene and fitness to work;
                                </span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    Pest Control;
                                </span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    Takeaway packaging;
                                </span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    Staff training;
                                </span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    Supplier assurance;
                                </span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    Supplier assurance;
                                </span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    Traceability;
                                </span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    Monitoring and auditing; and
                                </span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    Recall/withdrawal.
                                </span>
                        </p>
                    </div>
                </div>
                <div class="tc-wrap">
                    <p class="abt-txt-secTitle">
                        REQUIRED SERVICE STANDARDS
                    </p>
                    <div class="abt-li-txt">
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    Use reasonable precautions to reject less than 1% of Pickup Orders received through the Platform.
                                </span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    Use reasonable precautions to ensure that Pickup Orders are available for collection by customers at the time communicated on the Platform and keep customers waiting for Pickup Orders for no longer than five minutes.
                                </span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    Use reasonable precautions to ensure that no more than 1% of Pickup Orders contain errors.
                                </span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    You should be available for orders for 98% of your opening hours on the Platform.
                                </span>
                        </p>
                    </div>
                </div>
                <div class="tc-wrap">
                    <p class="abt-txt-secTitle">
                        SUSPENSION AND TERMINATION
                    </p>
                    <div class="abt-li-txt">
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    We reserve the right to suspend your use of the Platform on giving you notice if we know or suspect you have breached these terms, or if we otherwise consider suspension reasonably necessary taking account of all relevant circumstances.
                                </span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    We will give you the reasons for the suspension where permitted by law. We will maintain the suspension until you have remedied the breach to our reasonable satisfaction or we no longer consider the suspension necessary in the circumstances.
                                </span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    These terms take effect on acceptance (see above) and continue until terminated by either party giving 30 days’ written notice to the other. If permitted by law and where we consider immediate termination necessary in the circumstances, we may give you written notice to terminate these terms with immediate effect.
                                </span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    A service charge of 10% of sold items will be retained, and subject to change or increment without prior approval, however with notice as deemed appropriate to the partner restaurants and private kitchen present on the platform at the time of change.  Just help me add the line
                                </span>
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
