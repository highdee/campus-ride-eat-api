@extends('inc.nav')
@section('content')

        <!-- Hero banner -->
        <div class="bg-wrapper  abt-bg-img">
            <div class="abt-inner  width max-w">
                <h2 class="abt-title">About</h2>
                <p class="abt-txt">
                    RideEat is the most convenient online food ordering site, connecting people with the best
                    restaurants around them.We believe food is a pleasure and food ordering should be fast and
                    definitely fun experience. Ordering with RideEat is as easy as a 4-step recipe:
                </p>
                <p class="abt-txt">
                    <span class="abt-txt-num">1-</span>
                    Select your food or grocery
                    <span class="abt-span-txt">Browse restaurants that deliver near you</span>
                </p>
                <p class="abt-txt">
                    <span class="abt-txt-num">2-</span>
                    Receive it at your doorstep
                    <span class="abt-span-txt">Your order will be delivered to you in no time</span>
                </p>
                <p class="abt-txt">
                    <span class="abt-txt-num">3-</span> Enjoy your order
                    <span class="abt-span-txt">Enjoy your food or grocery items</span>
                </p>
                <p class="abt-txt">
                    On your mobile, tablet, desktop or via our app, RideEat is a delicious experience!
                    RideEat Coorporate Service: With the most advanced corporate food delivery of its kind,
                    RideEat helps you treat your employees to the best lunch delivery service available.
                    For more information, contact our corporate team via corporate.food@RideEat.com.ng
                </p>
            </div>
        </div>
    </div>
@endsection
