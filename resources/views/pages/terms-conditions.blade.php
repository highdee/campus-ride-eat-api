@extends('inc.nav')
@section('content')

        <!-- Hero banner -->
        <div class="bg-wrapper  abt-bg-img">
            <div class="abt-inner  width max-w">
                <div class="tc-wrap">
                    <h2 class="tc-title">1. Terms of Use</h2>
                    <p class="abt-d-txt">
                        This Terms and Conditions Agreement sets forth the standards of use of the Food Delivery service, (“Ride Eat”) Online Service. By using the Ride Eat website, you (the ”User”) agree to these terms and conditions. If you do not agree to the terms and conditions of this Agreement, you should immediately cease all usage of this website. We reserve the right, at any time, to modify, alter, or update the terms and conditions of this agreement without prior notice. Modifications shall become effective immediately upon being posted at Ride Eats website. Your continued use of the Service after amendments are posted constitutes an acknowledgement and acceptance of the Agreement and its modifications. Except as provided in this paragraph, this Agreement may not be amended.
                    </p>
                </div>
                <div class="tc-wrap">
                    <h2 class="tc-title">2. Services</h2>
                    <p class="abt-d-txt">
                        2.1 We are a delivery service provider providing you an online platform to
                    </p>
                    <p class="abt-d-txt">
                        (a) order food online from the list of restaurants and eateries available on the website (“Vendors”);
                    </p>
                    <p class="abt-d-txt">
                        (b) pre- pay for your food order by the banking channels made available to you on the website at the time of ordering your food order to your delivery address.
                    </p>
                    <p class="abt-d-txt">
                        2.2 We do not own, sell, resell, furnish, provide, prepare, manage and/or control the Vendors or the related services provided in connection thereof.
                    </p>
                    <p class="abt-d-txt">
                        2.3 Our responsibilities are limited to: (i) facilitating the availability of the Services; and (ii) serving as the limited agent of each Vendor for the purpose of accepting payments from you for your online food order and delivering your order.
                    </p>
                </div>
                <div class="tc-wrap">
                    <h2 class="tc-title">3. Registration</h2>
                    <p class="abt-d-txt">
                        3.1 By signing up to our website you are agreeing to the terms and conditions of sale which apply to all transactions on Ride Eat.
                    </p>
                    <p class="abt-d-txt">
                        Signing up to the service means we must have the following information:
                    </p>
                    <div class="abt-li-txt">
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    Your address, including the billing address associated with the payment card.
                                </span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">Your home telephone number or mobile telephone number</span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">Your email address, so we can supply you with important information such as your order</span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt"> confirmation and delivery details</span>
                        </p>
                    </div>
                    <p class="abt-d-txt">
                        3.2 There will be two stages of order confirmation. The first will be the 'on screen confirmation' immediately after the payment has been received, and the second will be a telephone call or email to confirm your full order details.
                    </p>
                    <p class="abt-d-txt">
                        3.3 You are responsible for safeguarding your account password. You agree that you will not disclose your password to any third party and that you will take sole responsibility for any activities or actions under your Ride Eat Account, whether or not you have authorized such activities or actions. You will immediately notify us of any unauthorized use of your Ride Eat Account.
                    </p>
                    <p class="abt-d-txt">
                        3.4 You may access the Services either by (a) registering to create an account (“Personal Account”) and become a member (“Member”); or (b) you can also register to join by logging into your account with certain third party social networking sites (“SNS”) (including, but not limited to, Facebook); each such account, a “Third Party Account”, via our Services, as described below. As part of the functionality of the Services, you may link your Ride Eat Account with Third Party Accounts, by either:
                    </p>
                    <div class="abt-li-txt">
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    Providing your Third Party Account login information to us through the Services; or
                                </span>
                        </p>
                        <p class="abt-d-txt">
                            <span class="abt-box"></span>
                            <span class="d-abt-txt">
                                    Allowing us to access your Third Party Account, as is permitted under the applicable terms and conditions that govern your use of each Third Party Account
                                </span>
                        </p>
                    </div>
                    <p class="abt-d-txt">
                        3.5 You represent that you are entitled to disclose your Third Party Account login information to us and/or grant us access to your Third Party Account (including, but not limited to, for use for the purposes described herein), without breach by you of any of the terms and conditions that govern your use of the applicable Third Party Account and without obligating us to pay any fees or making us subject to any usage limitations imposed by such third party service providers.
                    </p>
                    <p class="abt-d-txt">
                        3.6 By granting us access to any Third Party Accounts, you understand that we will access, make available and store (if applicable) any content or information that you have provided to and stored in your Third Party Account (“SNS Content”) so that it is available on and through the Services via your Ride Eat Account.
                    </p>
                    <p class="abt-d-txt">
                        3.7 Unless otherwise specified in these Terms of Use, all SNS Content, if any, will be considered to be your content for all purposes of these Terms.
                    </p>
                    <p class="abt-d-txt">
                        3.8 Depending on the Third Party Accounts you choose and subject to the privacy settings that you have set in such Third Party Accounts, personally identifiable information that you post to your Third Party Accounts will be available on and through your Ride Eat Account on the Services.
                    </p>
                    <p class="abt-d-txt">
                        3.9 Please note that if a Third Party Account or associated service becomes unavailable or our access to such Third Party Account is terminated by the third party service provider, then SNS Content will no longer be available on and through the Services.
                    </p>
                    <p class="abt-d-txt">
                        3.10 We will create your Ride Eat Account for your use of the Services based upon the personal information you provide to us or that we obtain via an SNS as described above.
                    </p>
                    <p class="abt-d-txt">
                        3.11 You agree to provide accurate, current and complete information during the registration process and to update such information to keep it accurate, current and complete.
                    </p>
                    <p class="abt-d-txt">
                        3.12 We reserve the right to suspend or terminate your Ride Eat Account and your access to the Services if any information provided during the registration process or thereafter proves to be inaccurate, not current or incomplete or if your use of the website violates any company policies.
                    </p>
                </div>
                <div class="tc-wrap">
                    <h2 class="tc-title"> 4. Product prices</h2>
                    <p class="abt-d-txt">
                        4.1 The price of goods charged will be as quoted on the website at the time you confirm your order with us. Excluding any inadvertent technical error on pricing, we will honour any prices as published by the vendors at the time of placing your order. Your debit card shall only be charged for items dispatched to you.
                    </p>
                    <p class="abt-d-txt">
                        4.2 Due to differing product promotions the prices displayed on the website may vary to original vendor prices.
                    </p>
                    <p class="abt-d-txt">
                        4.3 You agree to pay all fees and charges incurred in connection with your purchase(s) including any applicable taxes. The final charges for your order may be different than those stated on the website. Ride Eat does not assume any responsibility or liability for the actions, products, and content of any third parties related to the creation or delivery of your order. We may require additional verification or information before accepting any order.
                    </p>
                </div>
                <div class="tc-wrap">
                    <h2 class="tc-title"> 5. Ordering</h2>
                    <p class="abt-d-txt">
                        5.1 Any contract for the supply of Food Delivery from this Website is between you and the Participating Restaurant; for the supply of Goods or Services from this Website any contact is between you and Ride Eat Delivery Partners. You agree to take particular care when providing us with your details and warrant that these details are accurate and complete at the time of ordering. You also warrant that the debit card details that you provide are for your own debit card and that you have sufficient funds to make the payment.
                    </p>
                    <p class="abt-d-txt">
                        5.2 You can pay by debit card or banking services at the time of booking an order
                    </p>
                    <p class="abt-d-txt">
                        5.3 Food Delivery, Goods and Services purchased from this Website are intended for your use only and you warrant that any Goods purchased by you are not for resale and that you are acting as principal only and not as agent for another party when receiving the Services.
                    </p>
                    <p class="abt-d-txt">
                        5.4 Please note that some of our Goods may be suitable for certain age ranges only. You should check that the product you are ordering is suitable for the intended recipient.
                    </p>
                    <p class="abt-d-txt">
                        5.5 When ordering from this Website you may be required to provide an e-mail address and password. You must ensure that you keep the combination of these details secure and do not provide this information to a third party.
                    </p>
                    <p class="abt-d-txt">
                        5.6 We will take all reasonable care, in so far as it is in our power to do so, to keep the details of your order and payment secure, however in the absence of negligence on our part we cannot be held liable for any loss you may suffer if a third party procures unauthorized access to any data you provide when accessing or ordering from the Website.
                    </p>
                    <p class="abt-d-txt">
                        5.7 Any order that you place with us is subject to product availability, delivery capacity and acceptance by us and the Participating Restaurant. When you place your order online, we will send you an email to confirm that we have received it. This email confirmation will be produced automatically so that you have confirmation of your order details. You must inform us immediately if any details are incorrect. The fact that you receive an automatic confirmation does not necessarily mean that either we or the Participating Restaurant will be able to fill your order. Once we have sent the confirmation email we will check availability and delivery capacity.
                    </p>
                    <p class="abt-d-txt">
                        5.8 If the ordered Food Delivery and delivery capacity is available, the Participating Restaurant will accept the contract and confirm it to Ride Eat. If the details of the order are correct, the contract for the Food Delivery, Goods or Services will be confirmed by telephone call or via text message (SMS).
                    </p>
                    <p class="abt-d-txt">
                        5.9 In the case that Goods offered by Ride Eat were ordered,Ride Eat will confirm availability together with or separately from Food Delivery.
                    </p>
                    <p class="abt-d-txt">
                        5.10 The confirmation message will specify delivery details which may or may not include the approximate delivery time specified by the Participating Restaurant and confirm the price of the Food Delivery, Goods and Services ordered.
                    </p>
                    <p class="abt-d-txt">
                        5.11 If the Food Delivery and/or Goods are not available or if there is no delivery capacity, we will also let you know by text message (SMS) or phone call.
                    </p>
                </div>
                <div class="tc-wrap">
                    <h2 class="tc-title">6. Delivery</h2>
                    <p class="abt-d-txt">
                        6.1 The delivery period quoted at the time of ordering are approximates only and may vary. Food will be delivered to the address as provided by users while ordering.
                    </p>
                    <p class="abt-d-txt">
                        6.2 If the Food is not delivered within the estimated delivery time quoted, User shall contact Ride Eat by email or support contacts and Ride Eat will try to ensure that you receive your order as quickly as reasonably possible.
                    </p>
                    <p class="abt-d-txt">
                        6.3 All risk in the Food and Food Delivery shall pass to you upon delivery.
                    </p>
                    <p class="abt-d-txt">
                        6.4 If you fail to accept delivery of Food at the time they are ready for delivery or Ride Eat is unable to deliver at the nominated time due to your failure to provide appropriate instructions or authorizations, then the Food shall be deemed to have been delivered to you and all risk and responsibility in relation to such Food shall pass to you.
                    </p>
                    <p class="abt-d-txt">
                        6.5 Any storage, insurance and other costs which Ride Eat incur as a result of the inability to deliver to you caused by incomplete, incorrect information provided shall be your responsibility and you shall indemnify Ride Eat in full for such cost.
                    </p>
                    <p class="abt-d-txt">
                        6.6 You must understand and accept that it might not be possible to deliver for some locations. In such cases, Ride Eat will inform you accordingly and arrange for cancellation of the order or delivery to any alternative delivery address(if you choose to).
                    </p>
                </div>
                <div class="tc-wrap">
                    <h2 class="tc-title">7. Cancellations and Refunds</h2>
                    <p class="abt-d-txt">
                        7.1 You must notify the participating restaurant and/or Ride Eat immediately if you decide to cancel your order, preferably by phone, and quote your order number. If the restaurant accepts your cancellation, no cancellation fee applies. If the restaurant refuses cancellation, e.g. because preparation of Food Delivery has been initiated, completed and/or delivery personnel has already been dispatched, it may not be cancelled. We will not be able to refund any order, which has been already dispatched.
                    </p>
                    <p class="abt-d-txt">
                        7.2 We may cancel a contract if the product is not available for any reason. We will notify you if this is the case and return any payment that you have made;
                    </p>
                    <p class="abt-d-txt">
                        7.3 If the cancellation was made in time and once the restaurant has accepted your cancellation, we will refund or re-credit your debit card with the full amount within 14 days, which includes the initial delivery charge (where applicable) which you paid for the delivery of the Goods or the Services, as applicable.
                    </p>
                    <p class="abt-d-txt">
                        7.4 In the unlikely event that the Participating Restaurant delivers a wrong item, you have the right to reject the delivery of the wrong item with receipt and short video evidence and you shall be fully refunded for the missing item. If the Participating Restaurant can only do a partial delivery (a few items might be not available), its staff should inform you or propose a replacement for missing items. You have the right to refuse a partial order before delivery and get a refund. Ride Eats will not be responsible for wrong or partial delivery. The issue has to be settled directly/indirectly with the Participating Restaurant.
                    </p>
                </div>
                <div class="tc-wrap">
                    <h2 class="tc-title">8. Limitation of Liability</h2>
                    <p class="abt-d-txt">
                        8.1 Great care has been taken to ensure that the information available on this Website is correct and error free. We apologize for any errors or omissions that may have occurred. We cannot warrant that use of the Website will be error free or fit for purpose, or that defects will be corrected, or that the site or the server that makes it available are free of viruses or bugs or represents the full functionality, accuracy, reliability of the Website and we do not make any warranty whatsoever, whether express or implied, relating to fitness for purpose, or accuracy.
                    </p>
                    <p class="abt-d-txt">
                        8.2 By accepting these terms of use you agree to relieve us from any liability whatsoever arising from your use of information from any third party, or your use of any third party website, or your consumption of any food or beverages from a Participating Restaurant.
                    </p>
                    <p class="abt-d-txt">
                        8.3 We disclaim any and all liability to you for the supply of the Food Delivery, Goods and Services to the fullest extent permissible under applicable law. This does not affect your statutory rights as a consumer. If we are found liable for any loss or damage to you such liability is limited to the amount you have paid for the relevant Goods or Services. We cannot accept any liability for any loss, damage or expense, including any direct or indirect loss such as loss of profits to you, howsoever arising. This limitation of liability does not apply to personal injury or death arising as a direct result of our negligence.

                    </p>
                    <p class="abt-d-txt">
                        8.4 We do not accept any liability for any delays, failures, errors or omissions or loss of transmitted information, viruses or other contamination or destructive properties transmitted to you or your computer system via our Website.
                    </p>
                    <p class="abt-d-txt">
                        8.5 We shall not be held liable for any failure or delay in performing Services or delivering Goods where such failure arises as a result of any act or omission, which is outside our reasonable control such as all overwhelming and unpreventable events caused directly and exclusively by forces of nature that can be neither anticipated, nor controlled, nor prevented by the exercise of prudence, diligence, and care, including but not limited to: war, riot, civil commotion; compliance with any law or governmental order, rule, regulation or direction and acts of third parties.
                    </p>
                    <p class="abt-d-txt">
                        8.6 If we have contracted to provide identical or similar order to more than one Customer and are prevented from fully meeting our obligations to you by reason of an Event of Force Majeure, we may decide at our absolute discretion which orders we will process and to what extent.
                    </p>
                    <p class="abt-d-txt">
                        8.7 The products sold by us are provided for private domestic and consumer use only. Accordingly, we do not accept liability for any indirect loss, consequential loss, loss of data, loss of income or profit, loss of damage to property and/or loss from claims of third parties arising out of the use of the Website or for any products or services purchased from us.
                    </p>
                    <p class="abt-d-txt">
                        8.8 We have taken all reasonable steps to prevent Internet fraud and ensure any data collected from you is stored as securely and safely as possible. However, we cannot be held liable in the extremely unlikely event of a breach in our secure computer servers or those of third parties.
                    </p>
                    <p class="abt-d-txt">
                        8.9 In the event Ride Eat has a reasonable belief that there exists an abuse of vouchers and/or discount codes or in suspected instances of fraud, Ride Eat may cause the shopper (or customer) to be blocked immediately and reserves the right to refuse future service. Additionally, should there exist an abuse of vouchers or discount codes, Ride Eat reserves the right to seek compensation from any and all violators.
                    </p>
                    <p class="abt-d-txt">
                        8.10 Offers are subject to Ride Eat discretion and may be withdrawn at any time and without notice.
                    </p>
                </div>
                <div class="tc-wrap">
                    <h2 class="tc-title">9. General</h2>
                    <p class="abt-d-txt">
                        9.1 All prices are in Nigerian Naira. VAT(Value Added Tax) may be included where indicated
                    </p>
                    <p class="abt-d-txt">
                        9.2 We may subcontract any part or parts of the Services or Goods that we provide to you from time to time and we may assign and make changes to any part or parts of our rights under these Terms and Conditions without your consent or any requirement to notify you.
                    </p>
                    <p class="abt-d-txt">
                        9.3 We may alter or vary the Terms and Conditions at any time without prior notice to you.

                    </p>
                    <p class="abt-d-txt">
                        9.4 Payment must be made at the time of ordering the Food Delivery, Goods or Services obtained from us must be paid for promptly by debit card. We reserve the right to remove or add any suitable payment method.
                    </p>
                    <p class="abt-d-txt">
                        9.5 Do not use or launch any automated system or program in connection with our website or its online ordering functionality;
                    </p>
                    <p class="abt-d-txt">
                        9.6 Do not collect or harvest any personally identifiable information from the website, use communication systems provided by the website for any commercial solicitation purposes, solicit for any reason whatsoever any users of the website with respect to their submissions to the website, or publish or distribute any vouchers or codes in connection with the website, or scrape or hack the website.
                    </p>
                    <p class="abt-d-txt">
                        9.7 The Terms and Conditions together with the Privacy Policy, any order form and payment instructions constitute the entire agreement between you and Ride Eat. No other terms whether expressed or implied shall form part of this Agreement. In the event of any conflict between these Terms and Conditions and any other term or provision on the Ride Eat Website, these Terms and Conditions shall prevail.
                    </p>
                    <p class="abt-d-txt">
                        9.8 If any term or condition of our Agreement shall be deemed invalid, illegal or unenforceable, the parties hereby agree that such term or condition shall be deemed to be deleted and the remainder of the Agreement shall continue in force without such term or condition.
                    </p>
                    <p class="abt-d-txt">
                        9.9 These Terms and Conditions and our Agreement shall be governed by and construed in accordance with the laws of India. The parties hereto submit to the exclusive jurisdiction of the courts of India.
                    </p>
                    <p class="abt-d-txt">
                        9.10 No delay or failure on our part to enforce our rights or remedies under the Agreement shall constitute a waiver on our part of such rights or remedies unless such waiver is confirmed in writing.
                    </p>
                    <p class="abt-d-txt">
                        9.11 These Terms and Conditions and a contract (and all non-contractual obligations arising out of or connected to them) shall be governed and construed in accordance with India Laws. Both we and you hereby submit to the non-exclusive jurisdiction of the India Courts. All dealings, correspondence and contacts between us shall be made or conducted in the English language.
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection
