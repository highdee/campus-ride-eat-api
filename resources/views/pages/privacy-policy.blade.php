@extends('inc.nav')
@section('content')

        <!-- Hero banner -->
        <div class="bg-wrapper  abt-bg-img">
            <div class="abt-inner  width max-w">
                <h2 class="abt-title">PRIVACY POLICY FOR RIDE EATS</h2>
                <p class="abt-txt-secTitle">
                    <span class="abt-txt-num"></span>SECTION 1 -</span>
                    WHAT INFORMATION DO WE COLLECT
                </p>
                <p class="abt-txt">
                    Personal Data: We collect personal data from you when you voluntarily provide such information, such as when you contact us to place order for our products or to inquire about any information, respond to one of our surveys,which typically includes your:
                </p>
                <div class="abt-li-txt">
                    <p class="abt-d-txt">
                        <span class="abt-box"></span>
                        <span class="d-abt-txt">Name</span>
                    </p>
                    <p class="abt-d-txt">
                        <span class="abt-box"></span>
                        <span class="d-abt-txt">Telephone Number</span>
                    </p>
                    <p class="abt-d-txt">
                        <span class="abt-box"></span>
                        <span class="d-abt-txt">Email Address</span>
                    </p>
                    <p class="abt-d-txt">
                        <span class="abt-box"></span>
                        <span class="d-abt-txt">Home Address</span>
                    </p>
                    <p class="abt-d-txt">
                        <span class="abt-box"></span>
                        <span class="d-abt-txt">Information about your home which you give us</span>
                    </p>
                    <p class="abt-d-txt">
                        <span class="abt-box"></span>
                        <span class="d-abt-txt">Your payment details</span>
                    </p>
                    <p class="abt-d-txt">
                        <span class="abt-box"></span>
                        <span class="d-abt-txt">Your IP address and</span>
                    </p>
                    <p class="abt-d-txt">
                        <span class="abt-box"></span>
                        <span class="d-abt-txt">Any other personal information which give us in connection with the Services.</span>
                    </p>
                    <p class="abt-d-txt">
                        <span class="abt-box"></span>
                        <span class="d-abt-txt">Wherever we collect personal data we make an effort  to provide a link to this Privacy Policy</span>
                    </p>
                    <p class="abt-d-txt">
                        <span class="abt-box"></span>
                        <span class="d-abt-txt">If you send us personal correspondence, suchas emails or letters, we may collect such information into a file specific to you</span>
                    </p>
                </div>
                <p class="abt-d-txt">
                    <span class="abt-txt-num">URLs:</span>
                    In general, You can browse our Website without telling Us who You are or revealing any personal information about yourself. Once You give us your personal information, you are not anonymous to us. Where possible, we indicate which fields are required and which fields are optional. You always have the option to not provide information for the fields that are optional. We may automatically track certain information about You based upon your behaviour on Our Website. We use this information to do internal research on Our users' demographics, interests, and behaviour to better understand, protect and serve Our users. This information is compiled and analysed on an aggregated basis. This information may include the URL that you just came from (whether this URL is on our Website or not), which URL You next go to (whether this URL is on Our Website or not), your browser information, and your IP address.
                </p>
                <p class="abt-d-txt">
                    <span class="abt-txt-num">Cookies:</span>
                    We use data collection devices such as "cookies" on certain pages of the Website to help analyse our web page flow, measure promotional effectiveness, and promote trust and safety. "Cookies" are small files placed on your hard drive that assist us in providing our services. We offer certain features that are only available through the use of a "cookie". Most cookies are "session cookies," meaning that they are automatically deleted from your hard drive at the end of a session. You are always free to decline our cookies if your browser permits. Additionally, you may encounter "cookies" or other similar devices on certain pages of the Website that are placed by third parties. We do not control the use of cookies by third parties.
                </p>
                <p class="abt-d-txt">
                    <span class="abt-txt-num">Payment Information:</span>
                    If You transact with Us, We collect some additional information, such as a billing address, a debit card number and a debit card expiration date and/ or other payment instrument details and tracking information.
                </p>
                <p class="abt-txt-secTitle">
                    <span class="abt-txt-num"></span>SECTION 2 – </span>
                    WHAT DO WE DO WITH YOUR INFORMATION
                </p>
                <p class="abt-d-txt">
                    <span class="abt-txt-num">2.1</span>
                    We use personal information to provide the delivery of Our products to You. To the extent We use your personal information to market to You, We will provide You the ability to opt-out of such uses. We use your personal information to resolve disputes; troubleshoot problems; help promote a safe transaction; collect money; measure consumer interest in Our products, inform You about online and offline offers, products and updates; customize your experience; detect and protect Us against error, fraud and other criminal activity; enforce Our terms and conditions; and as otherwise described to You at the time of collection.
                </p>
                <p class="abt-d-txt">
                    <span class="abt-txt-num">2.2</span>
                    In Our efforts to continually improve Our product offerings, We collect and analyse demographic and profile data about Our Users' activity on Our Website. We identify and use your IP address to help diagnose problems with Our server, and to administer Our Website. Your IP address is also used to help identify You and to gather broad demographic information.
                </p>
                <p class="abt-txt-secTitle">
                    <span class="abt-txt-num"></span>SECTION 3 – </span>
                    CONSENT
                </p>
                <p class="abt-d-txt">
                    <span class="abt-txt-num">3.1 </span>
                     When You provide Us with personal information to complete a transaction, verify your debit card, place an order, arrange for a delivery or return a purchase, it is deemed that You have consented to the use of your personal information by Us to carry out the transaction as requested.
                </p>
                <p class="abt-d-txt">
                    <span class="abt-txt-num">3.2 </span>
                    In the course of business, if your personal information is required for any secondary reason, We shall specify the reason for requiring such information. Upon such request, You shall have the option to refrain from revealing such information to Us.
                </p>
                <p class="abt-d-txt">
                    <span class="abt-txt-num">3.3 </span>
                     If after You consent, either impliedly or expressly, for your personal information to be used by Us, You change your mind, You may withdraw your consent for Us to contact You, for the continued collection, use or disclosure of your information, at any time. Such withdrawal of consent shall be communicated to Us either through our provided support centers.
                </p>
                <p class="abt-txt-secTitle">
                    <span class="abt-txt-num"></span>SECTION 4 – </span>
                    DISCLOSURE OF PERSONAL INFORMATION
                </p>
                <p class="abt-d-txt">
                    <span class="abt-txt-num">4.1 </span>
                    We may share personal information with our other corporate entities and affiliates to help detect and prevent identity theft, fraud and other potentially illegal acts; correlate related or multiple accounts to prevent abuse of Our Website; and to facilitate joint or co-branded services where such services are provided by more than one corporate entity. Those entities and affiliates may not market to You as a result of such sharing unless You explicitly opt-in.
                </p>
                <p class="abt-d-txt">
                    <span class="abt-txt-num">4.2 </span>
                     We may disclose personal information of You if required to do so by law or in the good faith and belief that such disclosure is reasonably necessary to respond to subpoenas, court orders, or other legal process. We may disclose personal information to law enforcement offices, third party rights owners, or others in the good faith belief that such disclosure is reasonably necessary to enforce our Privacy Policy; respond to claims that an advertisement, posting or other content violates the rights of a third party; or protect the rights, property or personal safety of our Users or the general public.
                </p>
                <p class="abt-txt-secTitle">
                    <span class="abt-txt-num"></span>SECTION 5 – </span>
                    THIRD-PARTY SERVICES
                </p>
                <p class="abt-d-txt">
                    <span class="abt-txt-num">5.1 </span>
                    We may engage the services of third parties to help us serve Our customers better. In general, the third-party services providers engaged by Us will only collect, use and disclose your information to the extent necessary to allow them to perform the services they provide to Us.
                </p>
                <p class="abt-d-txt">
                    <span class="abt-txt-num">5.2 </span>
                    However, certain third-party service providers, such as payment gateways and other payment transaction processors, have their own privacy policies in respect to the information We are required to provide to them for your purchase-related transactions and we recommend that You read their privacy policies so You can understand the manner in which your personal information will be handled by these third party service providers.
                </p>
                <p class="abt-d-txt">
                    <span class="abt-txt-num">5.3 </span>
                    In particular, remember that certain providers may be located in or have facilities that are located a different jurisdiction than either you or us. So if you elect to proceed with a transaction that involves the services of a third-party service provider, then your information may become subject to the laws of the jurisdiction(s) in which that service provider or its facilities are located.
                </p>
                <p class="abt-d-txt">
                    <span class="abt-txt-num">5.4 </span>
                    Once you leave Our website or are redirected to a third-party website or application, You are no longer governed by this Privacy Policy or our website’s Terms of Service.
                </p>
                <p class="abt-d-txt">
                    <span class="abt-txt-num">5.5 </span>
                    When you click on third party links on Our Website, We are not responsible for the privacy practices of other sites and for the loss or consequential damage that may be caused to you and encourage You to read their privacy statements.
                </p>
                <p class="abt-txt-secTitle">
                    <span class="abt-txt-num"></span>SECTION 6 – </span>
                    SECURITY
                </p>
                <p class="abt-d-txt">
                    <span class="abt-txt-num"> 6.1 </span>
                    To protect your personal information, we take reasonable precautions and follow industry best practices to make sure it is not inappropriately lost, misused, accessed, disclosed, altered or destroyed.
                </p>
                <p class="abt-d-txt">
                    <span class="abt-txt-num">6.2 </span>
                    If you provide Us with your credit card information, the information is encrypted using secure socket layer technology (SSL). Although, no method of transmission over the Internet or electronic storage is 100% secure, we follow all requirements and implement additional generally accepted industry standards.
                </p>
                <p class="abt-txt-secTitle">
                    <span class="abt-txt-num"></span>SECTION 7 – </span>
                    AGE OF CONSENT
                </p>
                <p class="abt-d-txt">
                    <span class="abt-txt-num">7.1 </span>
                    By using this site, You represent that You are at least the age of majority in your state or province of residence and you have given us your consent to allow any of your minor dependents to use this site.
                </p>
                <p class="abt-txt-secTitle">
                    <span class="abt-txt-num"></span>SECTION 8 – </span>
                    CHANGES TO THIS PRIVACY POLICY
                </p>
                <p class="abt-d-txt">
                    <span class="abt-txt-num">8.1 </span>
                    We reserve the right to modify this privacy policy at any time, so please review it frequently. Changes and clarifications will take effect immediately upon their posting on the Website. If We make material changes to this policy, We will notify You here that it has been updated, so that You are aware of what information We collect, how we use it, and under what circumstances, if any, We use and/or disclose it.
                </p>
                <p class="abt-d-txt">
                    <span class="abt-txt-num">8.2 </span>
                    If our Website is acquired or merged with another company, your information may be transferred to the new owners so that we may continue to sell products to You.
                </p>
            </div>
        </div>
    </div>
@endsection
