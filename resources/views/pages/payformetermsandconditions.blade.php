@extends('inc.nav')
@section('content')

        <!-- Hero banner -->
        <div class="bg-wrapper  abt-bg-img">
            <div class="abt-inner  width max-w"> 
                <h2 class="tc-title">Terms And Condition For Payforme</h2>
                <div class="tc-wrap">
                    <p class="abt-d-txt">
                    You have agreed, to the best of your knowledge to pay part or full amount for your friend whose name was indicated and under no circumstances will Rideeats Food Ordering & Delivery company be required to make a refund.
                    </p>
                </div>
                <div class="tc-wrap">
                    <p class="abt-d-txt">
                    You fully acknowledged that the person in question who has provided you the link to make payment is your friend/acquaintance and you have accepted to make payment on behalf of this person.
                    </p>
                </div>
                <div class="tc-wrap">
                    <p class="abt-d-txt">
                    You fully agreed to provide any information, which may include but is not limited to phone number(s) and/or e-mail, required in the making of your Pay4Me payment to Rideeats Food Ordering & Delivery and chosen payment partner, required to get across to you for confirmation of your payment.
                    </p>
                </div>
                <div class="tc-wrap">
                    <p class="abt-d-txt">
                    You agree that Rideeats Food Ordering & Delivery may, in future times, be in correspondence with you via your contact information provided during the payment process.
                    </p>
                </div>
                <div class="tc-wrap">
                    <p class="abt-d-txt">
                    Disclaimer: Rideeats does not retain any information directly involved with making payments which may include: pin, passwords, bank verification number and debit/credit card information.
                    </p>
                </div>
                 
            </div>
        </div>
    </div>
@endsection
