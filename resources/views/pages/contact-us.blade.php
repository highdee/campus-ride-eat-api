@extends('inc.nav')
@section('content')

        <!-- Hero banner -->
        <div class="bg-wrapper c-wrapper  abt-bg-img">
            <div class="contact-inner c-width width max-w">
                <div class="row contact-form">
                    <div class="col-xm-12 col-sm-7 col-md-7 pad-radl">
                        <div class="c-form-wrap">
                            <h6 class="c-frm-title">Feel free to contact us any time.
                                We wiil get back to you as soon as we can!
                            </h6>
                            <form method="POST" action="/contact-us">
                                @if(session()->has('success'))
                                    <div class="alert alert-success">
                                        <p>{{ session()->get('success') }}</p>
                                    </div>
                                @endif
                                @csrf

                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Name</label>
                                    <input type="text" name="name" class="form-control c-inp-frm" id="exampleFormControlInput1" >
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlInput2">Email</label>
                                    <input type="email" name="email" class="form-control c-inp-frm" id="exampleFormControlInput2" >
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Message</label>
                                    <textarea class="form-control" name="message" id="exampleFormControlTextarea1" rows="4" ></textarea>
                                </div>

                                <button type="submit" class="btn btn-primary mb-2">Send</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-xm-12 col-sm-5 col-md-5 pad-radr">
                        <div class="contact-info-wrapper">
                            <h6 class="c-info-title">Contact Information</h6>
                            <ul class="c-info">
                                <li class="c-info-li">info@getintouch.com</li>
                                <li class="c-info-li">+2349069349646</li>
                                <li class="c-info-li">Under G</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
