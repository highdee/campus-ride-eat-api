@extends('inc.nav')
@section('content')


    <!-- Hero banner -->
    <div class="bg-wrapper width bg-img">
        <div class="bg-inner">
            <h1 class="bg-title">Your favorite restaurants <br> at your fingertips</h1>
            <div class="bg-app-btn">
                <a target="_blank" href="https://play.google.com/store/apps/details?id=com.highdee.Campus_Ride_eat" class="">
                    <img src="{{asset('app-assets/images/pages/googlePlaystore.png')}}" class="goog" alt="">
                </a>
                <a target="_blank" href="https://apps.apple.com/us/app/rideeats/id1559552064">
                    <img src="{{asset('app-assets/images/pages/app-store-logo.png')}}" class="apple" alt="">
                </a>
            </div>

            <div class="heroBan-img-wrapper">
                <img src="{{asset('app-assets/images/pages/paper-bag.png')}}" class="paper-bag" alt="">
                <img src="{{asset('app-assets/images/pages/penne-pasta.png')}}" class="pasta" alt="">
                <img src="{{asset('app-assets/images/pages/burger-stand.png')}}" class="burger" alt="">
            </div>
        </div>
    </div>
</div>

<!-- What we offer -->
<div class="container-fluid p-bt">
    <div class="row width max-w">
        <div class="col-xs-12 col-sm-6 col-md-4 after-line">
            <div class="wwo-txt-wrap">
                <p class="wwo-txt">
                    Shop from an unlimited supply of meals and groceries .
                </p>
                <div class="wwo-icon-wrap">
                    <img src="{{asset('app-assets/images/pages/Vector1.png')}}" alt="">
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 after-line">
            <div class="wwo-txt-wrap">
                <p class="wwo-txt">
                    Get your orders to your location by our delivery partners.
                </p>
                <div class="wwo-icon-wrap">
                    <img src="{{asset('app-assets/images/pages/Vector (2).png')}}" alt="">
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="wwo-txt-wrap">
                <p class="wwo-txt">
                    Receive your package at your doorstep.
                </p>
                <div class="wwo-icon-wrap">
                    <img src="{{asset('app-assets/images/pages/Vector (3).png')}}" alt="">
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Order Meal -->
<div class="container-fluid bg-yellow">
    <div class="row width">
        <div class="col-xs-12 col-sm-8 col-md-7 pr-0">
            <div class="order-img-wrapper">
                <img src="{{asset('app-assets/images/pages/4198224-01 1.png')}}" class="caller" alt="">
                <img src="{{asset('app-assets/images/pages/3696912-01 1.png')}}" class="rider" alt="">
            </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-5 pl-0">
            <h3 class="order-txt">
                More to love on the Android/iOs app
            </h3>
        </div>
    </div>
</div>
@endsection
