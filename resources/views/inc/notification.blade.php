@if (session()->has('msg'))
    @if(session()->get('status') == 'success')
        <div class="alert alert-success text-center">
            <ul class="list-unstyled mb-0">
                <li>{{ session()->get('msg') }}</li>
            </ul>
        </div>
    @else
        <div class="alert alert-danger  text-center">
            <ul class="list-unstyled mb-0">
                <li>{{ session()->get('msg') }}</li>
            </ul>
        </div>
    @endif
@endif
@if ($errors->any())
    <div class="alert alert-danger  text-center">
        <ul class="list-unstyled mb-0">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
