<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="google-site-verification" content="LnZvL-PkY5YEbOZ0cEYQnJRDJdExemlYtIndi1o8Td8" />
    <meta property="og:url"                content="https://rideeat.org" />
    <meta property="og:type"               content="Food" />
    <meta property="og:title"              content="Ride Eats| Online Based Food Ordering and Delivery" />
    <meta property="og:description"        content="Order meals, groceries, takeouts and more from favorite local restaurants. Order food online in the Ride Eats mobile/iOS app and support local restaurants" />
    <meta property="og:image"              content="{{asset('app-assets/images/pages/rideeatLogo.png')}}" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('app-assets/css/index.css')}}">
    <title>Ride Eats| Online Based Food Ordering and Delivery</title>
    @yield('app-asset')
</head>
<body>
<div class="container-fluid p-0 ">
    <!-- Navbar -->
    <div class="nav-wrapper width">
        <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top ">
            <a class="navbar-brand" href="/"><img src="{{asset('app-assets/images/pages/rideeatLogo.png')}}" class="nav-brand-img" alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon">
                    <!-- <i class="fas fa-bars" style="color:#ff8900; font-size:28px;"></i> -->
                    <span class="fo-tog"></span>
                    <span class="fo-tog"></span>
                    <span class="fo-tog"></span>
                </span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">

                    <li class="nav-item">
                        <a class="nav-link" href="/about-us">ABOUT US</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/contact-us">CONTACT US</a>
                    </li>
                </ul>
                <div class="navbar-icons">
                    <a href="https://www.facebook.com/rideeat.org/"><i class="fa fa-facebook"></i></a>
                    <a href="https://twitter.com/RideEat_Org?s=09"><i class="fa fa-twitter"></i></a>
                    <a href="https://www.instagram.com/rideeats_org?r=nametag"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
        </nav>
    </div>
    @yield('content')
    <div id="fb-root"></div>
    <!-- Footer -->
    <div class="footer-wrapper ">
    <div class="row width padding max-w">
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2 mb-4">
            <div class="footer-app-btn">
                <a target="_blank" href="https://play.google.com/store/apps/details?id=com.highdee.Campus_Ride_eat">
                    <img src="{{asset('app-assets/images/pages/googlePlaystore.png')}}" class="foot-bt" alt="">
                </a>
                <a target="_blank" href="https://apps.apple.com/us/app/rideeats/id1559552064">
                    <img src="{{asset('app-assets/images/pages/app-store-logo.png')}}" class="foot-bt" alt="">
                </a>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2 mb-4">
            <ul class="footer-ul">
                <li><a href="/about-us">About us</a></li>
                <li><a href="/contact-us">Contact us</a></li>
                <li><a href="#">Reviews</a></li>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 mb-4">
            <div class="footer-legal-txt">
                <p class="legal">Legal</p>
                <p class="pp"><a href="/privacy-policy">Privacy and Policy</a></p>
                <p class="pp"><a href="terms-and-conditions">Terms and Conditions</a></p>
            </div>
            <p class="copy-right">Copyright 2021 Ride Eat, All right reserved</p>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-2 col-lg-2 mb-4">
            <div class="footer-icons">
                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-instagram"></i></a>
            </div>
        </div>
    </div>
</div>

</div>
@yield('app-js')
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '290873175799949');
    fbq('track', 'PageView');
</script>
<noscript>
    <img height="1" width="1"
         src="https://www.facebook.com/tr?id=290873175799949&ev=PageView
&noscript=1"/>
</noscript>
<script>
    window.fbAsyncInit = function() {
      FB.init({
        xfbml            : true,
        version          : 'v11.0'
      });
    };

    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <!-- Your Chat Plugin code -->
    <div class="fb-customerchat"
      attribution="install_email"
      page_id="503189640416707">
    </div>
<!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-2RGWE0J9XC"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-2RGWE0J9XC');
</script>
</body>
</html>
