<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>Campus ride eat admin</title>
    <link rel="apple-touch-icon" href="{{asset('app-assets/images/ico/apple-icon-120.png')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('app-assets/images/ico/favicon.ico')}}">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/vendors.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/charts/apexcharts.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/extensions/swiper.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/bootstrap-extended.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/colors.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/components.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/themes/dark-layout.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/themes/semi-dark-layout.css')}}">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/pages/dashboard-ecommerce.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/plugins/forms/validation/form-validation.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/forms/select/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/pickers/pickadate/pickadate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/pages/page-users.css')}}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/style.css')}}">
    <!-- END: Custom CSS-->
    @yield('app-asset')

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 2-columns  navbar-sticky footer-static @yield('hoverToOpen')" data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">

    <!-- BEGIN: Header-->
    <div id="apps">
        <div class="header-navbar-shadow"></div>
        <nav class="header-navbar main-header-navbar navbar-expand-lg navbar navbar-with-menu fixed-top ">
            <div class="navbar-wrapper">
                <div class="navbar-container content">
                    <div class="navbar-collapse" id="navbar-mobile">
                        <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                            <ul class="nav navbar-nav">
                                <li class="nav-item mobile-menu d-xl-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ficon bx bx-menu"></i></a></li>
                            </ul>
                        </div>
                        <ul class="nav navbar-nav float-right">

                            <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                                    <div class="user-nav d-sm-flex d-none"><span class="user-name">Admin</span><span class="user-status text-muted">Available</span></div><span><img class="round" src="{{asset('app-assets/images/avatar.png')}}" alt="avatar" height="40" width="40"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right pb-0">
                                    <div class="dropdown-divider mb-0"></div>
                                    <a class="dropdown-item" href="/campus-admin/logout"><i class="bx bx-power-off mr-50"></i> Logout</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        <!-- BEGIN: Main Menu-->
        <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
            <div class="navbar-header">
                <ul class="nav navbar-nav flex-row">
                    <li class="nav-item mr-auto"><a class="navbar-brand" href="writers-api/admin">
                            {{-- <div class="brand-logo"></div> --}}
                            <h2 class="brand-text mb-0">Admin</h2>
                        </a>
                    </li>
                    <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="bx bx-x d-block d-xl-none font-medium-4 primary"></i><i class="toggle-icon bx-disc font-medium-4 d-none d-xl-block primary bx" data-ticon="bx-disc"></i></a></li>
                </ul>
            </div>
            <div class="shadow-bottom"></div>
            <div class="main-menu-content">
                <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation" data-icon-style="lines">
                    <li class=" nav-item @yield('adminHome')"><a href="/campus-admin/"><i class="bx bxs-home" data-icon="morph-folder"></i><span class="menu-title" data-i18n="Documentation">Dashboard</span></a></li>
                    <li class=" nav-item  @yield('userActive') "><a href="/campus-admin/userList"><i class="bx bx-user mr-1"></i><span class="menu-title">Users</span></a></li>
                    <li class=" nav-item  @yield('pay4me') "><a href="/campus-admin/pay4mes"><i class="bx bx-wallet-alt mr-1"></i><span class="menu-title">Pay4me</span></a></li>
                    <li class="nav-item has-sub @yield('configActive')"><a href="#"><i class="bx bx-cog"></i><span class="menu-title" data-i18n="Configuration">Configuration</span></a>
                        <ul class="menu-content">
                            <li class="@yield('delayActive')"><a href="/campus-admin/delay-time"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Delay Time">Delay Time</span></a>
                            </li>
                            <li class="@yield('annActive')"><a href="/campus-admin/announcement"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Announcement">Announcement</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class=" nav-item  @yield('streetActive') "><a href="/campus-admin/streetList"><i class="bx bx-directions mr-1"></i><span class="menu-title">Streets</span></a></li>
                    <li class=" nav-item  @yield('vendorActive') "><a href="/campus-admin/vendorList"><i class="bx bx-group mr-1"></i><span class="menu-title">Vendors</span></a></li>
                    <li class=" nav-item  @yield('featuredV') "><a href="/campus-admin/featured-vendors"><i class="bx bx-sort-z-a mr-1"></i><span class="menu-title">Featured Vendors</span></a></li>
                    <li class=" nav-item  @yield('riderActive') "><a href="/campus-admin/riderList"><i class="bx bx-bicycle mr-1"></i><span class="menu-title">Riders</span></a></li>
                    <li class=" nav-item  @yield('foodActive') "><a href="/campus-admin/foods"><i class="bx bx-dish mr-1"></i><span class="menu-title">Foods</span></a></li>
                    <li class=" nav-item  @yield('featuredF') "><a href="/campus-admin/featured-foods"><i class="bx bx-sort-z-a mr-1"></i><span class="menu-title">Featured Foods</span></a></li>
                    <li class=" nav-item  @yield('typeActive') "><a href="/campus-admin/types"><i class="bx bx-spa mr-1"></i><span class="menu-title">Food Types</span></a></li>
                    <li class=" nav-item  @yield('orderActive') "><a href="/campus-admin/orders"><i class="bx bx-package mr-1"></i><span class="menu-title">Orders</span></a></li>
                    <li class=" nav-item  @yield('deliveriesActive') "><a href="/campus-admin/deliveries"><i class="bx bx-basket mr-1"></i><span class="menu-title">Deliveries</span></a></li>
                    <li class=" nav-item  @yield('transActive') "><a href="/campus-admin/transactions"><i class="bx bx-money mr-1"></i><span class="menu-title">Transactions</span></a></li>
                    <li class=" nav-item  @yield('deliveryActive') "><a href="/campus-admin/delivery_system"><i class="bx bxs-gas-pump mr-1"></i><span class="menu-title">Delivery System</span></a></li>
                    <li class=" nav-item  @yield('sdsActive') "><a href="/campus-admin/sds"><i class="bx bx-street-view mr-1"></i><span class="menu-title">Source Delivery System</span></a></li>
                    <li class=" nav-item  @yield('withdraw') "><a href="/campus-admin/withdrawals"><i class="bx bx-credit-card-alt mr-1"></i><span class="menu-title">Withdrawals</span></a></li>
                    <li class=" nav-item  @yield('u_vendor') "><a href="/campus-admin/vendors-request"><i class="bx bx-question-mark mr-1"></i><span class="menu-title">Vendors Request</span></a></li>
                    <li class=" nav-item  @yield('u_rider') "><a href="/campus-admin/riders-request"><i class="bx bx-accessibility mr-1"></i><span class="menu-title">Riders Request</span></a></li>
                    <li class=" nav-item  @yield('withdrawr') "><a href="/campus-admin/withdrawal-request"><i class="bx bxl-mastercard mr-1"></i><span class="menu-title">Withdrawal Request</span></a></li>
                    <li class=" nav-item"><a href="/campus-admin/logout"><i class="bx bx-power-off mr-1"></i><span class="menu-title" data-i18n="Chat">Logout</span></a></li>
                </ul>
            </div>
        </div>
        <!-- END: Main Menu-->
        <div class="app-content content">
            <div class="content-overlay"></div>
            <div class="content-wrapper">
                @yield('content')
            </div>
        </div>
    </div>
    <script src="{{asset('app-assets/vendors/js/vendors.min.js')}}"></script>
    <script src="{{asset('app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js')}}"></script>
    <script src="{{asset('app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js')}}"></script>
    <script src="{{asset('app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js')}}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{asset('app-assets/vendors/js/charts/apexcharts.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/extensions/swiper.min.js')}}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{asset('app-assets/js/scripts/configs/vertical-menu-light.js')}}"></script>
    <script src="{{asset('app-assets/js/core/app-menu.js')}}"></script>
    <script src="{{asset('app-assets/js/core/app.js')}}"></script>
    <script src="{{asset('app-assets/js/scripts/components.js')}}"></script>
    <script src="{{asset('app-assets/js/scripts/footer.js')}}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{asset('app-assets/js/scripts/pages/dashboard-ecommerce.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/forms/validation/jqBootstrapValidation.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/pickers/pickadate/picker.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/pickers/pickadate/picker.date.js')}}"></script>
    <script src="{{ asset('app-assets/js/scripts/pages/page-users.js')}}"></script>
    <script src="{{ asset('app-assets/js/scripts/navs/navs.js')}}"></script>
    <!-- END: Page JS-->

    <script src="{{asset('app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/vfs_fonts.js')}}"></script>
    <script src="{{asset('app-assets/js/scripts/datatables/datatable.js')}}"></script>
    <script src="{{asset('app-assets/js/main.js')}}"></script>
    @yield('script')
</body>
<!-- END: Body-->

</html>
