<?php


Auth::routes();



Route::get('/', function () {
    return view('pages.home');
})->name('index');
Route::get('/pay-for-me/{url}', "HomeController@payforme");
Route::get('/payforme/terms-conditions', function(){
    return view('pages.payformetermsandconditions');
});

Route::get('about-us', function () {
    return view('pages.about-us');
});
//Route::get('contact-us', function () {
//    return view('pages.contact-us');
//});
Route::get('privacy-policy', function () {
    return view('pages.privacy-policy');
});
Route::get('guidelines', function () {
    return view('pages.guidelines');
});
Route::get('terms-and-conditions', function () {
    return view('pages.terms-conditions');
});

Route::post('/contact-us', 'ContactUsController@message');

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('/campus-admin')->group(function(){
    Route::get('/', 'adminController@index');
    Route::get('/login', 'adminLoginController@getlogin');
    Route::get('/featured-foods', 'adminController@showFeaturedFoods');
    Route::get('/featured-vendors', 'adminController@showFeaturedVendors');
    Route::get('/foods', 'adminController@foods');
    Route::get('/food/{id}', 'adminController@food');
    Route::post('/delete-food', 'adminController@deleteFood');
    Route::get('vendors-request', 'adminController@unverified_vendor');
    Route::get('unverified-vendor/{id}', 'adminController@unverified');
    Route::post('verify_vendor','adminController@verify_vendor');
    Route::post('cancel_vendor_request','adminController@cancelVendorRequest');
    Route::get('riders-request', 'adminController@unverified_riders');
    Route::get('unverified-rider/{id}','adminController@unverified_rider');
    Route::post('verify_rider','adminController@verify_rider');
    Route::post('cancel_rider_request','adminController@cancelRiderRequest');
    Route::post('/add-item-type', 'adminController@addItemType');
    Route::get('/types','adminController@types');
    Route::get('/type/{id}', 'adminController@type');
    Route::post('updateType', 'adminCOntroller@updateType');
    Route::post('deleteType', 'adminController@deleteType');
    Route::post('/login','adminLoginController@login');

    Route::post('mail-user', 'adminController@mailUser');
    Route::post('notify-all-users', 'adminController@notifyAllUser');
    Route::post('credit-user', 'adminController@creditUser');
    Route::post('verify-user-credit', 'adminController@verifyUserCredit');
    Route::get('confirm-user-credit/{user_id}/{amount}', 'adminController@confirmUserCredit');

    Route::post('credit-vendor', 'adminController@creditVendor');
    Route::post('verify-vendor-credit', 'adminController@verifyVendorCredit');
    Route::get('confirm-vendor-credit/{vendor_id}/{amount}', 'adminController@confirmVendorCredit');

    Route::post('credit-rider', 'adminController@creditRider');
    Route::post('verify-rider-credit', 'adminController@verifyRiderCredit');
    Route::get('confirm-rider-credit/{rider_id}/{amount}', 'adminController@confirmRiderCredit');
    Route::get('export-users', 'adminController@printUserExcel');

    Route::get('delay-time', 'adminController@delayTime');
    Route::post('update-delay-time', 'adminController@updateDelayTime');
    Route::get('announcement', 'adminController@announcement');
    Route::post('update-announcement', 'adminController@updateAnnouncement');

    Route::get('pay4mes', 'adminController@payformes');
    Route::get('pay4me/{id}', 'adminController@payforme');

    Route::get('/user/{id}', 'adminController@user');
    Route::get('vendor/{id}', 'adminController@vendor');
    Route::get('rider/{id}', 'adminController@rider');
    Route::get('/userList', 'adminController@userList');
    Route::get('/vendorList', 'adminController@vendorList');
    Route::get('/riderList', 'adminController@riderList');
    Route::get('/streetList', 'adminController@streetList');
    Route::post('/addStreet', 'adminController@addStreet');
    Route::post('/deleteUser', 'adminController@deleteUser');
    Route::post('/deleteVendor', 'adminController@deleteVendor');
    Route::post('/delete-rider', 'adminController@deleteRider');
    Route::post('deletestreet', 'adminController@deletestreet');
    Route::post('addDeliverySystem', 'adminController@addDeliverySystem');
    Route::post('deleteDelivery_system', 'adminController@deleteDelivery_system');
    Route::get('delivery_system', 'adminController@delivery_system');
    Route::get('deliveries', 'adminController@deliveries');
    Route::get('transactions', 'adminController@transactions');
    Route::get('sds', 'adminController@sds');
    Route::get('orders', 'adminController@orders');
    Route::post('admin-cancel-order', 'adminController@adminCancelOrder');
    Route::post('admin-complete-order', 'adminController@adminCompleteorder');
    Route::get('order/{id}', 'adminController@order');
    Route::get('transactions', 'adminController@transactions');
    Route::post('addSds','adminController@addSds');
    Route::post('updateSds', 'adminController@updateSds');
    Route::post('deleteSds', 'adminController@deleteSds');
    Route::post('banUser','adminController@banUser');
    Route::post('banvendor','adminController@banVendor');
    Route::post('ban-rider', 'adminController@banRider');
    Route::get('logout', 'adminController@logout');
    Route::post('food-feature-status', 'adminController@FoodFeaturedStatus');
    Route::post('vendor-feature-status', 'adminController@VendorFeaturedStatus');
    Route::get('withdrawal-request', 'adminController@withdrawalRequest');
    Route::get('withdrawals', 'adminController@withdrawals');
    Route::get('withdrawal/{id}', 'adminController@withdrawal');
    Route::post('accept-withdrawal', 'adminController@acceptWithdrawal');
    Route::post('reject-withdrawal', 'adminController@rejectWithdrawal');
});


Route::get('foods/large/{item}/{width?}/{height?}',function ($name,$width=0,$height=0){
    $img = Image::make(storage_path().'/foods/large/'.$name);
    if($width == 0 || $height == 0){
        $height=$img->height();
        $width=$img->width();
    }
    return $img->resize($width, $height)->response('jpg');
    // return $img->response('jpg');
});

Route::get('foods/types/{item}',function ($name){
    $img = Image::make(storage_path().'/foods/types/'.$name);
    return $img->response('jpg');
});


Route::get('vendors_images/{item}/{width?}/{height?}',function ($name,$width=0,$height=0){
    $img = Image::make(storage_path().'/vendors_images/'.$name);

    if($width == 0 || $height == 0){
        $height=$img->height();
        $width=$img->width();
    }
    return $img->resize($width, $height)->response('jpg');
});


Route::get('profile_pictures/{item}/{width?}/{height?}',function ($name,$width=0,$height=0){
    $img = Image::make(storage_path().'/profile_pictures/'.$name);

    if($width == 0 || $height == 0){
        $height=$img->height();
        $width=$img->width();
    }
    return $img->resize($width, $height)->response($img->mime());

});


