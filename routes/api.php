<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::prefix('v1')->group(function(){

//    Route::post('get-operators', 'loginController@getOperators');
//    Route::post('get-operators-products', 'loginController@getOperatorsProduct');
//    Route::post('charge-transaction', 'loginController@chargeTransaction');

    //=================== PUBLIC API'S ===========================
    Route::get('get', 'loginController@get');
    Route::post('login',"loginController@login");
    Route::post('create-account',"loginController@create_account");
    Route::get('verify-account/{token}',"loginController@verify_account");
    Route::post('forgot-password',"loginController@forget_password");
    Route::post('reset-password',"loginController@reset_password");
    Route::get('verify-token',"loginController@verify_password");




    //========================= GENERAL API's ================================
    Route::get('get-user',"userController@getUser");
    Route::post('logEvent',"userController@logEvent");
    Route::get('get-item-types',"appController@getItemsTypes");
    Route::get('get-restaurants',"appController@getRestaurants");
    Route::get('get-restaurant/{id}/{item_id}',"appController@getRestaurant");
    Route::get('get-delivery-details',"appController@getDeliveryData");
    Route::get('get-user-transactions/{id}',"appController@getTransactions");
    Route::get('get-user-payforme/{id}',"appController@getPayForMe");
    Route::post("review/{order}","ReviewController@review");
    Route::post("update-details","userController@updateDetails");
    Route::post("update-bank-details","userController@updateBankDetails");
    Route::get('get-withdrawal-requests',"userController@getWithdrawalRequest");
    Route::get('get-misc',"appController@getMisc");
    Route::get('update-device',"appController@updateDevice");
    Route::get('get-items',"appController@getItems");
    Route::get('reviews/{id}',"appController@reviews");
    Route::get('get-photos/{id}',"vendorController@getPhotos");
    Route::get('toggle-notification/{vendor}/{command}/{type}',"userController@toggleUserVendorNotification");
    Route::get('get-notification-status/{v_id}',"userController@getNotificationStatus");




    //==================== VENDOR'S API'S =================================
    Route::post('create-vendor-account',"vendorController@create");
    Route::post('add-food',"vendorController@add_food");
    Route::post('update-food/{id}',"vendorController@update_food");
    Route::post('update-profile-picture',"vendorController@update_picture");
    Route::post('update-banner-picture',"vendorController@update_banner");
    Route::post('update-vendor-profile',"vendorController@update_vendor");
    Route::post('update-opening-closing',"vendorController@update_opening_closing");
    Route::post('upload-photos',"vendorController@update_photos");
    Route::get('get-vendor',"vendorController@getVendor");
    Route::get('get-items/{id}',"vendorController@getItems");
    Route::get('get-item/{id}',"vendorController@getItem");
    Route::get('delete-item/{id}',"vendorController@deleteItem");
    Route::get('get-orders',"vendorController@getOrders");
    Route::get('get-order/{id}',"vendorController@getOrder");
    Route::get('get-vendor-transactions/{id}',"vendorController@getTransactions");


    //==================== RIDER'S API'S ====================================
    Route::post('create-rider-account',"riderController@create");
    Route::get('get-rider',"riderController@getRider");
    Route::post('rider-update-opening-closing',"riderController@update_opening_closing");
    Route::post('process-order/{order}/{action}',"vendorController@processOrder");
    Route::get('pick-order/{order}',"riderController@pickOrder");
    Route::get('deliver-order/{order}/{code}',"riderController@deliverOrder");
    Route::get('get-deliveries',"riderController@getDeliveries");
    Route::get('get-my-deliveries',"riderController@getMyDeliveries");
    Route::get('get-rider-transactions/{id}',"riderController@getTransactions");
    Route::post('update-rider-profile',"riderController@updateRider");




    //================== PAYMENT API'S ======================================
    Route::post('get-access-code',"PaymentController@getAccessCode");
    Route::get('get-cards',"PaymentController@getCard");
    Route::post('pay-with-card/{id}/{amount}',"PaymentController@payWithCard");
    Route::post('pay-with-wallet/{amount}',"PaymentController@payWithWallet");
    Route::post('credit-wallet',"PaymentController@creditWalletWithRef");
    Route::post('credit-with-card/{id}/{amount}',"PaymentController@creditWalletWithExistingCard");
    Route::post('create-pay-for-me',"PayformeController@createToken");
    Route::get('get-pay-for-me/{id}',"PayformeController@get");
    Route::post('payment-hook',"PaymentController@receivePaymentHook");


    Route::prefix("vendor-delivery")->middleware(['jwt.auth'])->group(function(){
        Route::post('get-cost/{source}/{destination}', "VendorDeliveryController@getCostAction");
        Route::get('get-orders', "VendorDeliveryController@getOrders");
        Route::get('get-available-orders', "VendorDeliveryController@getAvailableOrders");
        Route::get('get-order/{order}', "VendorDeliveryController@getOrder");
        Route::get('pick-order/{order}', "VendorDeliveryController@pickOrder");
        Route::post('create/{source}/{destination}', "VendorDeliveryController@createOrder");
        Route::get('complete-order/{order}/{code}', "VendorDeliveryController@completeOrder");
        Route::get('cancel-order/{order}', "VendorDeliveryController@cancelOrder");
    });


    Route::apiResources([
        'orders'=> 'OrderController',
    ]);

    //================== ADMIN API'S ======================================
    Route::post('sort-featured-food', 'loginController@sortFoods');
    Route::post('sort-featured-vendor', 'loginController@sortVendors');

});


